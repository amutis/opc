import Vue from 'vue'
import Router from 'vue-router'
import Public from '@/components/Public'
import Back from '@/components/Back'
import Login from '@/components/Back/Login'
import ResetPassword from '@/components/Back/ResetPassword'
import PasswordReset from '@/components/Back/PasswordReset'

// Public
import Index from '@/components/Public/Pages/Index'
import Reservation from '@/components/Public/Pages/Reservation'
import PFacility from '@/components/Public/Pages/Facility'
import PActivity from '@/components/Public/Pages/Activity'
import ActivitySelection from '@/components/Public/Pages/ActivitySelection'
import BookingInformation from '@/components/Public/Pages/BookingInformation'
import PaymentInformation from '@/components/Public/Pages/PaymentInformation'
import BookingSuccess from '@/components/Public/Pages/BookingSuccess'
import SupportUs from '@/components/Public/Pages/SupportUs'
import SupportPayment from '@/components/Public/Pages/SupportPayment'
import SuccessDonation from '@/components/Public/Pages/SuccessDonation'
import RegisterMember from '@/components/Public/Pages/RegisterMember'
import MembershipPayment from '@/components/Public/Pages/MembershipPayment'
import SuccessMembership from '@/components/Public/Pages/SuccessMembership'
import PVolunteerProgram from '@/components/Public/Repeated/Programs'
import Program from '@/components/Public/Pages/Program'
import ProgramPayment from '@/components/Public/Pages/ProgramPayment'
import PTermsAndConditions from '@/components/Public/Pages/TermsAndConditions'
import SuccessVolunteer from '@/components/Public/Pages/SuccessVolunteer'

// Back
import Dashboard from '@/components/Back/Dashboard'
import Calendar from '@/components/Back/Calendar'

// Booking
import NewBooking from '@/components/Back/Bookings/New'
import Bookings from '@/components/Back/Bookings/Bookings'
import Booking from '@/components/Back/Bookings/Booking'
import GroupBooking from '@/components/Back/Bookings/GroupBooking'
import BookingSettings from '@/components/Back/Bookings/BookingSettings'
import ConfirmedBookings from '@/components/Back/Bookings/Confirmed'
import UnconfirmedBookings from '@/components/Back/Bookings/Unconfirmed'
import FindBooking from '@/components/Back/Bookings/FindBooking'
import CheckedIn from '@/components/Back/Bookings/CheckedIn'
import CheckedOut from '@/components/Back/Bookings/CheckedOut'
import GatePayment from '@/components/Back/Bookings/GatePayment'
import GateTickets from '@/components/Back/Bookings/GateTickets'
import Receipt from '@/components/Back/Bookings/Receipt'
import PartnerReceipt from '@/components/Back/Bookings/PartnerReceipt'
import NewPartnerBooking from '@/components/Back/Bookings/NewPartnerBooking'
import Checkin from '@/components/Back/Bookings/CheckIn'
import EditBooking from '@/components/Back/Bookings/EditBooking'

// Inventory
import Inventories from '@/components/Back/Inventory/Inventory'
import InactiveInventory from '@/components/Back/Inventory/InactiveInventory'
import NewInventory from '@/components/Back/Inventory/NewInventory'
import SingleInventory from '@/components/Back/Inventory/SingleInventory'
import Availability from '@/components/Back/Inventory/Availability'
import InventorySettings from '@/components/Back/Inventory/InventorySettings'

// Membership
import Member from '@/components/Back/Membership/Member'
import Members from '@/components/Back/Membership/Members'
import NewMember from '@/components/Back/Membership/NewMember'
import MembershipSettings from '@/components/Back/Membership/MembershipSettings'

// Visitors
import NewVisitor from '@/components/Back/Visitors/NewVisitor'
import Visitor from '@/components/Back/Visitors/Visitor'
import VisitorHistory from '@/components/Back/Visitors/VisitorHistory'
import Visitors from '@/components/Back/Visitors/Visitors'
import VisitorSettings from '@/components/Back/Visitors/VisitorSettings'

// Partners
import NewPartner from '@/components/Back/Partners/NewPartner'
import Partner from '@/components/Back/Partners/Partner'
import Partners from '@/components/Back/Partners/Partners'
import PartnerSettings from '@/components/Back/Partners/PartnerSettings'
import PartnerBookings from '@/components/Back/Partners/PartnerBookings'

// Facilities
import Facility from '@/components/Back/Facilities/Facility'
import NewFacility from '@/components/Back/Facilities/NewFacility'
import Facilities from '@/components/Back/Facilities/Facilities'
import FacilitySettings from '@/components/Back/Facilities/FacilitiesSettings'
import InactiveFacilities from '@/components/Back/Facilities/InactiveFacilities'

// Taxes
import CateringLevyHistory from '@/components/Back/Taxes/CateringLevyHistory'
import UpdateCateringLevy from '@/components/Back/Taxes/UpdateCateringLevy'
import UpdateVat from '@/components/Back/Taxes/UpdateVat'
import VatHistory from '@/components/Back/Taxes/VatHistory'

// Reports
import BookingsReport from '@/components/Back/Reports/BookingsReport'
import CurrencyReport from '@/components/Back/Reports/CurrencyReport'
import PaymentsReport from '@/components/Back/Reports/PaymentsReport'
import VisitorsReport from '@/components/Back/Reports/VisitorsReport'
import CSV from '@/components/Back/Reports/CSV'

// Donations
import AddDonation from '@/components/Back/Donations/AddDonation'
import Donation from '@/components/Back/Donations/Donation'
import Donations from '@/components/Back/Donations/Donations'
import DonationSettings from '@/components/Back/Donations/DonationSettings'

// Doners
import AddDoner from '@/components/Back/Doners/AddDoner'
import Doner from '@/components/Back/Doners/Doner'
import Doners from '@/components/Back/Doners/Donners'

// User
import AddUser from '@/components/Back/Users/AddUser'
import User from '@/components/Back/Users/User'
import Users from '@/components/Back/Users/Users'
import UserTypes from '@/components/Back/Users/UserTypes'
import Roles from '@/components/Back/Users/Roles'
import Features from '@/components/Back/Users/Features'

// Staff
import AddStaff from '@/components/Back/Staff/AddStaff'
import AllStaff from '@/components/Back/Staff/AllStaff'
import StaffSettings from '@/components/Back/Staff/StaffSettings'

// Volunteer
import NewVolunteerProgram from '@/components/Back/VolunteerProgram/NewVolunteerProgram'
import VolunteerProgram from '@/components/Back/VolunteerProgram/VolunteerProgram'
// import VolunteerProgramSettings from '@/components/Back/VolunteerProgram/VolunteerProgramSettings'
import VolunteerPrograms from '@/components/Back/VolunteerProgram/VolunteerPrograms'
import SingleVolunteerApplicant from '@/components/Back/VolunteerProgram/SingleVolunteerApplication'

// Settings
import CallToActions from '@/components/Back/Settings/CallToActions'
import ConservancyFees from '@/components/Back/Settings/ConservancyFees'
import Pages from '@/components/Back/Settings/Pages'
import Page from '@/components/Back/Settings/Page'
import PageTypes from '@/components/Back/Settings/PageTypes'
import TermsAndConditions from '@/components/Back/Settings/TermsAndConditions'
import VehicleFees from '@/components/Back/Settings/VehicleFees'
import SalesForce from '@/components/Back/Settings/SalesForce'

// Currency
import AddCurrency from '@/components/Back/Currency/AddCurrency'
import CurrencyHistory from '@/components/Back/Currency/CurrencyHistory'
import UpdateCurrency from '@/components/Back/Currency/UpdateCurrency'
Vue.use(Router)

// App.vue level
export default new Router({
  routes: [
    {
      path: '/',
      component: Public,
      children: [
        {
          path: '/',
          name: 'Index',
          component: Index
        },
        {
          path: 'pt&c',
          name: 'PTermsAndConditions',
          component: PTermsAndConditions
        },
        {
          path: 'program-payment/:program_id',
          name: 'ProgramPayment',
          component: ProgramPayment,
          meta: { payment: true }
        },
        {
          path: 'reservation',
          name: 'Reservation',
          component: Reservation
        },
        {
          path: 'facility/:facility_id',
          name: 'PFacility',
          component: PFacility
        },
        {
          path: 'activity/:activity_id',
          name: 'PActivity',
          component: PActivity
        },
        {
          path: 'activity-selection',
          name: 'ActivitySelection',
          component: ActivitySelection
        },
        {
          path: 'express-selection',
          name: 'ExpressCheckout',
          component: ActivitySelection
        },
        {
          path: 'express-booking-info',
          name: 'ExpressBookingInformation',
          component: BookingInformation
        },
        {
          path: 'booking-info',
          name: 'BookingInformation',
          component: BookingInformation
        },
        {
          path: 'express-payment-info',
          name: 'ExpressPaymentInformation',
          component: PaymentInformation
        },
        {
          path: 'payment/info/:booking_id',
          name: 'PaymentInformation',
          component: PaymentInformation,
          meta: { payment: true }
        },
        {
          path: 'support-us',
          name: 'SupportUs',
          component: SupportUs
        },
        {
          path: 'support-us-payment/:donation_id',
          name: 'SupportPayment',
          component: SupportPayment,
          meta: { payment: true }
        },
        {
          path: 'volunteer-program',
          name: 'VolunteerProgram',
          component: PVolunteerProgram
        },
        {
          path: 'volunteer-program/:program_id',
          name: 'Program',
          component: Program
        },
        {
          path: 'membership-registration',
          name: 'RegisterMember',
          component: RegisterMember
        },
        {
          path: 'membership-payment/:member_id',
          name: 'MembershipPayment',
          component: MembershipPayment,
          meta: { payment: true }
        },
        {
          path: 'success/donation',
          name: 'SuccessDonation',
          component: SuccessDonation
        },
        {
          path: 'success/membership',
          name: 'SuccessMembership',
          component: SuccessMembership
        },
        {
          path: 'success/booking/:booking_id',
          name: 'BookingSuccess',
          component: BookingSuccess
        },
        {
          path: 'success/volunteer',
          name: 'SuccessVolunteer',
          component: SuccessVolunteer
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/reset-password',
      name: 'ResetPassword',
      component: ResetPassword
    },
    {
      path: '/password/reset/:reset_id',
      name: 'PasswordReset',
      component: PasswordReset
    },
    {
      path: '/',
      component: Back,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta: { requiresAuth: true }
        },
        // Settings
        {
          path: 'sales-force',
          name: 'SalesForce',
          component: SalesForce,
          meta: { requiresAuth: true }
        },
        {
          path: 'call-to-action',
          name: 'CallToActions',
          component: CallToActions,
          meta: { requiresAuth: true }
        },
        {
          path: 'conservation-fees',
          name: 'ConservancyFees',
          component: ConservancyFees,
          meta: { requiresAuth: true }
        },
        {
          path: 'add-page',
          name: 'Page',
          component: Page,
          meta: { requiresAuth: true }
        },
        {
          path: 'extra-pages',
          name: 'Pages',
          component: Pages,
          meta: { requiresAuth: true }
        },
        {
          path: 'page/:page_id',
          name: 'Page',
          component: Page,
          meta: { requiresAuth: true }
        },
        {
          path: 'page-types',
          name: 'PageTypes',
          component: PageTypes,
          meta: { requiresAuth: true }
        },
        {
          path: 'terms-and-conditions',
          name: 'TermsAndConditions',
          component: TermsAndConditions,
          meta: { requiresAuth: true }
        },
        {
          path: 'vehicle-fees',
          name: 'VehicleFees',
          component: VehicleFees,
          meta: { requiresAuth: true }
        },
        // Volunteer Programs
        {
          path: 'volunteer-application/:applicant_id',
          name: 'SingleVolunteerApplicant',
          component: SingleVolunteerApplicant,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-volunterr-program/:program_id',
          name: 'EditProgram',
          component: NewVolunteerProgram,
          meta: { requiresAuth: true }
        },
        {
          path: 'add-new-program',
          name: 'NewVolunteerProgram',
          component: NewVolunteerProgram,
          meta: { requiresAuth: true }
        },
        {
          path: 'all-programs',
          name: 'VolunteerPrograms',
          component: VolunteerPrograms,
          meta: { requiresAuth: true }
        },
        {
          path: 'm-program/:program_id',
          name: 'MVolunteerProgram',
          component: VolunteerProgram,
          meta: { requiresAuth: true }
        },
        // {
        //   path: 'program-settings',
        //   name: 'VolunteerProgramSettings',
        //   component: VolunteerProgramSettings,
        //   meta: { requiresAuth: true }
        // },
        // Staff
        {
          path: 'add-staff',
          name: 'AddStaff',
          component: AddStaff,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-staff/:staff_id',
          name: 'EditStaff',
          component: AddStaff,
          meta: { requiresAuth: true }
        },
        {
          path: 'all-staff',
          name: 'AllStaff',
          component: AllStaff,
          meta: { requiresAuth: true }
        },
        {
          path: 'staff-settings',
          name: 'StaffSettings',
          component: StaffSettings,
          meta: { requiresAuth: true }
        },
        // Calendar
        {
          path: 'calendar',
          name: 'Calendar',
          component: Calendar,
          meta: { requiresAuth: true }
        },
        // Users
        {
          path: 'add-user',
          name: 'AddUser',
          component: AddUser,
          meta: { requiresAuth: true }
        },
        {
          path: 'users',
          name: 'Users',
          component: Users,
          meta: { requiresAuth: true }
        },
        {
          path: 'user/:user_id',
          name: 'User',
          component: User,
          meta: { requiresAuth: true }
        },
        {
          path: 'user-types',
          name: 'UserTypes',
          component: UserTypes,
          meta: { requiresAuth: true }
        },
        {
          path: 'roles/:role_id',
          name: 'Roles',
          component: Roles,
          meta: { requiresAuth: true }
        },
        {
          path: 'features/:menu_id',
          name: 'Features',
          component: Features,
          meta: { requiresAuth: true }
        },
        // Doners
        {
          path: 'add-doner',
          name: 'AddDoner',
          component: AddDoner,
          meta: { requiresAuth: true }
        },
        {
          path: 'doner/:doner_id',
          name: 'Doner',
          component: Doner,
          meta: { requiresAuth: true }
        },
        {
          path: 'doners',
          name: 'Doners',
          component: Doners,
          meta: { requiresAuth: true }
        },
        // Donations
        {
          path: 'donations',
          name: 'Donations',
          component: Donations,
          meta: { requiresAuth: true }
        },
        {
          path: 'donation/:donation_id',
          name: 'Donation',
          component: Donation,
          meta: { requiresAuth: true }
        },
        {
          path: 'add-donation',
          name: 'AddDonation',
          component: AddDonation,
          meta: { requiresAuth: true }
        },
        {
          path: 'donation-settings',
          name: 'DonationSettings',
          component: DonationSettings,
          meta: { requiresAuth: true }
        },
        // Reports
        {
          path: 'csv',
          name: 'CSV',
          component: CSV,
          meta: { requiresAuth: true }
        },
        {
          path: 'booking-reports',
          name: 'BookingsReport',
          component: BookingsReport,
          meta: { requiresAuth: true }
        },
        {
          path: 'currency-reports',
          name: 'CurrencyReport',
          component: CurrencyReport,
          meta: { requiresAuth: true }
        },
        {
          path: 'payment-reports',
          name: 'PaymentsReport',
          component: PaymentsReport,
          meta: { requiresAuth: true }
        },
        {
          path: 'visitors-reports',
          name: 'VisitorsReport',
          component: VisitorsReport,
          meta: { requiresAuth: true }
        },
        // Taxes
        {
          path: 'catering-levy-history',
          name: 'CateringLevyHistory',
          component: CateringLevyHistory,
          meta: { requiresAuth: true }
        },
        {
          path: 'update-catering-levy',
          name: 'UpdateCateringLevy',
          component: UpdateCateringLevy,
          meta: { requiresAuth: true }
        },
        {
          path: 'update-vat',
          name: 'UpdateVat',
          component: UpdateVat,
          meta: { requiresAuth: true }
        },
        {
          path: 'vat-history',
          name: 'VatHistory',
          component: VatHistory,
          meta: { requiresAuth: true }
        },
        // Currency
        {
          path: 'currency-history',
          name: 'CurrencyHistory',
          component: CurrencyHistory,
          meta: { requiresAuth: true }
        },
        {
          path: 'add-currency',
          name: 'AddCurrency',
          component: AddCurrency,
          meta: { requiresAuth: true }
        },
        {
          path: 'update-currency',
          name: 'UpdateCurrency',
          component: UpdateCurrency,
          meta: { requiresAuth: true }
        },
        // Facilities
        {
          path: 'm-facility/:facility_id',
          name: 'Facility',
          component: Facility,
          meta: { requiresAuth: true }
        },
        {
          path: 'new-facility',
          name: 'NewFacility',
          component: NewFacility,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-facility/:facility_id',
          name: 'EditFacility',
          component: NewFacility,
          meta: { requiresAuth: true }
        },
        {
          path: 'facilities',
          name: 'Facilities',
          component: Facilities,
          meta: { requiresAuth: true }
        },
        {
          path: 'active-facilities',
          name: 'ActiveFacilities',
          component: Facilities,
          meta: { requiresAuth: true }
        },
        {
          path: 'inactive-facilities',
          name: 'InactiveFacilities',
          component: InactiveFacilities,
          meta: { requiresAuth: true }
        },
        {
          path: 'facilities-settings',
          name: 'FacilitySettings',
          component: FacilitySettings,
          meta: { requiresAuth: true }
        },
        // Partners
        {
          path: 'partner/:partner_id',
          name: 'Partner',
          component: Partner,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-partner/:partner_id',
          name: 'EditPartner',
          component: NewPartner,
          meta: { requiresAuth: true }
        },
        {
          path: 'new-partner',
          name: 'NewPartner',
          component: NewPartner,
          meta: { requiresAuth: true }
        },
        {
          path: 'partners',
          name: 'Partners',
          component: Partners,
          meta: { requiresAuth: true }
        },
        {
          path: 'partner-settings',
          name: 'PartnerSettings',
          component: PartnerSettings,
          meta: { requiresAuth: true }
        },
        {
          path: 'partner-bookings',
          name: 'PartnerBookings',
          component: PartnerBookings,
          meta: { requiresAuth: true }
        },
        // Booking
        {
          path: 'npartner-booking',
          name: 'NewPartnerBooking',
          component: NewPartnerBooking,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-booking/:booking_id',
          name: 'EditBooking',
          component: EditBooking,
          meta: { requiresAuth: true }
        },
        {
          path: 'new-booking',
          name: 'NewBooking',
          component: NewBooking,
          meta: { requiresAuth: true }
        },
        {
          path: 'incomplete-gate-tickets',
          name: 'GatePasses',
          component: GateTickets,
          meta: { requiresAuth: true }
        },
        {
          path: 'receipt/:booking_id',
          name: 'Receipt',
          component: Receipt,
          meta: { requiresAuth: true }
        },
        {
          path: 'p-receipt/:booking_id',
          name: 'PartnerReceipt',
          component: PartnerReceipt,
          meta: { requiresAuth: true }
        },
        {
          path: 'receipt-payment/:booking_id',
          name: 'GatePayment',
          component: GatePayment,
          meta: { requiresAuth: true }
        },
        {
          path: 'receipt-checkin/:booking_id',
          name: 'Checkin',
          component: Checkin,
          meta: { requiresAuth: true }
        },
        {
          path: 'bookings',
          name: 'Bookings',
          component: Bookings,
          meta: { requiresAuth: true }
        },
        {
          path: 'search-results',
          name: 'SearchResults',
          component: Bookings,
          meta: { requiresAuth: true }
        },
        {
          path: 'booking/:booking_id',
          name: 'Booking',
          component: Booking,
          meta: { requiresAuth: true }
        },
        {
          path: 'new-group-booking',
          name: 'GroupBooking',
          component: GroupBooking,
          meta: { requiresAuth: true }
        },
        {
          path: 'booking-settings',
          name: 'BookingSettings',
          component: BookingSettings,
          meta: { requiresAuth: true }
        },
        {
          path: 'confirmed-bookings',
          name: 'ConfirmedBookings',
          component: ConfirmedBookings,
          meta: { requiresAuth: true }
        },
        {
          path: 'un-confirmed-bookings',
          name: 'UnconfirmedBookings',
          component: UnconfirmedBookings,
          meta: { requiresAuth: true }
        },
        {
          path: 'find-booking',
          name: 'FindBooking',
          component: FindBooking,
          meta: { requiresAuth: true }
        },
        {
          path: 'checked-in',
          name: 'CheckedIn',
          component: CheckedIn,
          meta: { requiresAuth: true }
        },
        {
          path: 'checked-out',
          name: 'CheckedOut',
          component: CheckedOut,
          meta: { requiresAuth: true }
        },
        // Inventory
        {
          path: 'single-inventory/:inventory_id',
          name: 'SingleInventory',
          component: SingleInventory,
          meta: { requiresAuth: true }
        },
        {
          path: 'new-inventory',
          name: 'NewInventory',
          component: NewInventory,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-inventory/:inventory_id',
          name: 'EditInventory',
          component: NewInventory,
          meta: { requiresAuth: true }
        },
        {
          path: 'inventory',
          name: 'Inventory',
          component: Inventories,
          meta: { requiresAuth: true }
        },
        {
          path: 'active-inventory',
          name: 'ActiveInventory',
          component: Inventories,
          meta: { requiresAuth: true }
        },
        {
          path: 'inactive-inventory',
          name: 'InactiveInventory',
          component: InactiveInventory,
          meta: { requiresAuth: true }
        },
        {
          path: 'availability',
          name: 'Availability',
          component: Availability,
          meta: { requiresAuth: true }
        },
        {
          path: 'inventory-setting',
          name: 'InventorySettings',
          component: InventorySettings,
          meta: { requiresAuth: true }
        },
        // Membership
        {
          path: 'new-membership',
          name: 'NewMember',
          component: NewMember,
          meta: { requiresAuth: true }
        },
        {
          path: 'members',
          name: 'Members',
          component: Members,
          meta: { requiresAuth: true }
        },
        {
          path: 'member/:member_id',
          name: 'Member',
          component: Member,
          meta: { requiresAuth: true }
        },
        {
          path: 'membership-settings',
          name: 'MembershipSettings',
          component: MembershipSettings,
          meta: { requiresAuth: true }
        },
        // Visitors
        {
          path: 'new-visitor',
          name: 'NewVisitor',
          component: NewVisitor,
          meta: { requiresAuth: true }
        },
        {
          path: 'visitors',
          name: 'Visitors',
          component: Visitors,
          meta: { requiresAuth: true }
        },
        {
          path: 'visitor/:visitor_id',
          name: 'Visitor',
          component: Visitor,
          meta: { requiresAuth: true }
        },
        {
          path: 'visitors-history',
          name: 'VisitorHistory',
          component: VisitorHistory,
          meta: { requiresAuth: true }
        },
        {
          path: 'visitors-settings',
          name: 'VisitorSettings',
          component: VisitorSettings,
          meta: { requiresAuth: true }
        },
        {
          path: 'edit-visitor/:visitor_id',
          name: 'EditVisitor',
          component: NewVisitor,
          meta: { requiresAuth: true }
        }
      ]
    }
  ],
  mode: 'history'
})
