export default {
  computed: {
    allPageTypes: function () {
      return this.$store.state.PageTypeStore.all_page_types
    }
  }
}
