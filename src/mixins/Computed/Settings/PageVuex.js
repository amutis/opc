export default {
  computed: {
    singlePage: function () {
      return this.$store.state.PageStore.page
    },
    updatePage: function () {
      this.page.name = this.$store.state.PageStore.page.name
      this.page.description = this.$store.state.PageStore.page.description
    }
  }
}
