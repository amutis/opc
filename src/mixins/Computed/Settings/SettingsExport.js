import Vue from 'vue'

import CallToActionVuex from './CallToActionVuex'
import PageTypeVuex from './PageTypeVuex'
import PageVuex from './PageVuex'

Vue.mixin(CallToActionVuex)
Vue.mixin(PageTypeVuex)
Vue.mixin(PageVuex)
