export default {
  computed: {
    allVisitorTypes: function () {
      return this.$store.state.VisitorStore.visitor_types
    },
    allVisitors: function () {
      return this.$store.state.VisitorStore.all_visitors
    },
    currentVisitors: function () {
      return this.$store.state.VisitorStore.open_visitors
    },
    singleVisitor: function () {
      return this.$store.state.VisitorStore.visitor
    },
    updatingVisitor: function () {
      if (this.$route.name === 'EditVisitor') {
        this.visitor.id_number = this.$store.state.VisitorStore.visitor.visitor_identification_number
        this.visitor.plate = this.$store.state.VisitorStore.visitor.visitor_vehicle_plate
        this.visitor.count = this.$store.state.VisitorStore.visitor.visitor_count
        this.visitor.organisation = this.$store.state.VisitorStore.visitor.visitor_organisation
        this.visitor.purpose = this.$store.state.VisitorStore.visitor.visitor_purpose
        this.visitor.staff = this.$store.state.VisitorStore.visitor.visitor_staff_responsible
        this.visitor.phone = this.$store.state.VisitorStore.visitor.visitor_phone_number
        this.visitor.name = this.$store.state.VisitorStore.visitor.visitor_name
        this.visitor.id_type = this.$store.state.VisitorStore.visitor.visitor_identification_doc_type_id
        this.visitor.type = this.$store.state.VisitorStore.visitor.visitor_type_id
        this.visitor.visitor_id = this.$store.state.VisitorStore.visitor.visitor_public_id
      }
    }
  }
}
