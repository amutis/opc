import Vue from 'vue'

import CountriesGendersVuex from './CountriesGendersVuex'
import ProgramsVuex from './ProgramsVuex'
import VolunteerVuex from './VolunteerVuex'
import VolunteerContactVuex from './VolunteerContactVuex'

Vue.mixin(CountriesGendersVuex)
Vue.mixin(ProgramsVuex)
Vue.mixin(VolunteerVuex)
Vue.mixin(VolunteerContactVuex)
