export default {
  computed: {
    $allVolunteerContacts: function () {
      return this.$store.state.VolunteerContactStore.all_volunteer_contacts
    }
  }
}
