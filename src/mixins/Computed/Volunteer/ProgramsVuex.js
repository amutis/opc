export default {
  computed: {
    updateProgram: function () {
      var sProgram = this.singleProgram
      if (this.$route.name === 'EditProgram') {
        this.program.public_id = this.$route.params.program_id
        this.program.start_date = this.$moment(sProgram.start_date).format('YYYY-MM-DD')
        this.program.end_date = this.$moment(sProgram.end_date).format('YYYY-MM-DD')
        this.program.name = sProgram.name
        this.program.currency = sProgram.currency
        this.program.price = sProgram.price
        this.program.desc = sProgram.desc
        this.program.max_applicants = sProgram.max_applicants
        this.program.session = localStorage.getItem('lkjhgfdsa')
        // this.singleProgram
        return null
      }
    },
    allPrograms: function () {
      return this.$store.state.ProgramStore.all_programs
    },
    singleProgram: function () {
      return this.$store.state.ProgramStore.program
    },
    singleVolunteer () {
      return this.$store.state.VolunteerStore.volunteer
    },
    programVolunteers: function () {
      return this.$store.state.ProgramStore.program_voluntees
    },
    allGender: function () {
      return this.$store.state.GenderStore.all_genders
    },
    allCountries: function () {
      return this.$store.state.AuthenticationStore.all_countries
    }
  }
}
