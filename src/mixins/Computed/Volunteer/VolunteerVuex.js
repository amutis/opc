export default {
  computed: {
    singleVolunteer: function () {
      return this.$store.state.VolunteerStore.volunteer
    }
  }
}
