import Vue from 'vue'

import UserVuex from './UserVuex'
import UserTypesVuex from './UserTypesVuex'

Vue.mixin(UserVuex)
Vue.mixin(UserTypesVuex)
