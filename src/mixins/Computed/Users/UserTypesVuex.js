import Menu from '../../../mixins/Functions/Menu'
export default {
  computed: {
    allUserTypes: function () {
      if (this.menu === 4) {
        this.user.user_type = this.$store.state.UserTypeStore.all_user_types[3].public_id
      }
      return this.$store.state.UserTypeStore.all_user_types
    },
    currenctRoles: function () {
      return this.$store.state.UserTypeStore.user_type_features
    },
    allRoles: function () {
      return this.$store.state.FeatureStore.all_features
    },
    unAssigned: function () {
      return this.$store.state.UserTypeStore.not_assigned_features
    },
    $singleUserType: function () {
      return this.$store.state.UserTypeStore.user_type
    }
  },
  mixins: [Menu]
}
