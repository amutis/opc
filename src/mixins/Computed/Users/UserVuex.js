export default {
  computed: {
    allUsers: function () {
      return this.$store.state.UserStore.all_users
    },
    Users: function () {
      return this.$store.state.UserStore.users
    },
    singleUser: function () {
      return this.$store.state.UserStore.user
    },
    UpdateUser: function () {
      this.user.user_type = this.$store.state.UserStore.user.user_type_public_id
      this.user.first_name = this.$store.state.UserStore.user.first_name
      this.user.last_name = this.$store.state.UserStore.user.last_name
      this.user.email = this.$store.state.UserStore.user.email
      this.user.phone_number = this.$store.state.UserStore.user.phone_number
    }
  }
}
