export default {
  computed: {
    allAnonimity: function () {
      var allAnonimity = this.$store.state.AnonymityStore.all_anonymity
      if (this.$route.name === 'SupportUs' && allAnonimity.length > 0) {
        this.donation.anonymity = allAnonimity[0].public_id
      }
      return allAnonimity
    },
    allFrequency: function () {
      var allFrequency = this.$store.state.PaymentFrequencyStore.all_frequency
      if (this.$route.name === 'SupportUs' && allFrequency.length > 0) {
        this.donation.frequency = allFrequency[0].public_id
      }
      return allFrequency
    },
    allPaymentMethods: function () {
      return this.$store.state.DonationPaymentMethodStore.all_payment_methods
    },
    allCauses: function () {
      return this.$store.state.CauseStore.all_causes
    },
    allCountries: function () {
      return this.$store.state.CountryStore.all_countries
    },
    getDonation: function () {
      return this.$store.state.DonationStore.donation
    },
    allDonations: function () {
      return this.$store.state.DonationStore.all_donations
    }
  }
}
