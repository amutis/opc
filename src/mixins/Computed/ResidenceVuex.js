export default {
  computed: {
    allResidenceTypes: function () {
      return this.$store.state.ResidenceTypeStore.all_residence_types
    },
    selectedResidence: function () {
      return this.$store.state.ResidenceTypeStore.residence_types
    }
  }
}
