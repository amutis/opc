export default {
  computed: {
    allVehiclePrices: function () {
      return this.$store.state.BookingStore.vehicle_entry
    },
    vehiclePricing: function () {
      var small = this.vehicle.seater6
      var middle = this.vehicle.seater14
      var large = this.vehicle.seater60
      if (this.allVehiclePrices.length > 0) {
        small = small * this.allVehiclePrices[0].vehicle_charge_category_cost
        middle = middle * this.allVehiclePrices[1].vehicle_charge_category_cost
        large = large * this.allVehiclePrices[2].vehicle_charge_category_cost
      }
      var total = small + middle + large
      if (this.$store.state.CurrencyStore.browser_currency === 'KES') {
        return 'KES ' + (Number(total * localStorage.getItem('base'))).toLocaleString('en')
      } else if (this.$store.state.CurrencyStore.browser_currency === 'USD') {
        return '$' + (Number(total)).toLocaleString('en')
      }
    },
    vehiclePricingSide: function () {
      if (this.$store.state.CurrencyStore.browser_currency === 'KES') {
        return 'KES ' + (this.vehiclePricingSideTotal).toLocaleString('en')
      } else if (this.$store.state.CurrencyStore.browser_currency === 'USD') {
        return '$' + (this.vehiclePricingSideTotal).toLocaleString('en')
      }
    },
    vehiclePricingSideTotal: function () {
      var vehicles = this.$store.state.FacilityStore.browser_vehicles
      var small = vehicles.seater6
      var middle = vehicles.seater14
      var large = vehicles.seater60
      if (this.allVehiclePrices.length > 0) {
        small = small * this.allVehiclePrices[0].vehicle_charge_category_cost
        middle = middle * this.allVehiclePrices[1].vehicle_charge_category_cost
        large = large * this.allVehiclePrices[2].vehicle_charge_category_cost
      }
      var total = small + middle + large
      if (this.$store.state.CurrencyStore.browser_currency === 'KES') {
        return (Number(total) * this.bookingDays)
      } else if (this.$store.state.CurrencyStore.browser_currency === 'USD') {
        return (Number(total * this.bookingDays / localStorage.getItem('base')))
      }
    },
    facilityPricing: function () {
      return this.$store.state.PricingStore.pricing
    },
    inventoryTotal: function () {
      var inventory = this.$store.state.PricingStore.inventory_selection
      // console.log(inventory)
      var size = inventory.length
      var inventoryTotal = 0
      var localCurrency = this.$store.state.CurrencyStore.browser_currency
      if (size > 0) {
        for (var i = 0; i < size; i++) {
          if (localCurrency === 'KES' && inventory[i].currency === 'KES') {
            inventoryTotal = inventoryTotal + inventory[i].total
          } else if (localCurrency === 'USD' && inventory[i].currency === 'KES') {
            inventoryTotal = inventoryTotal + (inventory[i].total / localStorage.getItem('base'))
          } else if (localCurrency === 'USD' && inventory[i].currency === 'USD') {
            inventoryTotal = inventoryTotal + inventory[i].total
          } else if (localCurrency === 'KES' && inventory[i].currency === 'USD') {
            inventoryTotal = inventoryTotal + ((inventory[i].total) * localStorage.getItem('base'))
            // console.log(i + ': ' + inventoryTotal)
          }
        }
        return inventoryTotal
      } else {
        return 0
      }
    },
    bookingDays: function () {
      return this.$store.state.BookingStore.local_booking.days
    },
    gateEntry: function () {
      var citizenAdult = this.$store.state.BookingStore.gate_entry.payment_citizen_adult_amount
      var citizenChild = this.$store.state.BookingStore.gate_entry.payment_citizen_child_amount
      // var citizen_student = this.$store.state.BookingStore.gate_entry.payment_citizen_student_amount
      var eaResidentAdult = this.$store.state.BookingStore.gate_entry.payment_ea_resident_adult_amount
      var eaResidentChild = this.$store.state.BookingStore.gate_entry.payment_ea_resident_child_amount
      // var ea_resident_student = this.$store.state.BookingStore.gate_entry.payment_ea_resident_student_amount
      var nonResidentAdult = this.$store.state.BookingStore.gate_entry.payment_non_resident_adult_amount
      var nonResidentChild = this.$store.state.BookingStore.gate_entry.payment_non_resident_child_amount
      // var non_resident_student = this.$store.state.BookingStore.gate_entry.payment_non_resident_student_amount
      var localBooking = this.$store.state.BookingStore.local_booking
      citizenAdult = citizenAdult * localBooking.citizenAdults
      citizenChild = citizenChild * localBooking.citizenChildren
      eaResidentAdult = eaResidentAdult * localBooking.residentAdults
      eaResidentChild = eaResidentChild * localBooking.residentChildren
      nonResidentAdult = nonResidentAdult * localBooking.nonResidentAdults
      nonResidentChild = nonResidentChild * localBooking.nonResidentChildren

      var kenyanCurrency = citizenAdult + citizenChild + eaResidentAdult + eaResidentChild + (nonResidentAdult * localStorage.getItem('base')) + (nonResidentChild * localStorage.getItem('base')) * this.bookingDays
      var usdCurrency = (citizenAdult / localStorage.getItem('base')) + (citizenChild / localStorage.getItem('base')) + (eaResidentAdult / localStorage.getItem('base')) + (eaResidentChild / localStorage.getItem('base')) + nonResidentAdult + nonResidentChild
      if (this.$store.state.CurrencyStore.browser_currency === 'KES') {
        return 'KES ' + (kenyanCurrency * this.bookingDays).toLocaleString('en')
      } else if (this.$store.state.CurrencyStore.browser_currency === 'USD') {
        return '$' + (usdCurrency * this.bookingDays).toLocaleString('en')
      }
    },
    gateEntryl: function () {
      var citizenAdult = this.$store.state.BookingStore.gate_entry.payment_citizen_adult_amount
      var citizenChild = this.$store.state.BookingStore.gate_entry.payment_citizen_child_amount
      // var citizen_student = this.$store.state.BookingStore.gate_entry.payment_citizen_student_amount
      var eaResidentAdult = this.$store.state.BookingStore.gate_entry.payment_ea_resident_adult_amount
      var eaResidentChild = this.$store.state.BookingStore.gate_entry.payment_ea_resident_child_amount
      // var ea_resident_student = this.$store.state.BookingStore.gate_entry.payment_ea_resident_student_amount
      var nonResidentAdult = this.$store.state.BookingStore.gate_entry.payment_non_resident_adult_amount
      var nonResidentChild = this.$store.state.BookingStore.gate_entry.payment_non_resident_child_amount
      // var non_resident_student = this.$store.state.BookingStore.gate_entry.payment_non_resident_student_amount
      var localBooking = this.$store.state.BookingStore.local_booking
      citizenAdult = citizenAdult * localBooking.citizenAdults
      citizenChild = citizenChild * localBooking.citizenChildren
      eaResidentAdult = eaResidentAdult * localBooking.residentAdults
      eaResidentChild = eaResidentChild * localBooking.residentChildren
      nonResidentAdult = nonResidentAdult * localBooking.nonResidentAdults
      nonResidentChild = nonResidentChild * localBooking.nonResidentChildren

      var kenyanCurrency = citizenAdult + citizenChild + eaResidentAdult + eaResidentChild + (nonResidentAdult * localStorage.getItem('base')) + (nonResidentChild * localStorage.getItem('base'))
      var usdCurrency = (citizenAdult / localStorage.getItem('base')) + (citizenChild / localStorage.getItem('base')) + (eaResidentAdult / localStorage.getItem('base')) + (eaResidentChild / localStorage.getItem('base')) + nonResidentAdult + nonResidentChild
      if (this.$store.state.CurrencyStore.browser_currency === 'KES') {
        return (kenyanCurrency * this.bookingDays)
      } else if (this.$store.state.CurrencyStore.browser_currency === 'USD') {
        return (usdCurrency * this.bookingDays)
      }
    },
    totalPriceCurrency: function () {
      var localCurrency = this.$store.state.CurrencyStore.browser_currency
      if (localCurrency === 'USD') {
        localCurrency = '$'
      }
      return localCurrency
    },
    totalPrice: function () {
      var localCurrency = this.$store.state.CurrencyStore.browser_currency
      var total = this.inventoryTotal + Number(this.facilityTotal) + Number(this.gateEntryl) + Number(this.vehiclePricingSideTotal)
      if (localCurrency === 'USD') {
        localCurrency = '$'
      }
      return localCurrency + ' ' + total.toLocaleString('en')
    },
    paymentTotalPrice: function () {
      var localCurrency = this.$store.state.CurrencyStore.browser_currency
      var total = this.inventoryTotal + Number(this.facilityTotal) + Number(this.gateEntryl) + Number(this.vehiclePricingSideTotal)
      if (localCurrency === 'USD') {
        return total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      } else if (localCurrency === 'KES') {
        total = total / localStorage.getItem('base')
        // total = total.toFixed(2)
        return total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      }
    }
  }
}
