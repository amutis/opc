export default {
  computed: {
    AllIdentificationTypes: function () {
      return this.$store.state.IdentificationTypeStore.all_identification_types
    }
  }
}
