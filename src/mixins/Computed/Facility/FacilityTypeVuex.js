export default {
  computed: {
    allFacilityTypes: function () {
      return this.$store.state.FacilityTypeStore.all_facility_types
    },
    singleFacility: function () {
      var index = this.$route.params.facility_id
      return this.$store.state.FacilityStore.facilities[index]
    },
    accommodationTypes: function () {
      return this.$store.state.AccommodationTypeStore.all_accommodation_types
    }
  }
}
