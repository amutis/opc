import CurrencyConversion from '../../Functions/Currency'
export default {
  methods: {
    removeFacility: function (facility) {
      var localFacility = JSON.parse(localStorage.getItem('facility_data'))
      localFacility.splice(localFacility.indexOf(facility), 1)
      this.$store.commit('BROWSER_FACILITIES', localFacility)
      this.$store.commit('REMOVE_FACILITY', facility)
      localStorage.setItem('facility_data', JSON.stringify(localFacility))
    },
    facilityDifference: function (facility) {
      return this.facilityTotal(facility) - this.facilityDiscount(facility) - this.facilityCommission(facility)
    },
    facilityDiscount: function (facility) {
      if (facility.discount_status === 'true') {
        var discounts = facility.discounts
        var discount = 0
        var facilityD = 0
        for (var i = 0; i < discounts.length; i++) {
          if (this.gate_pass.tour_operator.type_public_id === discounts[i].discount_type_id) {
            discount = discounts[i].discount
          }
        }
        facilityD = this.facilityTotal(facility) * Number(discount) / 100
        return facilityD
      } else {
        return 0
      }
    },
    facilityCommission: function (facility) {
      if (facility.discount_status === 'true') {
        var discounts = facility.discounts
        var discount = 0
        var facilityD = 0
        for (var i = 0; i < discounts.length; i++) {
          if (this.gate_pass.tour_operator.type_public_id === discounts[i].discount_type_id) {
            discount = discounts[i].commission
          }
        }
        facilityD = this.facilityTotal(facility) * Number(discount) / 100
        return facilityD
      } else {
        return 0
      }
    },
    getDays: function (startDate, endDate) {
      if (startDate === endDate) {
        return 1
      } else {
        var b = this.$moment(endDate)
        var a = this.$moment(startDate)
        return b.diff(a, 'days')
      }
    },
    developPricePacks (facility) {
      var packs = []
      facility.price.map(function (item, index) {
        if (item.hasOwnProperty('lower_limit_list') && item.lower_limit_list.length > 0) {
          // merging currency and limits
          item.lower_limit_list.map(function (pack, index) {
            var currency = {currency: item.lower_currency[index]}
            Object.assign(pack, currency)
            packs.push(pack)
          })
        }
      })
      console.log(packs)
      return packs
    },
    facilityTotal: function (facility) {
      var total = 0
      var children = 0
      var currencyIndex = 0
      var days = this.getDays(facility.start_date, facility.end_date)
      if (facility.facility_type === 'Accomodation' && facility.accommodation_type === 'Pelican') {
        var totalGuests = Number(facility.adults) + Number(facility.children)
        // If it has special pricing
        if ('extra_price' in facility) {
          var excessPrice = 0
          // Check if number exceeds
          if (totalGuests > facility.to_number) {
            // Excess number of people
            var excess = totalGuests - facility.to_number
            excessPrice = Number(excess) * Number(facility.extra_price)
          }
          var limitPrice = facility.special_mandatory_price
          total = Number(limitPrice) + Number(excessPrice)
          total = total * days
          total = this.convert1(facility.special_currency, total).amount
          return total
        } else {
          // Self Catered
          if (facility.catering_type === '9a9a77eb') {
            facility.pack_prices.lower_limit_list.map(function (price, index) {
              if (Number(price.lower_limit) <= Number(totalGuests) && Number(price.upper_limit) >= Number(totalGuests)) {
                total = Number(price.price)
                children = Number(price.child_price)
                currencyIndex = index
              }
            })
            console.log(total)
            total = this.convert1(facility.pack_prices.lower_currency[currencyIndex], total).amount
            total = total * days * Number(facility.adults)
            total = total + (days * this.convert1(facility.pack_prices.lower_currency[currencyIndex], children).amount * Number(facility.children))
          } else if (facility.catering_type === '266677c3') {
            facility.pack_prices.upper_upper_limit_list.map(function (price, index) {
              if (Number(price.lower_limit) <= Number(totalGuests) && Number(price.upper_limit) >= Number(totalGuests)) {
                total = Number(price.price)
                children = Number(price.child_price)
                currencyIndex = index
              }
            })
            total = this.convert1(facility.pack_prices.upper_currency[currencyIndex], total).amount
            total = total * days * Number(facility.adults)
            total = total + (days * this.convert1(facility.pack_prices.upper_currency[currencyIndex], children).amount * Number(facility.children))
          }
          if (isNaN(total) === true) {
            total = 0
          }
          return total
        }
      } else if (facility.facility_type === 'Camping Sites') {
        var childTotal = Number(facility.children) * Number(facility.child_price)
        var adultTotal = Number(facility.adults) * Number(facility.adult_price)
        var mandatoryPrice = Number(facility.mandatory_price)
        mandatoryPrice = Number(mandatoryPrice * Math.ceil(days / 7))
        childTotal = childTotal * days
        adultTotal = adultTotal * days
        total = childTotal + adultTotal + mandatoryPrice
        total = this.convert1(facility.currency, total).amount
        if (isNaN(total) === true) {
          total = 0
        }
        return total
      } else if (facility.facility_type === 'Accomodation') {
        var mandatoryPrice2 = Number(facility.mandatory_price)
        var days2 = this.getDays(facility.start_date, facility.end_date)
        mandatoryPrice2 = mandatoryPrice2 * days2
        total = mandatoryPrice2
        total = this.convert1(facility.currency, total).amount
        if (isNaN(total) === true) {
          total = 0
        }
        return total
      }
    }
  },
  computed: {
    allFacilityTotal: function () {
      var total = 0
      for (var x = 0; x < this.chosenFacilities.length; x++) {
        total = total + this.facilityTotal(this.chosenFacilities[x])
      }
      return total
    },
    facilitySchedule: function () {
      return this.$store.state.BookingStore.facility_schedule
    },
    chosenFacilities: function () {
      return this.$store.state.FacilityStore.chosenFacilities
    },
    facilityImages: function () {
      return this.$store.state.FacilityStore.facility_images
    },
    facilityPricing: function () {
      return this.$store.state.FacilityPricingStore.facility_pricing
    },
    facilityContacts: function () {
      return this.$store.state.FacilityContactStore.all_facility_contacts
    },
    allFacilities: function () {
      return this.$store.state.FacilityStore.active_facilities
    },
    inactiveFacilities: function () {
      return this.$store.state.FacilityStore.inactive_facilities
    },
    vehicleSelection: function () {
      return this.$store.state.FacilityStore.browser_vehicles
    },
    allCampsites: function () {
      return this.$store.state.FacilityStore.active_camping
    },
    allAccommodations: function () {
      return this.$store.state.FacilityStore.active_accommodations
    },
    browserFacilities: function () {
      // this.$store.commit('BROWSER_FACILITIES', JSON.parse(localStorage.getItem('facility_data')))
      return this.$store.state.FacilityStore.browser_facilities
    },
    singleFacility: function () {
      return this.$store.state.FacilityStore.facility
    },
    singlePricing: function () {
      return this.$store.state.PricingStore.pricing
    },
    updateFacility: function () {
      if (this.$route.name === 'EditFacility') {
        this.facility.description = this.singleFacility.description
        this.facility.name = this.singleFacility.name
        this.facility.code = this.singleFacility.code
        this.facility.analysis_code = this.singleFacility.analysis_code
        this.facility.maximum_guests = this.singleFacility.maximum_guests
        this.facility.facility_type_id = this.singleFacility.facility_type_id
        this.facility.accomodation_type_id = this.singleFacility.accomodation_type_id
        this.facility.quantity = this.singleFacility.quantity
      }
    }
  },
  mixins: [CurrencyConversion]
}
