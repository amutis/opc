export default {
  computed: {
    allFacilityCurrency: function () {
      return this.$store.state.FacilityCurrencyStore.all_facility_currencies
    }
  }
}
