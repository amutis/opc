import Vue from 'vue'

import FacilityCurrencyVuex from './FacilityCurrencyVuex'
import FacilityTypeVuex from './FacilityTypeVuex'
import FacilityVuex from './FacilityVuex'

Vue.mixin(FacilityCurrencyVuex)
Vue.mixin(FacilityTypeVuex)
Vue.mixin(FacilityVuex)
