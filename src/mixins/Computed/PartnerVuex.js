export default {
  computed: {
    allPartnerTypes: function () {
      return this.$store.state.PartnerTypeStore.all_partner_types
    },
    allPartners: function () {
      return this.$store.state.PartnerStore.all_partners
    },
    $tourOperators: function () {
      return this.allPartners.filter(function (partner) {
        return partner.type_public_id === '6bac8c28'
      })
    },
    partnerUsers: function () {
      return this.$store.state.UserStore.users
    },
    allPartnerBookings: function () {
      return this.$store.state.BookingStore.all_partner_bookings
    },
    partnerBookings: function () {
      return this.$store.state.BookingStore.partner_bookings
    },
    singlePartner: function () {
      return this.$store.state.PartnerStore.partner
    },
    updatePartner: function () {
      if (this.$route.name === 'EditPartner') {
        this.partner.partner_type = this.singlePartner.type_public_id
        this.partner.name = this.singlePartner.name
        this.partner.postal_address = this.singlePartner.postal_address
        this.partner.postal_code = this.singlePartner.postal_code
        this.partner.email = this.singlePartner.email
        this.partner.tel_number = this.singlePartner.tel
      }
    }
  }
}
