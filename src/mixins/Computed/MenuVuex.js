export default {
  computed: {
    allMenus: function () {
      return this.$store.state.MenuStore.all_menus
    },
    allFeatures: function () {
      return this.$store.state.FeatureStore.all_features[0].features
    }
  }
}
