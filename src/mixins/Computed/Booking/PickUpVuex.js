export default {
  computed: {
    allPickUpLocations () {
      return this.$store.state.PickUpLocationStore.all_pick_up_locations
    }
  }
}
