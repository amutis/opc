import CurrencyConversion from '../../Functions/Currency'
export default {
  methods: {
    getDays: function (startDate, endDate) {
      if (startDate === endDate) {
        return 1
      } else {
        var b = this.$moment(endDate)
        var a = this.$moment(startDate)
        var reducedToNights = b.diff(a, 'days') - 1
        if (reducedToNights >= 1) {
          return reducedToNights
        } else {
          return 1
        }
      }
    },
    getCoservationGuests: function () {
      var conservationFees
      if (this.$route.name === 'NewBooking' || this.$route.name === 'EditBooking' || this.$route.name === 'NewPartnerBooking') {
        conservationFees = this.allConservationFees
      } else {
        conservationFees = JSON.parse(localStorage.getItem('conservation_data'))
      }
      var size = conservationFees.length
      var guests = 0
      for (var i = 0; i < size; i++) {
        guests = Number(guests) + Number(conservationFees[i].payment_guests)
      }
      return guests
    },
    getGroupGuests: function () {
      var conservationFees
      if (this.$route.name === 'NewBooking') {
        conservationFees = this.allSchoolConservationFees
      } else {
        conservationFees = JSON.parse(localStorage.getItem('conservation_data'))
      }
      var size = conservationFees.length
      var guests = 0
      for (var i = 0; i < size; i++) {
        guests = Number(guests) + Number(conservationFees[i].payment_guests)
      }
      return guests
    },
    convertConservation: function (data) {
      var total = 0
      total = this.convert1(data.payment_person_currency, data.payment_person_amount).amount * Number(data.payment_guests)
      if (this.$route.name === 'Reservation' || this.$route.name === 'PaymentInformation') {
        var booking = JSON.parse(localStorage.getItem('user_data'))
        total = total * this.getDays(booking.check_in, booking.check_out)
      }
      return total
    },
    wholeNumber: function (n) {
      return Math.round(n)
    }
  },
  computed: {
    localConservationFees: function () {
      return this.$store.state.ConservationFeeStore.local_conservation
    },
    allConservationFees: function () {
      if (this.$route.name === 'NewBooking') {
        return this.$store.state.ConservationFeeStore.all_conservation_fees
      } else if (this.$route.name === 'EditBooking') {
        this.$store.commit('UPDATE_CONSERVATION_FEES', this.$store.state.BookingStore.booking.guests)
        return this.$store.state.ConservationFeeStore.all_conservation_fees
      } else {
        return this.$store.state.ConservationFeeStore.all_conservation_fees
      }
    },
    allSchoolConservationFees: function () {
      return this.$store.state.ConservationFeeStore.all_school_conservation_fees
    },
    allFullConservationFees: function () {
      return this.$store.state.ConservationFeeStore.all_full_conservation_fees
    },
    schoolConservationTotal: function () {
      var conservationFees
      conservationFees = this.allSchoolConservationFees
      var size = conservationFees.length
      var total = 0
      for (var i = 0; i < size; i++) {
        total = total + this.convert1(conservationFees[i].payment_person_currency, conservationFees[i].payment_person_amount).amount * Number(conservationFees[i].payment_guests) * ((100 - conservationFees[i].payment_person_discount) / 100)
      }
      total = total * this.getDays(this.gate_pass.check_in, this.gate_pass.check_out)
      // if (isNaN(total)) {
      //   total = 'No Check in and Checkout Dates'
      // }
      return this.wholeNumber(total)
    },
    conservationTotal: function () {
      var conservationFees
      if (this.$route.name === 'NewBooking') {
        conservationFees = this.allConservationFees
      } else {
        localStorage.setItem('conservation_data', JSON.stringify(this.allConservationFees))
        conservationFees = JSON.parse(localStorage.getItem('conservation_data'))
      }
      var size = conservationFees.length
      var total = 0
      for (var i = 0; i < size; i++) {
        total = total + this.convert1(conservationFees[i].payment_person_currency, conservationFees[i].payment_person_amount).amount * Number(conservationFees[i].payment_guests) * ((100 - conservationFees[i].payment_person_discount) / 100)
      }
      if (this.$route.name === 'NewBooking') {
        total = total * this.getDays(this.gate_pass.check_in, this.gate_pass.check_out)
      } else if (this.$route.name === 'EditBooking') {
        total = total * this.getDays(this.$store.state.BookingStore.booking.booking_check_in_date, this.$store.state.BookingStore.booking.booking_check_out_date)
      } else if (this.$route.name === 'Reservation' || this.$route.name === 'PaymentInformation') {
        var booking = JSON.parse(localStorage.getItem('user_data'))
        total = total * this.getDays(booking.check_in, booking.check_out)
      }
      // if (isNaN(total)) {
      //   total = 'No Check in and Checkout Dates'
      // }
      return this.wholeNumber(total)
    },
    conservationFeeDiscount: function () {
      var conservationFeeDiscount
      conservationFeeDiscount = (this.conservation_fee_discount / 100) * Number(this.conservationTotal)
      return conservationFeeDiscount
    },
    conservationDifference: function () {
      var conservationDifference
      conservationDifference = this.conservationTotal - this.conservationFeeDiscount
      return conservationDifference
    },
    conservationGuests: function () {
      var conservationFees = this.allConservationFees
      var size = conservationFees.length
      var total = 0
      for (var i = 0; i < size; i++) {
        total = total + Number(conservationFees[i].payment_guests)
      }
      return total
    }
  },
  mixins: [CurrencyConversion]
}
