import CurrencyConversion from '../../Functions/Currency'
export default {
  methods: {
    getDays: function (startDate, endDate) {
      if (startDate === endDate) {
        return 1
      } else {
        var b = this.$moment(endDate)
        var a = this.$moment(startDate)
        return b.diff(a, 'days')
      }
    },
    wholeNumber: function (n) {
      return Math.round(n)
    }
  },
  computed: {
    $vehicleCheckbox () {
      return this.$store.state.VehicleFeeStore.vehicle_separate
    },
    $vehicleDisabled () {
      return this.$store.state.VehicleFeeStore.disable_vehicles
    },
    localVehicles: function () {
      return this.$store.state.VehicleFeeStore.local_vehicles
    },
    allVehicles: function () {
      if (this.$route.name === 'EditBooking') {
        this.$store.commit('GET_ALL_VEHICLE_FEES', {data: this.$store.state.BookingStore.booking.vehicles})
        return this.$store.state.VehicleFeeStore.all_vehicle_fees
      } else {
        return this.$store.state.VehicleFeeStore.all_vehicle_fees
      }
    },
    vehicleTotal: function () {
      var vehicleFees
      var days = 0
      if (this.$route.name === 'NewBooking' || this.$route.name === 'NewPartnerBooking') {
        vehicleFees = this.allVehicles
        days = this.getDays(this.gate_pass.check_in, this.gate_pass.check_out)
      } else if (this.$route.name === 'EditBooking') {
        vehicleFees = this.allVehicles
        days = this.getDays(this.$store.state.BookingStore.booking.booking_check_in_date, this.$store.state.BookingStore.booking.booking_check_out_date)
      } else {
        days = this.getDays(JSON.parse(localStorage.getItem('user_data')).check_in, JSON.parse(localStorage.getItem('user_data')).check_out)
        localStorage.setItem('vehicle_data', JSON.stringify(this.allVehicles))
        vehicleFees = JSON.parse(localStorage.getItem('vehicle_data'))
      }
      var size = vehicleFees.length
      var total = 0
      for (var i = 0; i < size; i++) {
        total = total + this.convert1(vehicleFees[i].vehicle_charge_cost_currency, vehicleFees[i].vehicle_charge_category_cost).amount * Number(vehicleFees[i].vehicles) * ((100 - vehicleFees[i].discount) / 100)
      }
      return this.wholeNumber(Number(total * days))
    },
    totalVehicles: function () {
      var vehicleFees = this.allVehicles
      var size = vehicleFees.length
      var total = 0
      for (var i = 0; i < size; i++) {
        total = total + Number(vehicleFees[i].vehicles)
      }
      return this.wholeNumber(total)
    }
  },
  mixins: [CurrencyConversion]
}
