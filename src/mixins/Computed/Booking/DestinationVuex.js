export default {
  computed: {
    allDestinations: function () {
      return this.$store.state.DestinationStore.all_destinations
    }
  }
}
