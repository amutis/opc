export default {
  computed: {
    allBookingTypes: function () {
      return this.$store.state.BookingTypeStore.all_booking_types
    },
    allGroupBookingTypes: function () {
      return this.$store.state.BookingTypeStore.group_booking_types
    }
  }
}
