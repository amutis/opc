import Vue from 'vue'
import BookingTypeVuex from './BookingTypeVuex'
import BookingVuex from './BookingVuex'
import ConservationFeeVuex from './ConservationFeeVuex'
import DestinationVuex from './DestinationVuex'
import VehicleFee from './VehicleFee'
import PickUpLocation from './PickUpVuex'
import FacilityPricingType from './FacilityPricingType'

Vue.mixin(BookingTypeVuex)
Vue.mixin(BookingVuex)
Vue.mixin(ConservationFeeVuex)
Vue.mixin(DestinationVuex)
Vue.mixin(VehicleFee)
Vue.mixin(PickUpLocation)
Vue.mixin(FacilityPricingType)
