export default {
  data () {
    return {
      booking_status_filter: '',
      booking_types_filter: '',
      booking_guest_number_filter: '',
      booking_guest_type_filter: []
    }
  },
  methods: {
    $openPayment: function () {
      this.$router.push({
        name: 'GatePayment',
        params: {
          booking_id: this.$route.params.booking_id
        }
      })
    },
    adjustDates: function (checkIn, checkOut) {
      var userData = JSON.parse(localStorage.getItem('user_data'))
      userData.check_in = checkIn
      userData.check_out = checkOut
      localStorage.setItem('user_data', JSON.stringify(userData))
    },
    $findBookingStatus () {
      var status = ['No Filter']
      this.allBookings.map(function (booking) {
        status.push(booking.booking_status)
      })
      return status
    },
    $findBookingTypes () {
      var status = ['No Filter']
      this.allBookings.map(function (booking) {
        status.push(booking.booking_type)
      })
      return status
    },
    $findGuestNumbers () {
      var status = ['No Filter', '1', '2', '<= 3', '<= 5', '<= 10', '<= 25', '<= 50']
      return status
    }
  },
  computed: {
    $bookingPaidStatus () {
      if (this.singleBooking.booking_payment_balance > 0) {
        return false
      } else {
        return true
      }
    },
    updateGateEntry: function () {
      console.log(this.singleBooking)
      if (this.$route.name !== 'NewBooking') {
        this.gate_pass.check_in = this.$moment(this.singleBooking.booking_check_in_date).format('YYYY-MM-DD')
        this.gate_pass.check_out = this.$moment(this.singleBooking.booking_check_out_date).format('YYYY-MM-DD')
        this.gate_pass.currency = this.singleBooking.currency
        this.gate_pass.first_name = this.singleBooking.booking_details.first_name
        this.gate_pass.last_name = this.singleBooking.booking_details.last_name
        this.gate_pass.email = this.singleBooking.booking_details.email_address
        this.gate_pass.phone = this.singleBooking.booking_details.phone_number
        this.gate_pass.destination = this.singleBooking.destination_id
        var vehicleData = {data: this.singleBooking.vehicles}
        this.$store.commit('GET_ALL_VEHICLE_FEES', vehicleData)
        // Inventory
        // var instance = this
        console.log(this.singleBooking)
        // this.singleBooking.inventory_bookings.map(function (inventory) {
        //   console.log(inventory)
        // var data = {
        //   public_id: inventory.public_id,
        //   name: inventory.inventory_name,
        //   date: instance.$moment(inventory.inventory_booking_date).format('YYYY-MM-DD'),
        //   adults: inventory.inventory_booking_adults,
        //   adult_price: inventory.inventory_booking_adult_cost,
        //   child_price: inventory.inventory_booking_child_cost,
        //   currency: inventory.inventory_booking_currency,
        //   children: inventory.inventory_booking_children
        //   // discounts: this.activity.inventory.discount,
        //   // discount_status: this.activity.inventory.discount_status
        // }
        // instance.$store.commit('CHOSEN_ACTIVITIES', [])
        // instance.$store.commit('ADD_ACTIVITY', data)
        // })
      }
    },
    getCheckinCheckout: function () {
      var userData = JSON.parse(localStorage.getItem('user_data'))
      // this.$store.state.ConservationFeeStore.all_conservation_fees = JSON.parse(localStorage.getItem('conservation_data'))
      this.booking.check_in = userData.check_in
      this.booking.check_out = userData.check_out
    },
    pendingPartnerBookings: function () {
      return this.$store.state.BookingStore.pending_partner_bookings
    },
    pendingGroupBookings: function () {
      return this.$store.state.BookingStore.pending_group_bookings
    },
    allBookings: function () {
      return this.$store.state.BookingStore.all_bookings
    },
    $filterBooking () {
      var allBookings = this.allBookings
      if (this.booking_status_filter.length > 0 && this.booking_status_filter !== 'No Filter') {
        allBookings = allBookings.filter(booking => booking.booking_status === this.booking_status_filter)
      }
      if (this.booking_types_filter.length > 0 && this.booking_types_filter !== 'No Filter') {
        allBookings = allBookings.filter(booking => booking.booking_type === this.booking_types_filter)
      }
      if (this.booking_guest_number_filter.length > 0 && this.booking_guest_number_filter !== 'No Filter') {
        if (this.booking_guest_number_filter === '1') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) === 1
          })
        }
        if (this.booking_guest_number_filter === '2') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) === 2
          })
        }
        if (this.booking_guest_number_filter === '<= 3') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) <= 3
          })
        }
        if (this.booking_guest_number_filter === '<= 5') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) <= 5
          })
        }
        if (this.booking_guest_number_filter === '<= 10') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) <= 10
          })
        }
        if (this.booking_guest_number_filter === '<= 25') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) <= 25
          })
        }
        if (this.booking_guest_number_filter === '<= 50') {
          allBookings = allBookings.filter(function (booking) {
            return Number(booking.guest_total) <= 50
          })
        }
      }
      if (this.booking_guest_type_filter.length > 0) {
        var bookings = []
        this.booking_guest_type_filter.map(function (filter) {
          allBookings.map(function (booking) {
            booking.guests.find(function (guest) {
              if (guest.guest_type === filter && guest.no_of_guests > 0) {
                bookings.push(booking)
              }
            })
            console.log('Added to bookings : ')
            console.log(bookings)
          })
        })
        return bookings
        // allBookings = allBookings.filter(booking => booking.booking_type === this.booking_types_filter)
      }
      return allBookings
    },
    organizationTypes: function () {
      return this.$store.state.GroupBookingTypeStore.all_group_booking_types
    },
    singleBooking: function () {
      return this.$store.state.BookingStore.booking
    },
    successBooking: function () {
      return this.$store.state.BookingStore.success_booking
    },
    vehiclePrices: function () {
      return this.$store.BookingStore.vehicle_entry
    },
    inventoryAvailability: function () {
      return this.$store.state.InventoryStore.inventory_num
    },
    browserBooking: function () {
      return this.$store.state.BookingStore.local_booking
    },
    inventoryBudget: function () {
      return this.$store.state.InventoryStore.inventory_selection
    },
    getCheckInToday: function () {
      return this.$store.state.BookingStore.checking_in_today
    },
    getCheckOutToday: function () {
      return this.$store.state.BookingStore.checking_out_today
    },
    getInventoryCalendar: function () {
      return this.$store.state.CalendarStore.calendar_inventory
    },
    getCampingCalendar: function () {
      return this.$store.state.CalendarStore.calendar_camping
    },
    getAccomodationCalendar: function () {
      return this.$store.state.CalendarStore.calendar_accomodation
    },
    gatePasses: function () {
      return this.$store.state.BookingStore.gate_passes
    },
    gatePass: function () {
      return this.$store.state.BookingStore.gate_pass
    },
    confirmedBookings: function () {
      return this.$store.state.BookingStore.confirmed_bookings
    },
    unconfirmedBookings: function () {
      return this.$store.state.BookingStore.un_confirmed_bookings
    },
    checkedIn: function () {
      return this.$store.state.BookingStore.checked_in_bookings
    },
    checkedOut: function () {
      return this.$store.state.BookingStore.checked_out_bookings
    },
    gateDefaults: function () {
      return this.$store.state.BookingStore.gate
    },
    getCurrentCalendar: function () {
      return this.$store.state.BookingStore.calendar
    },
    destinations: function () {
      return this.$store.state.BookingStore.destinations
    },
    approvedBookings: function () {
      return this.$store.state.BookingStore.approved
    },
    bookingPaymentMethods: function () {
      var array = []
      for (var i = 0; i < 4; i++) {
        array.push(this.$store.state.PaymentMethodStore.all_payment_methods[i])
      }
      return array
    },
    allGates: function () {
      return this.$store.state.GateStore.all_gates
    },
    partnerCheckinTodayBookings: function () {
      return this.$store.state.BookingStore.partner_checkin_today_bookings
    },
    partnerCheckoutTodayBookings: function () {
      return this.$store.state.BookingStore.partner_checkout_today_bookings
    }
  }
}
