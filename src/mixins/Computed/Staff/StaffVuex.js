export default {
  methods: {
    putData () {
      console.log(this.singleStaff)
      this.staff = this.singleStaff
    }
  },
  computed: {
    allStaff: function () {
      return this.$store.state.StaffStore.all_staffs
    },
    singleStaff: function () {
      return this.$store.state.StaffStore.staff
    },
    $updateStaff () {
      if (this.$route.name === 'EditStaff') {
        this.putData()
      }
      return null
    }
  }
}
