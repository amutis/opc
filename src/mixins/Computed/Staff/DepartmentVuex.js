export default {
  computed: {
    allDepartments: function () {
      return this.$store.state.DepartmentStore.all_departments
    }
  }
}
