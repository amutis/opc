import Vue from 'vue'

import DepartmentVuex from './DepartmentVuex'
import StaffVuex from './StaffVuex'

Vue.mixin(DepartmentVuex)
Vue.mixin(StaffVuex)
