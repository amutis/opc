export default {
  computed: {
    allCurrencies: function () {
      var all = this.$store.state.CurrencyStore.all_currencies
      var currenciesToDisplay = []
      all.map(function (currency) {
        if (currency.buying_selling.length > 0 || currency.currency_name === 'KES') {
          var fullName = currency.currency_name + ' ' + '- Current Buying : ' + currency.buying_selling[0].buy_amount + ' KES, Current Selling : ' + currency.buying_selling[0].sell_amount + ' KES'
          Object.assign(currency, {full_name: fullName})
          currenciesToDisplay.push(currency)
        }
      })
      return currenciesToDisplay
    },
    allExchanges: function () {
      return this.$store.state.CurrencyStore.exchanges
    },
    allCurrencyTypes: function () {
      return this.$store.state.CurrencyStore.all_currency_types
    },
    currentCurrency: function () {
      return this.$store.state.CurrencyStore.currency[0]
    },
    singleCurrency: function () {
      return this.$store.state.CurrencyStore.currency[0]
    },
    numberFormat: function (n) {
      return n.toLocaleString('en')
    },
    browserCurrency: function () {
      return this.$store.state.CurrencyStore.browser_currency
    }
  },
  methods: {
    currencyChange: function (data) {
      this.$store.commit('LOCAL_CURRENCY2', data)
    },
    wholeNumber: function (n) {
      return Math.round(n)
    },
    convertToKes: function (amount) {
      var usd = localStorage.getItem('base')
      var newAmount = this.wholeNumber(amount * usd)
      return 'KES ' + newAmount.toLocaleString('en')
    },
    convertToUsd: function (amount) {
      var usd = localStorage.getItem('base')
      var newAmount = this.wholeNumber(amount / usd)
      return '$ ' + newAmount.toLocaleString('en')
    }
  }
}
