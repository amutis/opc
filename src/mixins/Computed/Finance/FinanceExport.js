import Vue from 'vue'

import CateringLevyVuex from './CateringLevyVuex'
import CurrencyVuex from './CurrencyVuex'
import VatVuex from './VatVuex'

Vue.mixin(CateringLevyVuex)
Vue.mixin(CurrencyVuex)
Vue.mixin(VatVuex)
