export default {
  computed: {
    allCateringLevies: function () {
      return this.$store.state.CateringLevyStore.all_catering_levies
    },
    currentCateringLevy: function () {
      return this.$store.state.CateringLevyStore.catering_levy[0]
    },
    singleCateringLevy: function () {
      return this.$store.state.CateringLevyStore.catering_levy[0]
    }
  }
}
