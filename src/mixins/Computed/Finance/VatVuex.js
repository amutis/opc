export default {
  computed: {
    allVats: function () {
      return this.$store.state.VatStore.all_vats
    },
    currentVat: function () {
      return this.$store.state.VatStore.vat[0]
    },
    singleVat: function () {
      return this.$store.state.VatStore.vat[0]
    }
  }
}
