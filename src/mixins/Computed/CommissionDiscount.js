export default {
  computed: {
    allFacilityDiscountTypes: function () {
      return this.$store.state.FacilityDiscountTypeStore.all_facility_discount_types
    },
    allInventoryDiscountTypes: function () {
      return this.$store.state.ActivityDiscountTypeStore.all_a_discount_types
    },
    allFacilityDiscounts: function () {
      return this.$store.state.FacilityDiscountStore.facility_discounts
    },
    allInventoryDiscounts: function () {
      return this.$store.state.ActivityDiscountStore.activity_discounts
    }
  }
}
