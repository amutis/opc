import Vue from 'vue'

import CountriesGendersVuex from './CommissionDiscount'
import DonationVuex from './DonationVuex'
import IdentificationTypeVuex from './IdentificationTypeVuex'
import MenuVuex from './MenuVuex'
import PartnerVuex from './PartnerVuex'
import PricingVuex from './PricingVuex'
import ResidenceVuex from './ResidenceVuex'
import VisitorSettingVuex from './VisitorSettingVuex'
import VisitorVuex from './VisitorVuex'
import VolunteerVuex from './VolunteerVuex'

Vue.mixin(CountriesGendersVuex)
Vue.mixin(DonationVuex)
Vue.mixin(IdentificationTypeVuex)
Vue.mixin(MenuVuex)
Vue.mixin(PartnerVuex)
Vue.mixin(PricingVuex)
Vue.mixin(ResidenceVuex)
Vue.mixin(VisitorSettingVuex)
Vue.mixin(VisitorVuex)
Vue.mixin(VolunteerVuex)
