import CurrencyConversion from '../../Functions/Currency'
export default {
  computed: {
    allMemberLevels: function () {
      return this.$store.state.MembershipSettingStore.all_membership_settings
    },
    allBeneficiaries: function () {
      return this.$store.state.MemberStore.beneficiaries
    },
    allMemberTypes: function () {
      return this.$store.state.MemberTypeStore.all_member_types
    },
    allMemberSettings: function () {
      var allSetting = this.$store.state.MembershipSettingStore.all_membership_settings
      allSetting.map(function (setting) {
        var customName = {custom_name: setting.name + ' - ' + setting.membership_type + ' (' + setting.residence + ')'}
        Object.assign(setting, customName)
      })
      return allSetting
    },
    memberSettingsGrouped: function () {
      // var array = []
      this.allMemberSettings.map(function (member) {
      })
    },
    memberTotal: function () {
      var total = this.convert1('KES', this.selectedSetting.price).amount
      this.allBeneficiaries.map(function (beneficiary) {
        total = total + beneficiary.amount
      })
      if (isNaN(total) === true) {
        return 0
      } else {
        return total
      }
    },
    allHouseHoldTypes: function () {
      return this.$store.state.HouseHoldStore.all_house_holds
    },
    selectedSetting: function () {
      return this.$store.state.MemberStore.local_member_setting
    },
    membershipRegistrationTotal: function () {
      return Number(this.selectedSetting.number_of_persons) + Number(this.selectedSetting.number_of_children)
    },
    allMembers: function () {
      return this.$store.state.MemberStore.all_members
    },
    singleMember: function () {
      return this.$store.state.MemberStore.member
    },
    updateMember: function () {
      if (this.$route.name === 'Member') {
        this.member.type = this.$store.state.MemberStore.member.setting_public_id
        this.member.residence = this.$store.state.MemberStore.member.residence_public_id
        this.member.household = this.$store.state.MemberStore.member.household_type_public_id
        this.member.registration_type = this.$store.state.MemberStore.member.registration_type_public_id
        this.member.first_name = this.$store.state.MemberStore.member.full_name
        this.member.last_name = this.$store.state.MemberStore.member.full_name
        this.member.title = this.$store.state.MemberStore.member.title
        this.member.postal_address = this.$store.state.MemberStore.member.postal_address
        this.member.gender = this.$store.state.MemberStore.member.gender_public_id
        this.member.dob = this.$moment(this.$store.state.MemberStore.member.dob).format('MM/DD/YYYY')
        this.member.postal_code = this.$store.state.MemberStore.member.postal_code
        this.member.identification_number = this.$store.state.MemberStore.member.id_alien_number
        this.member.email = this.$store.state.MemberStore.member.email
        this.member.telephone_number = this.$store.state.MemberStore.member.tel_number
      }
    }
  },
  mixins: [CurrencyConversion]
}
