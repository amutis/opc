import Vue from 'vue'

import MembershipTypesVuex from './MembershipTypesVuex'
import MembershipVuex from './MembershipVuex'

Vue.mixin(MembershipTypesVuex)
Vue.mixin(MembershipVuex)
