export default {
  computed: {
    allMembershipLevels: function () {
      return this.$store.state.MemberTypeStore.member_levels
    },
    allRegistrationTypes: function () {
      return this.$store.state.MemberTypeStore.registration_types
    },
    allHouseHolds: function () {
      return this.$store.state.MemberTypeStore.house_holds
    },
    allOfHouseHolds: function () {
      return this.$store.state.HouseHoldStore.all_house_holds
    },
    allGender: function () {
      return this.$store.state.GenderStore.all_genders
    }
  }
}
