import Vue from 'vue'

import InventoryCurrencyVuex from './InventoryCurrencyVuex'
import InventoryVuex from './InventoryVuex'
import InventoryTypeVuex from './InventoryTypeVuex'

Vue.mixin(InventoryVuex)
Vue.mixin(InventoryCurrencyVuex)
Vue.mixin(InventoryTypeVuex)
