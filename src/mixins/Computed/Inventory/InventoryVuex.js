import CurrencyConversion from '../../Functions/Currency'
export default {
  methods: {
    removeInventory: function (key) {
      var localInventory = JSON.parse(localStorage.getItem('activity_data'))
      localInventory.splice(key, 1)
      this.$store.commit('CHOSEN_ACTIVITIES', localInventory)
      // this.$store.commit('REMOVE_INVENTORY', inventory)
      localStorage.setItem('activity_data', JSON.stringify(localInventory))
    },
    activityDifference: function (activity) {
      return this.findActivityTotal(activity) - this.activityDiscount(activity) - this.activityCommission(activity)
    },
    activityDiscount: function (activity) {
      if (activity.discount_status === 'true') {
        var discounts = activity.discounts
        var discount = 0
        var activityD = 0
        for (var i = 0; i < discounts.length; i++) {
          if (this.gate_pass.tour_operator.type_public_id === discounts[i].discount_type_id) {
            discount = discounts[i].discount
          }
        }
        activityD = this.findActivityTotal(activity) * Number(discount) / 100
        return activityD
      } else {
        return 0
      }
    },
    activityCommission: function (activity) {
      if (activity.discount_status === 'true') {
        var discounts = activity.discounts
        var discount = 0
        var activityD = 0
        for (var i = 0; i < discounts.length; i++) {
          if (this.gate_pass.tour_operator.type_public_id === discounts[i].discount_type_id) {
            discount = discounts[i].commission
          }
        }
        activityD = this.findActivityTotal(activity) * Number(discount) / 100
        return activityD
      } else {
        return 0
      }
    },
    findActivityTotal: function (activity) {
      var total = 0
      var children = Number(activity.children)
      var childPrice = Number(activity.child_price)
      var adults = Number(activity.adults)
      var adultPrice = Number(activity.adult_price)
      var childCost = children * childPrice
      var adultCost = adults * adultPrice
      total = childCost + adultCost
      total = this.convert1(activity.currency, total).amount
      return total
    }
  },
  computed: {
    allInventoryTotal: function () {
      var total = 0
      for (var x = 0; x < this.chosenInventory.length; x++) {
        total = total + this.findActivityTotal(this.chosenInventory[x])
      }
      return total
    },
    inventorySchedule: function () {
      return this.$store.state.BookingStore.inventory_schedule
    },
    chosenInventory: function () {
      return this.$store.state.ActivityStore.chosenInventory
    },
    inventoryImages: function () {
      return this.$store.state.ActivityStore.activity_images
    },
    allInventory: function () {
      return this.$store.state.ActivityStore.active_facilities
    },
    activeInventory: function () {
      return this.$store.state.ActivityStore.active_activities
    },
    inventoryWithPrices: function () {
      var array = []
      this.activeInventory.map(function (inventory) {
        if (inventory.pricing_status === 'true') {
          array.push(inventory)
        }
      })
      return array
    },
    selectedInventory: function () {
      return this.$store.state.ActivityStore.inventory_selection
    },
    inActiveInventory: function () {
      return this.$store.state.ActivityStore.in_active_activities
    },
    singlePricing: function () {
      return this.$store.state.ActivityPricingStore.activity_pricing
    },
    singleInventory: function () {
      return this.$store.state.ActivityStore.activity
    },
    activitySlots: function () {
      return this.$store.state.BookingStore.activity_slots
    },
    inventoryPriceSchedule: function () {
      return this.$store.state.ActivityPricingStore.activity_pricings
    },
    inventoryBudget: function () {
      return this.$store.state.ActivityPricingStore.inventory_selection
    },
    inventoryAvailability: function () {
      return this.$store.state.ActivityStore.inventory_num
    },
    inventoryContact: function () {
      return this.$store.state.ActivityContactStore.activity_contacts
    },
    inventoryCurrency: function () {
      return this.$store.state.ActivityCurrencyStore.all_activity_currencies
    },
    updateInventory: function () {
      if (this.$route.name === 'EditInventory') {
        this.inventory.code = this.singleInventory.code
        this.inventory.analysis_code = this.singleInventory.analysis_code
        this.inventory.vat = this.singleInventory.vat
        this.inventory.categing_levy = this.singleInventory.categing_levy
        this.inventory.sub_code = this.singleInventory.sub_code
        this.inventory.name = this.singleInventory.name
        this.inventory.description = this.singleInventory.description
        this.inventory.maximum_guests = this.singleInventory.maximum_guests
      }
    }
  },
  mixins: [CurrencyConversion]
}
