export default {
  computed: {
    allInventoryCurrency: function () {
      return this.$store.state.ActivityCurrencyStore.all_activity_currencies
    }
  }
}
