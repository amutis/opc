export default {
  computed: {
    AllVisitorTypes: function () {
      return this.$store.state.VisitorTypeStore.all_visitor_types
    }
  }
}
