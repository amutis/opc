export default {
  methods: {
    setCurrencyToUSD () {
      var newCurrency = {}
      this.allExchanges.find(function (currency) {
        if (currency.currency.toLowerCase() === 'usd') {
          newCurrency = currency
        }
      })
      this.$store.commit('LOCAL_CURRENCY2', newCurrency)
    },
    setCurrencyToKES () {
      var newCurrency = {}
      this.allExchanges.find(function (currency) {
        if (currency.currency.toLowerCase() === 'kes') {
          newCurrency = currency
        }
      })
      this.$store.commit('LOCAL_CURRENCY2', newCurrency)
    },
    convert1: function (currencyToConvertFrom, amountToConvert) {
      if (currencyToConvertFrom === undefined || amountToConvert === undefined) {
        return {
          amount: 0,
          raw_amount: 0,
          sign: ''
        }
      }
      if (amountToConvert === undefined) {
        amountToConvert = 0
      }
      var currencyToConvertTo = this.$store.state.CurrencyStore.browser_currency.toLowerCase()
      var allExchanges = this.$store.state.CurrencyStore.exchanges
      currencyToConvertFrom = currencyToConvertFrom.toLowerCase()
      var sign = ''
      if (currencyToConvertFrom === currencyToConvertTo) {
        if (currencyToConvertFrom === 'kes') {
          sign = 'KES'
        } else {
          allExchanges.find(function (currency) {
            if (currency.currency.toLowerCase() === currencyToConvertTo) {
              sign = currency.sign
            }
          })
        }
        return {
          amount: amountToConvert,
          raw_amount: amountToConvert,
          sign: sign
        }
      } else if (currencyToConvertFrom === 'kes') {
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertTo) {
            amountToConvert = Number(amountToConvert) / Number(currency.sell_amount)
            sign = currency.sign
          }
        })
        return {
          amount: amountToConvert,
          raw_amount: amountToConvert,
          sign: sign
        }
      } else if (currencyToConvertTo === 'kes') {
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertFrom) {
            amountToConvert = Number(amountToConvert) * Number(currency.buy_amount)
          }
        })
        return {
          amount: amountToConvert,
          raw_amount: amountToConvert,
          sign: 'KES'
        }
      } else if (currencyToConvertFrom !== currencyToConvertTo && currencyToConvertFrom !== 'kes' && currencyToConvertTo !== 'kes') {
        var amount = 0
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertFrom) {
            amount = Number(amountToConvert) * Number(currency.buy_amount)
          }
        })
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertTo) {
            amount = Number(amount) / Number(currency.sell_amount)
            sign = currency.sign
          }
        })
        return {
          amount: amount,
          raw_amount: amount,
          sign: sign
        }
      }
    },
    convert2: function (currencyToConvertFrom, currencyToConvertTo, amountToConvert) {
      currencyToConvertFrom = currencyToConvertFrom.toLowerCase()
      currencyToConvertTo = currencyToConvertTo.toLowerCase()
      var sign = ''
      var allExchanges = this.$store.state.CurrencyStore.exchanges
      if (currencyToConvertFrom === currencyToConvertTo) {
        if (currencyToConvertFrom === 'kes') {
          sign = 'KES'
        } else {
          allExchanges.find(function (currency) {
            if (currency.currency.toLowerCase() === currencyToConvertTo) {
              sign = currency.sign
            }
          })
        }
        return {
          amount: Math.round(amountToConvert),
          raw_amount: amountToConvert,
          sign: sign
        }
      } else if (currencyToConvertFrom === 'kes') {
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertTo) {
            amountToConvert = Number(amountToConvert) / Number(currency.sell_amount)
            sign = currency.sign
          }
        })
        return {
          amount: Math.round(amountToConvert),
          raw_amount: amountToConvert,
          sign: sign
        }
      } else if (currencyToConvertTo === 'kes') {
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertFrom) {
            amountToConvert = Number(amountToConvert) * Number(currency.buy_amount)
          }
        })
        return {
          amount: Math.round(amountToConvert),
          raw_amount: amountToConvert,
          sign: 'KES'
        }
      } else {
        var amount = 0
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertFrom) {
            amount = Number(amountToConvert) * Number(currency.buy_amount)
          }
        })
        allExchanges.find(function (currency) {
          if (currency.currency.toLowerCase() === currencyToConvertTo) {
            amount = Number(amount) / Number(currency.sell_amount)
            sign = currency.sign
          }
        })
        return {
          amount: Math.round(amount),
          raw_amount: amount,
          sign: sign
        }
      }
    },
    findCurrency: function (newCurrency) {

    }
  }
}
