export default {
  computed: {
    menu: function () {
      var mainMenu = this.$store.state.AuthenticationStore.menu.menu
      var newMenu = []
      mainMenu.map(function (menu) {
        if (menu.features[0].type === 1 || menu.features[0].type === '1') {
          newMenu.push(Object.assign(menu, {model: false}))
        }
      })
      return newMenu
    },
    $update () {
      var mainMenu = this.$store.state.AuthenticationStore.menu.menu
      var crud = []
      mainMenu.map(function (menu) {
        if (menu.features[0].type === 2 || menu.features[0].type === '2') {
          crud.push(Object.assign(menu, {model: false}))
        }
      })
      if (crud.length > 0) {
        if (crud[0].features[0].public_id === 'ereer') {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    },
    $user_type: function () {
      return this.$store.state.AuthenticationStore.menu.user_type
    }
  }
}
