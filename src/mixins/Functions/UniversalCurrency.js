export default {
  computed: {
    $localCurrency () {
      var allCurrencies = this.$store.state.CurrencyStore.all_currencies
      var localCurrency = this.$store.state.CurrencyStore.browser_currency.toLowerCase()
      var sign = ''
      allCurrencies.find(function (currency) {
        if (currency.currency_name.toLowerCase() === localCurrency) {
          sign = currency.currency_sign
        }
      })
      return sign
    },
    $loadingTrue () {
      return this.$store.state.LoadingStore.loading
    }
  },
  methods: {
    $roundOff: function (number) {
      return Math.round(number).toLocaleString('en')
    },
    $imageUniversalUrl: function () {
      // http://165.227.117.249 Development
      // http://167.99.85.35 Production
      // return 'http://165.227.117.249'
      return 'https://bookings.olpejetaconservancy.org'
    }
  }
}
