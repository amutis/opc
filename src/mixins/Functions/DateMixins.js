export default {
  methods: {
    formated_date: function (date) {
      if (this.$moment.parseZone(date).format('YYYY-MM-DD') === this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return 'Today | ' + this.$moment.parseZone(date).format('DD, MMM YYYY')
      } else {
        return this.$moment(date).format('DD, MMM YYYY')
      }
    },
    formated_date_time: function (date) {
      if (this.$moment.parseZone(date).format('YYYY-MM-DD') === this.$moment(Date(new Date())).format('YYYY-MM-DD')) {
        return 'Today | ' + this.$moment.parseZone(date).format('YYYY-MM-DD') + ' at ' + this.$moment.parseZone(date).format('HH:mm')
      } else {
        return this.$moment.parseZone(date).format('YYYY-MM-DD') + ' at ' + this.$moment.parseZone(date).format('HH:mm')
      }
    }
  }
}
