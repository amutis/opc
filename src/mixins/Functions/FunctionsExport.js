import Vue from 'vue'

import Currency from './Currency'
import DateMixins from './DateMixins'
import Menu from './Menu'
import TrueFalse from './TrueFalse'
import UniversalCurrency from './UniversalCurrency'

Vue.mixin(Currency)
Vue.mixin(DateMixins)
Vue.mixin(Menu)
Vue.mixin(TrueFalse)
Vue.mixin(UniversalCurrency)
