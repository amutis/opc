export default {
  data () {
    return {
      // Live c3d68cda-85f3-44c1-b578-6609e86e305e
      // Demo a13ea432-fae7-479c-93ec-3a5799034c7c
      live_id: 'c3d68cda-85f3-44c1-b578-6609e86e305e',
      // live_id: 'a13ea432-fae7-479c-93ec-3a5799034c7c',
      // Test
      // iveri_url: 'http://165.227.117.249'
      // Production
      // http://167.99.85.35

      // iveri_url: 'http://165.227.117.249'
      iveri_url: 'http://167.99.85.35'
    }
  },
  computed: {
    bookingStatus: function () {
      var name = this.$route.name.split('.', 1)
      if (name[0] === 'Booking') {
        this.booking = true
      } else {
        this.booking = false
      }
    }
  },
  methods: {
    numberLimiter (min, currentNumber, max) {
      if (currentNumber <= min && currentNumber >= max) {
        return currentNumber
      } else {
        alert('That input is beyond the Maximum')
      }
    }
  }
}
