// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './vuex/Store'
import VueCarousel from 'vue-carousel'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'
import VueResource from 'vue-resource'
import VueContentPlaceholders from 'vue-content-placeholders'
import VueGoodTable from 'vue-good-table'
import Vueditor from 'vueditor'
import FileUpload from 'v-file-upload'
// import VueSocketio from 'vue-socket.io'
// import bugsnag from 'bugsnag-js'
// import bugsnagVue from 'bugsnag-vue'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import vSelect from 'vue-select'
import 'vueditor/dist/style/vueditor.min.css'
import Vuetify from 'vuetify'
import 'babel-polyfill'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import VueisOffLine from 'vue-isoffline'
import VueTippy from 'vue-tippy'
import flatPickr from 'vue-flatpickr-component'
import 'flatpickr/dist/flatpickr.css'

// Mixins
Vue.mixin(require('./mixins/Computed/Finance/FinanceExport'))
Vue.mixin(require('./mixins/Computed/Booking/BookingExport'))
Vue.mixin(require('./mixins/Computed/Facility/FacilityExport'))
Vue.mixin(require('./mixins/Computed/Inventory/InventoryExport'))
Vue.mixin(require('./mixins/Computed/Membership/MembershipExport'))
Vue.mixin(require('./mixins/Computed/Settings/SettingsExport'))
Vue.mixin(require('./mixins/Computed/Staff/StaffExport'))
Vue.mixin(require('./mixins/Computed/Users/UsersExport'))
Vue.mixin(require('./mixins/Computed/Volunteer/VolunteerExport'))
Vue.mixin(require('./mixins/Computed/ComputedExport'))
Vue.mixin(require('./mixins/Functions/FunctionsExport'))

Vue.config.productionTip = false
Vue.use(flatPickr)
Vue.use(FileUpload)
Vue.use(VueGoodTable)
Vue.use(VueCarousel)
Vue.use(VueResource)
Vue.use(VueContentPlaceholders)
Vue.use(VueMomentJS, moment)
Vue.use(VueisOffLine)
Vue.use(VueTippy)

Vue.component('v-select', vSelect)
// Vue.use(VueSocketio, 'http://597d5ad5.ngrok.io')
Vue.use(Vuetify, {
  theme: {
    musk: '#520036',
    primary: '#73a533',
    secondary: '#42A5F5',
    accent: '#8c9eff',
    warning: '#ea691f',
    error: '#E53935',
    yellow: '#fdb813'
  }
})
// const bugsnagClient = bugsnag('63844213d3c52e39ff1cb17bc467abeb')
// bugsnagClient.use(bugsnagVue(Vue))

// If using mini-toastr, provide additional configuration
const toastTypes = {
  success: 'success',
  error: 'error',
  info: 'info',
  warn: 'warn'
}

// Vue Editor
// your config here
let config = {
  toolbar: [
    'removeFormat', 'undo', '|', 'elements', 'fontName', 'fontSize', 'foreColor', 'backColor', 'divider',
    'bold', 'italic', 'underline', 'strikeThrough', 'links', 'divider', 'subscript', 'superscript',
    'divider', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull', '|', 'indent', 'outdent',
    'insertOrderedList', 'insertUnorderedList', '|', 'tables', '|', 'switchView'
  ],
  fontName: [
    {val: 'arial black'},
    {val: 'times new roman'},
    {val: 'Courier New'}
  ],
  fontSize: ['12px', '14px', '16px', '18px', '0.8rem', '1.0rem', '1.2rem', '1.5rem', '2.0rem'],
  uploadUrl: ''
}
Vue.use(Vueditor, config)

miniToastr.init({types: toastTypes})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (localStorage.getItem('lkjhgfdsa') === null) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

// Here we setup messages output to `mini-toastr`
function toast ({title, message, type, timeout, cb}) {
  return miniToastr[type](message, title, timeout, cb)
}

// Binding for methods .success(), .error() and etc. You can specify and map your own methods here.
// Required to pipe our output to UI library (mini-toastr in example here)
// All not-specified events (types) would be piped to output in console.
const options = {
  success: toast,
  error: toast,
  info: toast,
  warn: toast
}

Vue.use(VueNotifications, options)// VueNotifications have auto install but if we want to specify options we've got to do it manually.

Vue.http.interceptors.push(function (request, next) {
  // modify headers
  request.headers.set('Accept', 'application/json')
  // request.headers.set('Access-Control-Allow-Origin', '*')
  // request.headers.set('Access-Control-Request-Method', '*')
  // continue to next interceptor
  request.headers.set('Authorization', 'Bearer ' + localStorage.getItem('access_token'))
  store.dispatch('loading_true')
  next(function (response) {
    if (response.status < 400) {
      if (response.data.message) {
        VueNotifications.info({message: response.data.message})
      }
    } else if (response.status > 400) {
      if (response.data.message) {
        VueNotifications.error({message: response.data.message})
        // bugsnagClient.notify()
      }
      if (response.data.messages) {
        for (var i = 0; response.data.messages.length > i; i++) {
          VueNotifications.error({message: response.data.messages[i]})
        }
      }
    }
    store.dispatch('loading_false')
  })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
