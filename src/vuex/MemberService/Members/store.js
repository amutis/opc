import mutations from './mutations'
import actions from './actions'

const state = {
  all_members: [],
  members: [],
  member: [],
  deleted_members: [],
  local_member_setting: {},
  member_children: [],
  member_adults: [],
  beneficiaries: []
}

export default {
  state, mutations, actions
}
