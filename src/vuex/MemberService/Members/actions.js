import Vue from 'vue'
import {memberUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_members (context) {
    Vue.http.get(memberUrls.getAllMembers).then(function (response) {
      context.commit('GET_ALL_MEMBERS', response.data)
      context.dispatch('loading_false')
    })
  },
  remind_membership_payment (context, publicId) {
    Vue.http.get(memberUrls.remindMembershipPayment + publicId).then(function (response) {
      context.dispatch('loading_false')
    })
  },
  get_members (context, publicId) {
    Vue.http.get(memberUrls.getMembers + publicId).then(function (response) {
      context.commit('GET_MEMBERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_members (context) {
    Vue.http.get(memberUrls.getDeletedMember).then(function (response) {
      context.commit('GET_DELETED_MEMBERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_member (context, publicId) {
    Vue.http.get(memberUrls.getMember + publicId).then(function (response) {
      context.commit('GET_MEMBER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_public_member (context, data) {
    Vue.http.post(memberUrls.postMember, data).then(function (responce) {
      router.push({
        name: 'MembershipPayment',
        params: {
          member_id: responce.body.member_ref_code
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  member_request_mpesa (context, data) {
    Vue.http.post(memberUrls.requestMpesa, data).then(function () {
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_member (context, data) {
    Vue.http.post(memberUrls.postMember, data).then(function () {
      context.dispatch('get_all_members')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_member (context, data) {
    Vue.http.post(memberUrls.editMember, data).then(function () {
      context.dispatch('get_all_members')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_member (context, publicId) {
    Vue.http.get(memberUrls.deleteMember + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEMBERS', response.data)
      router.push({
        name: 'Module.DeletedMembers'
      })
      context.dispatch('loading_false')
    })
  },
  restoreMember (context, publicId) {
    Vue.http.get(memberUrls.restoreMember + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEMBERS', response.data)
      router.push({
        name: 'Module.Members'
      })
      context.dispatch('loading_false')
    })
  }
}
