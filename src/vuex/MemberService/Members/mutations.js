export default {
  GET_MEMBERS (state, data) {
    state.members = data.data
  },
  GET_ALL_MEMBERS (state, data) {
    state.all_members = data
  },
  GET_MEMBER (state, data) {
    state.member = data
  },
  GET_DELETED_MEMBERS (state, data) {
    state.deleted_members = data.data
  },
  GET_LOCAL_MEMBERS (state, data) {
    state.local_member_setting = data
  },
  CLEA_LOCAL_MEMBERS (state) {
    state.local_member_setting = []
  },
  GET_LOCAL_MEMBER_CHILDREN (state, data) {
    state.member_children = data
  },
  GET_LOCAL_MEMBER_ADULTS (state, data) {
    state.member_adults = data
  },
  ADD_BENEFICIARY (state, data) {
    state.beneficiaries.push(data)
  },
  CLEAR_BENEFICIARIES (state) {
    state.beneficiaries = []
  }
}
