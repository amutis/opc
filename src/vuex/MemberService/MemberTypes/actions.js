import Vue from 'vue'
import {memberTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_member_types (context) {
    Vue.http.get(memberTypeUrls.getAllMemberTypes).then(function (response) {
      context.commit('GET_ALL_MEMBER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_member_types (context, publicId) {
    Vue.http.get(memberTypeUrls.getMemberTypes + publicId).then(function (response) {
      context.commit('GET_MEMBER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_member_genders (context) {
    Vue.http.get(memberTypeUrls.getMemberGenders).then(function (response) {
      context.commit('GET_GENDERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_member_types (context) {
    Vue.http.get(memberTypeUrls.getDeletedMemberType).then(function (response) {
      context.commit('GET_DELETED_MEMBER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_member_type (context, publicId) {
    Vue.http.get(memberTypeUrls.getMemberType + publicId).then(function (response) {
      context.commit('GET_MEMBER_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_member_type (context, data) {
    Vue.http.post(memberTypeUrls.postMemberType, data).then(function () {
      context.dispatch('get_all_membership_settings')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_member_type (context, data) {
    Vue.http.patch(memberTypeUrls.editMemberType, data).then(function () {
      context.dispatch('get_all_membership_settings')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_member_type (context, publicId) {
    Vue.http.get(memberTypeUrls.deleteMemberType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MEMBER_TYPES', response.data)
      router.push({
        name: 'Module.DeletedMemberTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreMemberType (context, publicId) {
    Vue.http.get(memberTypeUrls.restoreMemberType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEMBER_TYPES', response.data)
      router.push({
        name: 'Module.MemberTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
