export default {
  GET_MEMBER_TYPES (state, data) {
    state.member_types = data.data
  },
  GET_ALL_MEMBER_TYPES (state, data) {
    state.all_member_types = data.data
  },
  GET_MEMBER_TYPE (state, data) {
    state.member_type = data.data
  },
  GET_DELETED_MEMBER_TYPES (state, data) {
    state.deleted_member_types = data.data
  },
  GET_GENDERS (state, data) {
    state.genders = data.data
  }
}
