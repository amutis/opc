import mutations from './mutations'
import actions from './actions'

const state = {
  all_member_types: [],
  member_types: [],
  member_type: [],
  deleted_member_types: [],
  genders: []
}

export default {
  state, mutations, actions
}
