import {urls} from '../urls'
export const currencyService = urls.developmentUrl + ':5004/v1/'

export const houseHoldurls = {
  postHouseHold: currencyService + 'starts/post',
  getSettingHousehold: currencyService + 'type/household/',
  getHouseHolds: currencyService + 'households',
  getHouseHold: currencyService + 'type/household/',
  deleteHouseHold: currencyService + 'starts/delete/',
  editHouseHold: currencyService + 'starts/edit/',
  getAllHouseHolds: currencyService + 'households',
  restoreHouseHold: currencyService + 'starts/restore/',
  getDeletedHouseHold: currencyService + 'starts/deleted'
}

export const memberUrls = {
  requestMpesa: currencyService + 'member/payment',
  postMember: currencyService + 'add/member',
  getMembers: currencyService + 'members',
  getMember: currencyService + 'member/',
  deleteMember: currencyService + 'members/delete/',
  editMember: currencyService + 'update/member/',
  getAllMembers: currencyService + 'members',
  remindMembershipPayment: currencyService + 'membership/email/reminder/',
  restoreMember: currencyService + 'members/restore/',
  getDeletedMember: currencyService + 'members/deleted'
}

export const memberTypeUrls = {
  postMemberType: currencyService + 'add/member/setting',
  getMemberTypes: currencyService + 'membertypes',
  getMemberGenders: currencyService + 'genders',
  getMemberType: currencyService + 'starts/single/',
  deleteMemberType: currencyService + 'starts/delete/',
  editMemberType: currencyService + 'update/member/setting',
  getAllMemberTypes: currencyService + 'membertypes',
  restoreMemberType: currencyService + 'starts/restore/',
  getDeletedMemberType: currencyService + 'starts/deleted'
}

export const residenceTypeUrls = {
  postResidenceType: currencyService + 'starts/post',
  getResidenceTypes: currencyService + 'residence',
  getResidenceType: currencyService + 'settings/residence/',
  deleteResidenceType: currencyService + 'starts/delete/',
  editResidenceType: currencyService + 'residence',
  getAllResidenceTypes: currencyService + 'residence',
  restoreResidenceType: currencyService + 'starts/restore/',
  getDeletedResidenceType: currencyService + 'starts/deleted'
}

export const membershipSettingUrls = {
  postMembershipSetting: currencyService + 'add/member/setting',
  getMembershipSettings: currencyService + 'member/settings',
  getMembershipSetting: currencyService + 'member/setting/',
  deleteMembershipSetting: currencyService + 'delete/member/setting',
  editMembershipSetting: currencyService + 'starts/edit/',
  getAllMembershipSettings: currencyService + 'member/settings',
  restoreMembershipSetting: currencyService + 'starts/restore/',
  getDeletedMembershipSetting: currencyService + 'starts/deleted'
}
