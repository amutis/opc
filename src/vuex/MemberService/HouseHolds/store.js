import mutations from './mutations'
import actions from './actions'

const state = {
  all_house_holds: [],
  house_holds: [],
  house_hold: [],
  deleted_house_holds: []
}

export default {
  state, mutations, actions
}
