import Vue from 'vue'
import {houseHoldurls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_house_holds (context) {
    Vue.http.get(houseHoldurls.getAllHouseHolds).then(function (response) {
      context.commit('GET_ALL_HOUSE_HOLDS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_setting_household (context, publicId) {
    Vue.http.get(houseHoldurls.getSettingHousehold + publicId).then(function (response) {
      context.commit('GET_ALL_HOUSE_HOLDS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_house_holds (context, publicId) {
    Vue.http.get(houseHoldurls.getHouseHolds + publicId).then(function (response) {
      context.commit('GET_HOUSE_HOLDS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_house_holds (context) {
    Vue.http.get(houseHoldurls.getDeletedHouseHold).then(function (response) {
      context.commit('GET_DELETED_HOUSE_HOLDS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_house_hold (context, publicId) {
    Vue.http.get(houseHoldurls.getHouseHold + publicId).then(function (response) {
      context.commit('GET_HOUSE_HOLD', response.data)
      context.dispatch('loading_false')
    })
  },
  post_house_hold (context, data) {
    Vue.http.post(houseHoldurls.postHouseHold, data).then(function () {
      context.dispatch('get_all_house_holds')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_house_hold (context, data) {
    Vue.http.post(houseHoldurls.editHouseHold, data).then(function () {
      context.dispatch('get_all_house_holds')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_house_hold (context, publicId) {
    Vue.http.get(houseHoldurls.deleteHouseHold + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_HOUSE_HOLDS', response.data)
      router.push({
        name: 'Module.DeletedHouseHolds'
      })
      context.dispatch('loading_false')
    })
  },
  restoreHouseHold (context, publicId) {
    Vue.http.get(houseHoldurls.restoreHouseHold + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_HOUSE_HOLDS', response.data)
      router.push({
        name: 'Module.HouseHolds'
      })
      context.dispatch('loading_false')
    })
  }
}
