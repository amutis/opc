export default {
  GET_HOUSE_HOLDS (state, data) {
    state.house_holds = data.data
  },
  GET_ALL_HOUSE_HOLDS (state, data) {
    state.all_house_holds = data.data
  },
  GET_HOUSE_HOLD (state, data) {
    state.house_hold = data.data
  },
  GET_DELETED_HOUSE_HOLDS (state, data) {
    state.deleted_house_holds = data.data
  }
}
