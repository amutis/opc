import mutations from './mutations'
import actions from './actions'

const state = {
  all_residence_types: [],
  residence_types: [],
  residence_type: [],
  deleted_residence_types: []
}

export default {
  state, mutations, actions
}
