export default {
  GET_RESIDENCE_TYPES (state, data) {
    state.residence_types = data.data
  },
  GET_ALL_RESIDENCE_TYPES (state, data) {
    state.all_residence_types = data.data
  },
  GET_RESIDENCE_TYPE (state, data) {
    state.residence_type = data.data
  },
  GET_DELETED_RESIDENCE_TYPES (state, data) {
    state.deleted_residence_types = data.data
  }
}
