import Vue from 'vue'
import {residenceTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_residence_types (context) {
    Vue.http.get(residenceTypeUrls.getAllResidenceTypes).then(function (response) {
      context.commit('GET_ALL_RESIDENCE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_residence_types (context, publicId) {
    Vue.http.get(residenceTypeUrls.getResidenceType + publicId).then(function (response) {
      context.commit('GET_RESIDENCE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_residence_types (context) {
    Vue.http.get(residenceTypeUrls.getDeletedResidenceType).then(function (response) {
      context.commit('GET_DELETED_RESIDENCE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_residence_type (context, publicId) {
    Vue.http.get(residenceTypeUrls.getResidenceType + publicId).then(function (response) {
      context.commit('GET_RESIDENCE_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_residence_type (context, data) {
    Vue.http.post(residenceTypeUrls.postResidenceType, data).then(function () {
      context.dispatch('get_all_residence_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_residence_type (context, data) {
    Vue.http.post(residenceTypeUrls.editResidenceType, data).then(function () {
      context.dispatch('get_all_residence_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_residence_type (context, publicId) {
    Vue.http.get(residenceTypeUrls.deleteResidenceType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_RESIDENCE_TYPES', response.data)
      router.push({
        name: 'Module.DeletedResidenceTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreResidenceType (context, publicId) {
    Vue.http.get(residenceTypeUrls.restoreResidenceType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_RESIDENCE_TYPES', response.data)
      router.push({
        name: 'Module.ResidenceTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
