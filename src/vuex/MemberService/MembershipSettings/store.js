import mutations from './mutations'
import actions from './actions'

const state = {
  all_membership_settings: [],
  membership_settings: [],
  membership_setting: [],
  deleted_membership_settings: []
}

export default {
  state, mutations, actions
}
