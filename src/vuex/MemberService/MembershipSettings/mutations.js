export default {
  GET_MEMBERSHIP_SETTINGS (state, data) {
    state.membership_settings = data.data
  },
  GET_ALL_MEMBERSHIP_SETTINGS (state, data) {
    state.all_membership_settings = data.data
  },
  GET_MEMBERSHIP_SETTING (state, data) {
    state.membership_setting = data.data
  },
  GET_DELETED_MEMBERSHIP_SETTINGS (state, data) {
    state.deleted_membership_settings = data.data
  }
}
