import Vue from 'vue'
import {membershipSettingUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_membership_settings (context) {
    Vue.http.get(membershipSettingUrls.getAllMembershipSettings).then(function (response) {
      context.commit('GET_ALL_MEMBERSHIP_SETTINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_membership_settings (context, publicId) {
    Vue.http.get(membershipSettingUrls.getMembershipSettings + publicId).then(function (response) {
      context.commit('GET_MEMBERSHIP_SETTINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_membership_settings (context) {
    Vue.http.get(membershipSettingUrls.getDeletedMembershipSetting).then(function (response) {
      context.commit('GET_DELETED_MEMBERSHIP_SETTINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_membership_setting (context, publicId) {
    Vue.http.get(membershipSettingUrls.getMembershipSetting + publicId).then(function (response) {
      context.commit('GET_MEMBERSHIP_SETTING', response.data)
      context.dispatch('loading_false')
    })
  },
  post_membership_setting (context, data) {
    Vue.http.post(membershipSettingUrls.postMembershipSetting, data).then(function () {
      context.dispatch('get_all_membership_settings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_membership_setting (context, data) {
    Vue.http.post(membershipSettingUrls.editMembershipSetting, data).then(function () {
      context.dispatch('get_all_membership_settings')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_membership_setting (context, data) {
    Vue.http.patch(membershipSettingUrls.deleteMembershipSetting, data).then(function () {
      context.dispatch('get_all_membership_settings')
      context.dispatch('loading_false')
    })
  },
  restoreMembershipSetting (context, publicId) {
    Vue.http.get(membershipSettingUrls.restoreMembershipSetting + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MEMBERSHIP_SETTINGS', response.data)
      router.push({
        name: 'Module.MembershipSettings'
      })
      context.dispatch('loading_false')
    })
  }
}
