import Vue from 'vue'

export default {
  mpesa_request (context, data) {
    Vue.http.post('http://localhost:9000/checkout', data).then(function () {
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  }
}
