import {urls} from '../urls'
export const currencyService = urls.developmentUrl + ':5006/'

export const vatUrls = {
  postVat: currencyService + 'vat/rates/new',
  getCurrentVat: currencyService + 'vat/rates/view/latest',
  getVats: currencyService + 'vat/rates/view',
  getVat: currencyService + 'vat/rates/view/',
  deleteVat: currencyService + 'vat/rates/delete/',
  editVat: currencyService + 'starts/edit/',
  getAllVats: currencyService + 'vat/rates/view',
  restoreVat: currencyService + 'starts/restore/',
  getDeletedVat: currencyService + 'starts/deleted'
}

export const cateringLevyUrls = {
  postCateringLevy: currencyService + 'levy/rates/new',
  getCateringLevies: currencyService + 'levy/rates/view',
  getCurrentCateringLevy: currencyService + 'levy/rates/view/latest',
  getCateringLevy: currencyService + 'levy/rates/view/',
  deleteCateringLevy: currencyService + 'levy/rates/delete/',
  editCateringLevy: currencyService + 'levy/rates/view',
  getAllCateringLevies: currencyService + 'levy/rates/view',
  restoreCateringLevy: currencyService + 'starts/restore/',
  getDeletedCateringLevy: currencyService + 'starts/deleted'
}

export const currencyUrls = {
  postExchange: currencyService + 'currency/bs/rate/new',
  postCurrency: currencyService + 'currency/new',
  getCurrencies: currencyService + 'currency/rates/view',
  getCurrency: currencyService + 'currency/rates/view/',
  getAllExchanges: currencyService + 'currency/bs/rate/view',
  getCurrentCurrency: currencyService + 'currency/rates/view/latest',
  deleteCurrency: currencyService + 'currency/rates/delete/',
  editCurrency: currencyService + 'starts/edit/',
  getAllCurrencies: currencyService + 'currency/view',
  getAllCurrencyTypes: currencyService + 'currency/view',
  getAllActiveCurrency: currencyService + 'currency/bs/rate/view/latest',
  restoreCurrency: currencyService + 'starts/restore/',
  getDeletedCurrency: currencyService + 'starts/deleted'
}
