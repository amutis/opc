import mutations from './mutations'
import actions from './actions'

const state = {
  all_vats: [],
  vats: [],
  vat: [],
  deleted_vats: []
}

export default {
  state, mutations, actions
}
