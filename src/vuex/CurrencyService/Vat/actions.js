import Vue from 'vue'
import {vatUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_vats (context) {
    Vue.http.get(vatUrls.getAllVats).then(function (response) {
      context.commit('GET_ALL_VATS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vats (context, publicId) {
    Vue.http.get(vatUrls.getVats + publicId).then(function (response) {
      context.commit('GET_VATS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_vats (context) {
    Vue.http.get(vatUrls.getDeletedVat).then(function (response) {
      context.commit('GET_DELETED_VATS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vat (context, publicId) {
    Vue.http.get(vatUrls.getVat + publicId).then(function (response) {
      context.commit('GET_VAT', response.data)
      context.dispatch('loading_false')
    })
  },
  get_current_vat (context, publicId) {
    Vue.http.get(vatUrls.getCurrentVat + publicId).then(function (response) {
      context.commit('GET_VAT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_vat (context, data) {
    Vue.http.post(vatUrls.postVat, data).then(function () {
      context.dispatch('get_all_vats')
      router.push({
        name: 'VatHistory'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_vat (context, data) {
    Vue.http.post(vatUrls.postVat, data).then(function () {
      context.dispatch('get_all_vats')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_vat (context, publicId) {
    Vue.http.delete(vatUrls.deleteVat + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_VATS', response.data)
      router.push({
        name: 'Module.DeletedVats'
      })
      context.dispatch('loading_false')
    })
  },
  restoreVat (context, publicId) {
    Vue.http.get(vatUrls.restoreVat + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VATS', response.data)
      router.push({
        name: 'Module.Vats'
      })
      context.dispatch('loading_false')
    })
  }
}
