export default {
  GET_VATS (state, data) {
    state.vats = data.data
  },
  GET_ALL_VATS (state, data) {
    state.all_vats = data.data
  },
  GET_VAT (state, data) {
    state.vat = data.data
  },
  GET_DELETED_VATS (state, data) {
    state.deleted_vats = data.data
  }
}
