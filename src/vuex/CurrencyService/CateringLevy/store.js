import mutations from './mutations'
import actions from './actions'

const state = {
  all_catering_levies: [],
  catering_levies: [],
  catering_levy: [],
  deleted_catering_levies: []
}

export default {
  state, mutations, actions
}
