export default {
  GET_CATERING_LEVIES (state, data) {
    state.catering_levies = data.data
  },
  GET_ALL_CATERING_LEVIES (state, data) {
    state.all_catering_levies = data.data
  },
  GET_CATERING_LEVY (state, data) {
    state.catering_levy = data.data
  },
  GET_DELETED_CATERING_LEVIES (state, data) {
    state.deleted_catering_levies = data.data
  }
}
