import Vue from 'vue'
import {cateringLevyUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_catering_levies (context) {
    Vue.http.get(cateringLevyUrls.getAllCateringLevies).then(function (response) {
      context.commit('GET_ALL_CATERING_LEVIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_catering_levies (context, publicId) {
    Vue.http.get(cateringLevyUrls.getCateringLevies + publicId).then(function (response) {
      context.commit('GET_CATERING_LEVIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_catering_levies (context) {
    Vue.http.get(cateringLevyUrls.getDeletedCateringLevy).then(function (response) {
      context.commit('GET_DELETED_CATERING_LEVIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_catering_levy (context, publicId) {
    Vue.http.get(cateringLevyUrls.getCateringLevy + publicId).then(function (response) {
      context.commit('GET_CATERING_LEVY', response.data)
      context.dispatch('loading_false')
    })
  },
  get_current_catering_levy (context) {
    Vue.http.get(cateringLevyUrls.getCurrentCateringLevy).then(function (response) {
      context.commit('GET_CATERING_LEVY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_catering_levy (context, data) {
    Vue.http.post(cateringLevyUrls.postCateringLevy, data).then(function () {
      context.dispatch('get_all_catering_levies')
      router.push({
        name: 'CateringLevyHistory'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_catering_levy (context, data) {
    Vue.http.post(cateringLevyUrls.editCateringLevy, data).then(function () {
      context.dispatch('get_all_catering_levies')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_catering_levy (context, publicId) {
    Vue.http.delete(cateringLevyUrls.deleteCateringLevy + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CATERING_LEVIES', response.data)
      router.push({
        name: 'Module.DeletedCateringLevies'
      })
      context.dispatch('loading_false')
    })
  },
  restoreCateringLevy (context, publicId) {
    Vue.http.get(cateringLevyUrls.restoreCateringLevy + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CATERING_LEVIES', response.data)
      router.push({
        name: 'Module.CateringLevies'
      })
      context.dispatch('loading_false')
    })
  }
}
