export default {
  GET_CURRENCIES (state, data) {
    state.currencies = data.data
  },
  GET_EXCHANGES (state, data) {
    state.exchanges = data.data
  },
  GET_ALL_CURRENCIES (state, data) {
    state.all_currencies = data.data
  },
  GET_ALL_CURRENCY_TYPE (state, data) {
    state.all_currency_types = data.data
  },
  GET_CURRENCY (state, data) {
    localStorage.setItem('base', data.data[0].base_currency_amount)
    localStorage.setItem('target', data.data[0].target_currency_amount)
    state.currency = data.data[0]
    state.browser_currency = localStorage.getItem('currency')
  },
  GET_DELETED_CURRENCIES (state, data) {
    state.deleted_currencies = data.data
  },
  LOCAL_CURRENCY (state, data) {
    state.browser_currency = data
    state.local_currency = data
  },
  LOCAL_CURRENCY2 (state, data) {
    state.browser_currency = data.currency_name
    state.browser_currency_id = data.currency_public_id
    state.local_currency = data.currency_name
  }
}
