import mutations from './mutations'
import actions from './actions'

const state = {
  all_currencies: [],
  currencies: [],
  currency: [],
  all_currency_types: [],
  deleted_currencies: [],
  browser_currency: [],
  local_currency: [],
  exchanges: [],
  browser_currency_id: ''
}

export default {
  state, mutations, actions
}
