import Vue from 'vue'
import {currencyUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_currencies (context) {
    Vue.http.get(currencyUrls.getAllCurrencies).then(function (response) {
      context.commit('GET_ALL_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_currency_types (context) {
    Vue.http.get(currencyUrls.getAllCurrencyTypes).then(function (response) {
      context.commit('GET_ALL_CURRENCY_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_exchanges (context) {
    Vue.http.get(currencyUrls.getAllActiveCurrency).then(function (response) {
      context.commit('GET_EXCHANGES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_currencies (context, publicId) {
    Vue.http.get(currencyUrls.getCurrencies + publicId).then(function (response) {
      context.commit('GET_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_currencies (context) {
    Vue.http.get(currencyUrls.getDeletedCurrency).then(function (response) {
      context.commit('GET_DELETED_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_currency (context, publicId) {
    Vue.http.get(currencyUrls.getCurrency + publicId).then(function (response) {
      context.commit('GET_CURRENCY', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_exchanges (context) {
    Vue.http.get(currencyUrls.getAllExchanges).then(function (response) {
      context.commit('GET_EXCHANGES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_current_currency (context) {
    Vue.http.get(currencyUrls.getCurrentCurrency).then(function (response) {
      context.commit('GET_CURRENCY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_exchange (context, data) {
    Vue.http.post(currencyUrls.postExchange, data).then(function () {
      context.dispatch('get_all_exchanges')
      router.push({
        name: 'CurrencyReport'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_currency (context, data) {
    Vue.http.post(currencyUrls.postCurrency, data).then(function () {
      context.dispatch('get_all_currencies')
      router.push({
        name: 'UpdateCurrency'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_currency (context, data) {
    Vue.http.post(currencyUrls.postCurrency, data).then(function () {
      context.dispatch('get_all_currencies')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_currency (context, publicId) {
    Vue.http.delete(currencyUrls.deleteCurrency + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CURRENCIES', response.data)
      router.push({
        name: 'Module.DeletedCurrencies'
      })
      context.dispatch('loading_false')
    })
  },
  restoreCurrency (context, publicId) {
    Vue.http.get(currencyUrls.restoreCurrency + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CURRENCIES', response.data)
      router.push({
        name: 'Module.Currencies'
      })
      context.dispatch('loading_false')
    })
  }
}
