export default {
  GET_GATES (state, data) {
    state.gates = data.data
  },
  GET_ALL_GATES (state, data) {
    state.all_gates = data.data
  },
  GET_GATE (state, data) {
    state.gate = data.data
  },
  GET_DELETED_GATES (state, data) {
    state.deleted_gates = data.data
  }
}
