import mutations from './mutations'
import actions from './actions'

const state = {
  all_gates: [],
  gates: [],
  gate: [],
  deleted_gates: []
}

export default {
  state, mutations, actions
}
