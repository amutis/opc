import Vue from 'vue'
import {gateUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_gates (context) {
    Vue.http.get(gateUrls.getAllGates).then(function (response) {
      context.commit('GET_ALL_GATES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_gates (context, publicId) {
    Vue.http.get(gateUrls.getGates + publicId).then(function (response) {
      context.commit('GET_GATES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_gates (context) {
    Vue.http.get(gateUrls.getDeletedGate).then(function (response) {
      context.commit('GET_DELETED_GATES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_gate (context, publicId) {
    Vue.http.get(gateUrls.getGate + publicId).then(function (response) {
      context.commit('GET_GATE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_gate (context, data) {
    Vue.http.post(gateUrls.postGate, data).then(function () {
      context.dispatch('get_all_gates')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_gate (context, data) {
    Vue.http.post(gateUrls.editGate, data).then(function () {
      context.dispatch('get_all_gates')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_gate (context, publicId) {
    Vue.http.get(gateUrls.deleteGate + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_GATES', response.data)
      router.push({
        name: 'Module.DeletedGates'
      })
      context.dispatch('loading_false')
    })
  },
  restoreGate (context, publicId) {
    Vue.http.get(gateUrls.restoreGate + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_GATES', response.data)
      router.push({
        name: 'Module.Gates'
      })
      context.dispatch('loading_false')
    })
  }
}
