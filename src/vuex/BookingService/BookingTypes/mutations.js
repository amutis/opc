export default {
  GET_BOOKING_TYPES (state, data) {
    state.booking_types = data.data
  },
  GET_ALL_BOOKING_TYPES (state, data) {
    state.all_booking_types = data.data
  },
  GET_BOOKING_TYPE (state, data) {
    state.booking_type = data.data
  },
  GET_DELETED_BOOKING_TYPES (state, data) {
    state.deleted_booking_types = data.data
  }
}
