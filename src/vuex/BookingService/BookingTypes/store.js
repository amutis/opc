import mutations from './mutations'
import actions from './actions'

const state = {
  all_booking_types: [],
  booking_types: [],
  booking_type: [],
  deleted_booking_types: []
}

export default {
  state, mutations, actions
}
