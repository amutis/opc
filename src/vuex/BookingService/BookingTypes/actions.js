import Vue from 'vue'
import {bookingTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_booking_types (context) {
    Vue.http.get(bookingTypeUrls.getAllBookingTypes).then(function (response) {
      context.commit('GET_ALL_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_booking_types (context, publicId) {
    Vue.http.get(bookingTypeUrls.getBookingTypes + publicId).then(function (response) {
      context.commit('GET_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_booking_types (context) {
    Vue.http.get(bookingTypeUrls.getDeletedBookingType).then(function (response) {
      context.commit('GET_DELETED_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_booking_type (context, publicId) {
    Vue.http.get(bookingTypeUrls.getBookingType + publicId).then(function (response) {
      context.commit('GET_BOOKING_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_booking_type (context, data) {
    Vue.http.post(bookingTypeUrls.postBookingType, data).then(function () {
      context.dispatch('get_all_booking_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_booking_type (context, data) {
    Vue.http.patch(bookingTypeUrls.editBookingType, data).then(function () {
      context.dispatch('get_all_booking_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_booking_type (context, data) {
    Vue.http.patch(bookingTypeUrls.deleteBookingType, data).then(function (response) {
      context.dispatch('get_all_booking_types')
      context.commit('GET_DELETED_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  restoreBookingType (context, publicId) {
    Vue.http.get(bookingTypeUrls.restoreBookingType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_BOOKING_TYPES', response.data)
      router.push({
        name: 'Module.BookingTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
