import {urls} from '../urls'
const bookingService = urls.developmentUrl + ':5005/'

export const paymentMethodUrls = {
  postPaymentMethod: bookingService + 'payments/methods/new',
  getPaymentMethods: bookingService + 'payments/methods/view',
  getPaymentMethod: bookingService + 'starts/single/',
  deletePaymentMethod: bookingService + 'payments/methods/delete',
  editPaymentMethod: bookingService + 'payments/default/modify',
  getAllPaymentMethods: bookingService + 'payments/methods/view',
  restorePaymentMethod: bookingService + 'starts/restore/',
  getDeletedPaymentMethod: bookingService + 'payments/default/delete'
}

export const bookingTypeUrls = {
  postBookingType: bookingService + 'bookings/types/new',
  getPublicBookingTypes: bookingService + 'bookings/public/types/view',
  getBackBookingTypes: bookingService + 'bookings/back/types/view',
  getBookingTypes: bookingService + 'bookings/types/view',
  getBookingType: bookingService + 'bookings/types/view/',
  deleteBookingType: bookingService + 'bookings/types/delete',
  editBookingType: bookingService + 'bookings/types/modify',
  getAllBookingTypes: bookingService + 'bookings/types/view',
  restoreBookingType: bookingService + 'starts/restore/',
  getDeletedBookingType: bookingService + 'starts/deleted'
}

export const bookingUrls = {
  generateReports: bookingService + 'generate/reports/csv',
  inventoryBookingSchedule: bookingService + 'bookings/inventory/view/schedule',
  facilityBookingSchedule: bookingService + 'bookings/facility/view/schedule',
  pendingPartnerBookings: bookingService + 'partners/bookings/view/pending',
  pendingGroupBookings: bookingService + 'group/bookings/view/pending',
  postPayment: bookingService + 'bookings/payment/new',
  postGroupBooking: bookingService + 'group/bookings/new',
  postPartnerBooking: bookingService + 'partners/bookings/new',
  postExpressBooking: bookingService + 'bookings/express/new',
  postBooking: bookingService + 'bookings/new',
  getCheckedInBookings: bookingService + 'bookings/view/checked_in',
  checkInventoryAvailability: bookingService + 'bookings/inventory/view/by_id_date',
  checkFacilityAvailability: bookingService + 'bookings/facility/available/date',
  getCheckedOutBookings: bookingService + 'bookings/view/checked_out',
  getConfirmedBookings: bookingService + 'bookings/view/confirmed',
  getUnConfirmedBookings: bookingService + 'bookings/view/unconfirmed',
  confirmBooking: bookingService + 'bookings/confirm',
  checkInBooking: bookingService + 'bookings/check_in',
  checkOutBooking: bookingService + 'bookings/check_out',
  reprintReceipt: bookingService + 'print/receipt',
  getBookings: bookingService + 'bookings/view',
  getBooking: bookingService + 'bookings/view/',
  deleteBooking: bookingService + 'bookings/delete',
  editBooking: bookingService + 'bookings/modify',
  getAllBookings: bookingService + 'bookings/view',
  getAllPartnerBookings: bookingService + 'partners/bookings/view',
  getPartnerBookings: bookingService + 'partners/bookings/view/partner/',
  getPartnerCheckinTodayBookings: bookingService + 'partners/bookings/view/partner/',
  getPartnerCheckoutTodayBookings: bookingService + 'partners/bookings/view/partner/',
  getCheckInToday: bookingService + 'bookings/view/check_in/today',
  getCheckOutToday: bookingService + 'bookings/view/check_out/today',
  restoreBooking: bookingService + 'starts/restore/',
  mpesaRequest: bookingService + 'bookings/payment/mpesa/new',
  requestEmailPaymentReminder: bookingService + 'reminders/',
  getDeletedBooking: bookingService + 'starts/deleted',
  generateCustomReport: bookingService + 'filtered/report'
}

export const bookingSearchUrls = {
  searchByKey: bookingService + 'bookings/search/by_key',
  searchBySingleDate: bookingService + 'bookings/search/by_date/single',
  searchCheckInByDateRange: bookingService + 'bookings/search/by_date/range/check_in',
  searchCheckOutByDateRange: bookingService + 'bookings/search/by_date/range/check_out',
  searchByDateRange: bookingService + 'bookings/search/by_date/range/check_in_out'
}

export const conservationFeesUrls = {
  postConservationFee: bookingService + 'payments/default/new',
  getConservationFees: bookingService + 'payments/default/view',
  getFullConservationFees: bookingService + 'payments/default/view',
  getConservationFee: bookingService + 'starts/single/',
  deleteConservationFee: bookingService + 'payments/default/delete',
  editConservationFee: bookingService + 'payments/default/modify',
  getAllConservationFees: bookingService + 'payments/default/view/public',
  getSchoolConservationFees: bookingService + 'payments/default/view/school',
  restoreConservationFee: bookingService + 'starts/restore/',
  getDeletedConservationFee: bookingService + 'starts/deleted'
}

export const gatePassUrls = {
  payGatePass: bookingService + 'gatepass/payments/new',
  postGatePass: bookingService + 'gatepass/new',
  postGatePassMember: bookingService + 'gatepass/members/new',
  getGatePasses: bookingService + 'gatepass/view',
  getGatePass: bookingService + 'gatepass/view/',
  getAllGatePasses: bookingService + 'gatepass/view'
}

export const calendarUrls = {
  getCalendar: bookingService + 'calendar/view',
  getFacilityBookings: bookingService + 'bookings/facility/list',
  getInventoryBookings: bookingService + 'bookings/inventory/list'
}

export const vehicleFeeUrls = {
  postVehicleFee: bookingService + 'vehicle/charges/new',
  getVehicleFees: bookingService + 'vehicle/charges/view',
  getVehicleFee: bookingService + 'starts/single/',
  deleteVehicleFee: bookingService + 'vehicle/charges/delete',
  editVehicleFee: bookingService + 'vehicle/charges/edit',
  getAllVehicleFees: bookingService + 'vehicle/charges/view',
  restoreVehicleFee: bookingService + 'starts/restore/',
  getDeletedVehicleFee: bookingService + 'starts/deleted'
}

export const destinationUrls = {
  postDestination: bookingService + 'destination/new',
  getDestinations: bookingService + 'destination/view',
  getDestination: bookingService + 'starts/single/',
  deleteDestination: bookingService + 'destination/delete',
  editDestination: bookingService + 'destination/modify',
  getAllDestinations: bookingService + 'destination/view',
  restoreDestination: bookingService + 'starts/restore/',
  getDeletedDestination: bookingService + 'starts/deleted'
}

export const groupBookingTypeUrls = {
  postGroupBookingType: bookingService + 'starts/post',
  getGroupBookingTypes: bookingService + 'bookings/group/types/view',
  getGroupBookingType: bookingService + 'starts/single/',
  deleteGroupBookingType: bookingService + 'starts/delete/',
  editGroupBookingType: bookingService + 'starts/edit/',
  getAllGroupBookingTypes: bookingService + 'bookings/group/types/view',
  restoreGroupBookingType: bookingService + 'starts/restore/',
  getDeletedGroupBookingType: bookingService + 'starts/deleted'
}

export const gateUrls = {
  postGate: bookingService + 'starts/post',
  getGates: bookingService + 'gates/view',
  getGate: bookingService + 'starts/single/',
  deleteGate: bookingService + 'starts/delete/',
  editGate: bookingService + 'starts/edit/',
  getAllGates: bookingService + 'gates/view',
  restoreGate: bookingService + 'starts/restore/',
  getDeletedGate: bookingService + 'starts/deleted'
}

export const PickUpLocationUrls = {
  postPickuplocation: bookingService + 'pick-up/new',
  getAllPick_up_locations: bookingService + 'pick-up/view',
  getPickuplocation: bookingService + 'get_single',
  editPickuplocation: bookingService + 'pick-up/modify',
  getDeletedPick_up_locations: bookingService + 'delete',
  deletePickuplocation: bookingService + 'pick-up/delete'
}

export const FacilityPricingTypeUrls = {
  postFacilitypricingtype: bookingService + 'facility/pricing/new',
  getAllFacility_pricing_types: bookingService + 'facility/pricing/view',
  getFacilitypricingtype: bookingService + 'facility/pricing/view/',
  editFacilitypricingtype: bookingService + 'facility/pricing/modify',
  getDeletedFacility_pricing_types: bookingService + 'facility/pricing/delete',
  deleteFacilitypricingtype: bookingService + 'delete'
}
