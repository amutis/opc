import mutations from './mutations'
import actions from './actions'

const state = {
  all_destinations: [],
  destinations: [],
  destination: [],
  deleted_destinations: []
}

export default {
  state, mutations, actions
}
