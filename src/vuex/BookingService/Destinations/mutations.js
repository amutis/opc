export default {
  GET_DESTINATIONS (state, data) {
    state.destinations = data.data
  },
  GET_ALL_DESTINATIONS (state, data) {
    state.all_destinations = data.data
  },
  GET_DESTINATION (state, data) {
    state.destination = data.data
  },
  GET_DELETED_DESTINATIONS (state, data) {
    state.deleted_destinations = data.data
  }
}
