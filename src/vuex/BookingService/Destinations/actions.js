import Vue from 'vue'
import {destinationUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_destinations (context) {
    Vue.http.get(destinationUrls.getAllDestinations).then(function (response) {
      context.commit('GET_ALL_DESTINATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_destinations (context, publicId) {
    Vue.http.get(destinationUrls.getDestinations + publicId).then(function (response) {
      context.commit('GET_DESTINATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_destinations (context) {
    Vue.http.get(destinationUrls.getDeletedDestination).then(function (response) {
      context.commit('GET_DELETED_DESTINATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_destination (context, publicId) {
    Vue.http.get(destinationUrls.getDestination + publicId).then(function (response) {
      context.commit('GET_DESTINATION', response.data)
      context.dispatch('loading_false')
    })
  },
  post_destination (context, data) {
    Vue.http.post(destinationUrls.postDestination, data).then(function () {
      context.dispatch('get_all_destinations')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_destination (context, data) {
    Vue.http.patch(destinationUrls.editDestination, data).then(function () {
      context.dispatch('get_all_destinations')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_destination (context, data) {
    Vue.http.post(destinationUrls.deleteDestination, data).then(function (response) {
      context.dispatch('get_all_destinations')
      context.dispatch('loading_false')
    })
  },
  restoreDestination (context, publicId) {
    Vue.http.get(destinationUrls.restoreDestination + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_DESTINATIONS', response.data)
      router.push({
        name: 'Module.Destinations'
      })
      context.dispatch('loading_false')
    })
  }
}
