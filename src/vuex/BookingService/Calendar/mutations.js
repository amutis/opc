export default {
  GET_CALENDAR (state, data) {
    state.calendar_inventory = data.inventory
    state.calendar_camping = data.camping
    state.calendar_accomodation = data.accomodation
  }
}
