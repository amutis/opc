import mutations from './mutations'
import actions from './actions'

const state = {
  calendar_inventory: [],
  calendar_camping: [],
  calendar_accomodation: []
}

export default {
  state, mutations, actions
}
