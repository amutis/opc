import Vue from 'vue'
import {calendarUrls} from '../urls'

export default {
  get_calendar (context, data) {
    Vue.http.post(calendarUrls.getCalendar, data).then(function (response) {
      context.commit('GET_CALENDAR', response.data)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_calendar_facility_bookings (context, data) {
    Vue.http.post(calendarUrls.getFacilityBookings, data).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_ALL_BOOKINGS', {data: []})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_calendar_inventory_bookings (context, data) {
    Vue.http.post(calendarUrls.getInventoryBookings, data).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_ALL_BOOKINGS', {data: []})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  }
}
