import mutations from './mutations'
import actions from './actions'

const state = {
  all_vehicle_fees: [],
  vehicle_fees: [],
  vehicle_fee: [],
  deleted_vehicle_fees: [],
  local_vehicles: [],
  vehicle_separate: false,
  disable_vehicles: false
}

export default {
  state, mutations, actions
}
