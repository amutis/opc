export default {
  GET_VEHICLE_FEES (state, data) {
    state.vehicle_fees = data.data
  },
  GET_ALL_VEHICLE_FEES (state, data) {
    state.all_vehicle_fees = data.data
  },
  GET_VEHICLE_FEE (state, data) {
    state.vehicle_fee = data.data
  },
  GET_DELETED_VEHICLE_FEES (state, data) {
    state.deleted_vehicle_fees = data.data
  },
  LOCAL_VEHICLES (state, data) {
    state.all_vehicle_fees = data
  },
  VEHICLE_SEPARATE (state, data) {
    state.vehicle_separate = data
  },
  DISABLED_STATUS (state, data) {
    state.disable_vehicles = data
  }
}
