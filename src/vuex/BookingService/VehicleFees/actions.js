import Vue from 'vue'
import {vehicleFeeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_vehicle_fees (context) {
    Vue.http.get(vehicleFeeUrls.getAllVehicleFees).then(function (response) {
      context.commit('GET_ALL_VEHICLE_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_fees (context, publicId) {
    Vue.http.get(vehicleFeeUrls.getVehicleFees + publicId).then(function (response) {
      context.commit('GET_VEHICLE_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_vehicle_fees (context) {
    Vue.http.get(vehicleFeeUrls.getDeletedVehicleFee).then(function (response) {
      context.commit('GET_DELETED_VEHICLE_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_vehicle_fee (context, publicId) {
    Vue.http.get(vehicleFeeUrls.getVehicleFee + publicId).then(function (response) {
      context.commit('GET_VEHICLE_FEE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_vehicle_fee (context, data) {
    Vue.http.post(vehicleFeeUrls.postVehicleFee, data).then(function () {
      context.dispatch('get_all_vehicle_fees')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_vehicle_fee (context, data) {
    Vue.http.patch(vehicleFeeUrls.editVehicleFee, data).then(function () {
      context.dispatch('get_all_vehicle_fees')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_vehicle_fee (context, data) {
    Vue.http.patch(vehicleFeeUrls.deleteVehicleFee, data).then(function () {
      context.dispatch('loading_false')
      context.dispatch('get_all_vehicle_fees')
    })
  },
  restoreVehicleFee (context, publicId) {
    Vue.http.get(vehicleFeeUrls.restoreVehicleFee + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VEHICLE_FEES', response.data)
      router.push({
        name: 'Module.VehicleFees'
      })
      context.dispatch('loading_false')
    })
  }
}
