import Vue from 'vue'
import {gatePassUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_gate_passes (context) {
    Vue.http.get(gatePassUrls.getAllGatePasses).then(function (response) {
      context.commit('GET_ALL_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_gate_passes (context, publicId) {
    Vue.http.get(gatePassUrls.getGatePasses + publicId).then(function (response) {
      context.commit('GET_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_gate_passes (context) {
    Vue.http.get(gatePassUrls.getDeletedGatePass).then(function (response) {
      context.commit('GET_DELETED_STARTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_gate_pass (context, publicId) {
    Vue.http.get(gatePassUrls.getGatePass + publicId).then(function (response) {
      context.commit('GET_START', response.data)
      context.dispatch('loading_false')
    })
  },
  post_gate_pass (context, data) {
    Vue.http.post(gatePassUrls.postGatePass, data).then(function () {
      context.dispatch('get_all_gate_passes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  pay_gate_pass (context, data) {
    Vue.http.post(gatePassUrls.payGatePass, data).then(function () {
      context.dispatch('get_all_gate_passes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_gate_pass_member (context, data) {
    Vue.http.post(gatePassUrls.postGatePassMember, data).then(function () {
      context.dispatch('get_all_gate_passes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_gate_pass (context, data) {
    Vue.http.post(gatePassUrls.postGatePass, data).then(function () {
      context.dispatch('get_all_gate_passes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_gate_pass (context, publicId) {
    Vue.http.get(gatePassUrls.deleteGatePass + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_STARTS', response.data)
      router.push({
        name: 'Module.DeletedGatePasses'
      })
      context.dispatch('loading_false')
    })
  },
  restoreGatePass (context, publicId) {
    Vue.http.get(gatePassUrls.restoreGatePass + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STARTS', response.data)
      router.push({
        name: 'Module.GatePasses'
      })
      context.dispatch('loading_false')
    })
  }
}
