export default {
  GET_PENDING_PARTNER_BOOKINGS (state, data) {
    state.pending_partner_bookings = data.data
  },
  GET_PENDING_GROUP_BOOKINGS (state, data) {
    state.pending_group_bookings = data.data
  },
  GET_BOOKINGS (state, data) {
    state.bookings = data.data
  },
  GET_ALL_BOOKINGS (state, data) {
    state.all_bookings = data.data
  },
  GET_BOOKING (state, data) {
    state.booking = data.data[0]
  },
  GET_DELETED_BOOKINGS (state, data) {
    state.deleted_bookings = data.data
  },
  GET_CONFIRMED_BOOKINGS (state, data) {
    state.confirmed_bookings = data.data
  },
  GET_UN_CONFIRMED_BOOKINGS (state, data) {
    state.un_confirmed_bookings = data.data
  },
  GET_CHECKED_IN_BOOKINGS (state, data) {
    state.checked_in_bookings = data.data
  },
  GET_CHECKED_OUT_BOOKINGS (state, data) {
    state.checked_out_bookings = data.data
  },
  GET_AVAILABILITY (state, data) {
    state.activity_slots = data.data[0].available_slots
  },
  REMOVE_AVAILABILITY (state) {
    state.activity_slots = 0
  },
  GET_CHECKING_IN_TODAY (state, data) {
    state.checking_in_today = data.data
  },
  GET_CHECKING_OUT_TODAY (state, data) {
    state.checking_out_today = data.data
  },
  GET_ALL_PARTNER_BOOKINGS (state, data) {
    state.all_partner_bookings = data.data
  },
  GET_PARTNER_BOOKINGS (state, data) {
    state.partner_bookings = data.data
  },
  GET_PARTNER_CHECKIN_TODAY_BOOKINGS (state, data) {
    state.partner_checkin_today_bookings = data.data
  },
  GET_PARTNER_CHECOUT_TODAY_BOOKINGS (state, data) {
    state.partner_checkout_today_bookings = data.data
  },
  GET_INVENTORY_SCHEDULE (state, data) {
    state.inventory_schedule = data.data[0]
  },
  GET_FACILITY_SCHEDULE (state, data) {
    state.facility_schedule = data.data[0]
  }
}
