import mutations from './mutations'
import actions from './actions'

const state = {
  all_bookings: [],
  bookings: [],
  booking: [],
  deleted_bookings: [],
  confirmed_bookings: [],
  un_confirmed_bookings: [],
  checked_in_bookings: [],
  checked_out_bookings: [],
  activity_slots: 0,
  checking_in_today: [],
  checking_out_today: [],
  partner_bookings: [],
  all_partner_bookings: [],
  pending_partner_bookings: [],
  pending_group_bookings: [],
  inventory_schedule: [],
  facility_schedule: [],
  partner_checkin_today_bookings: [],
  partner_checkout_today_bookings: []
}

export default {
  state, mutations, actions
}
