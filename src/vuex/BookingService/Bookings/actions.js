import Vue from 'vue'
import {bookingUrls} from '../urls'
import router from '../../../router/index'
import VueNotifications from 'vue-notifications'

export default {
  get_all_bookings (context) {
    Vue.http.get(bookingUrls.getAllBookings).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_partner_bookings (context) {
    Vue.http.get(bookingUrls.getAllPartnerBookings).then(function (response) {
      context.commit('GET_ALL_PARTNER_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_pending_partner_bookings (context) {
    Vue.http.get(bookingUrls.pendingPartnerBookings).then(function (response) {
      context.commit('GET_PENDING_PARTNER_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_pending_group_bookings (context) {
    Vue.http.get(bookingUrls.pendingGroupBookings).then(function (response) {
      context.commit('GET_PENDING_GROUP_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner_bookings (context, publicId) {
    Vue.http.get(bookingUrls.getPartnerBookings + publicId).then(function (response) {
      context.commit('GET_PARTNER_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner_checkin_today_bookings (context, publicId) {
    Vue.http.get(bookingUrls.getPartnerCheckinTodayBookings + publicId + '/check_in/today').then(function (response) {
      context.commit('GET_PARTNER_CHECKIN_TODAY_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner_checkout_today_bookings (context, publicId) {
    Vue.http.get(bookingUrls.getPartnerCheckoutTodayBookings + publicId + '/check_in/today').then(function (response) {
      context.commit('GET_PARTNER_CHECOUT_TODAY_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  send_email_payment_reminder (context, publicId) {
    Vue.http.get(bookingUrls.requestEmailPaymentReminder + publicId).then(function (response) {
      context.commit('GET_PARTNER_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_check_in_bookings_today (context) {
    Vue.http.get(bookingUrls.getCheckInToday).then(function (response) {
      context.commit('GET_CHECKING_IN_TODAY', response.data)
      context.dispatch('loading_false')
    })
  },
  get_check_out_bookings_today (context) {
    Vue.http.get(bookingUrls.getCheckOutToday).then(function (response) {
      context.commit('GET_CHECKING_OUT_TODAY', response.data)
      context.dispatch('loading_false')
    })
  },
  check_date_facility_availability (context, data) {
    Vue.http.post(bookingUrls.checkFacilityAvailability, data).then(function (response) {
      context.commit('GET_FACILITY_AVAILABILITY', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_inventory_schedule (context, data) {
    Vue.http.post(bookingUrls.inventoryBookingSchedule, data).then(function (response) {
      context.commit('GET_INVENTORY_SCHEDULE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_facility_schedule (context, data) {
    Vue.http.post(bookingUrls.facilityBookingSchedule, data).then(function (response) {
      context.commit('GET_FACILITY_SCHEDULE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  booking_mpesa_request (context, data) {
    Vue.http.post(bookingUrls.mpesaRequest, data).then(function () {
      context.dispatch('loading_false')
    }).catch(function () {
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  check_inventory_availability (context, data) {
    Vue.http.post(bookingUrls.checkInventoryAvailability, data).then(function (response) {
      if (response.body.data[0].available_slots === 0) {
        VueNotifications.error({message: 'All slots are taken for that date. Choose a different date.'})
      }
      context.commit('GET_AVAILABILITY', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_checked_in_bookings (context) {
    Vue.http.get(bookingUrls.getCheckedInBookings).then(function (response) {
      context.commit('GET_CHECKED_IN_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_checked_out_bookings (context) {
    Vue.http.get(bookingUrls.getCheckedOutBookings).then(function (response) {
      context.commit('GET_CHECKED_OUT_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_confirmed_bookings (context) {
    Vue.http.get(bookingUrls.getConfirmedBookings).then(function (response) {
      context.commit('GET_CONFIRMED_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_un_confirmed_bookings (context) {
    Vue.http.get(bookingUrls.getUnConfirmedBookings).then(function (response) {
      context.commit('GET_UN_CONFIRMED_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  generate_custom_csv (context, data) {
    Vue.http.post(bookingUrls.generateCustomReport, data).then(function (response) {
      window.open(response.body.url, '_blank')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  confirm_booking (context, data) {
    Vue.http.post(bookingUrls.confirmBooking, data).then(function () {
      context.dispatch('get_confirmed_bookings')
      context.dispatch('get_un_confirmed_bookings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  generate_booking_reports (context, data) {
    Vue.http.post(bookingUrls.generateReports, data).then(function (response) {
      window.open(response.body.url, '_blank')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_checkin (context, data) {
    Vue.http.patch(bookingUrls.checkInBooking, data).then(function (response) {
      window.open(response.body.url, '_blank')
      router.go(-3)
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_payment (context, data) {
    Vue.http.post(bookingUrls.postPayment, data).then(function () {
      context.dispatch('get_booking', data.booking_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  check_in_booking (context, data) {
    Vue.http.post(bookingUrls.checkInBooking, data).then(function (response) {
      context.dispatch('get_checked_in_bookings')
      context.dispatch('get_checked_out_bookings')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  check_out_booking (context, data) {
    Vue.http.patch(bookingUrls.checkOutBooking, data).then(function () {
      context.dispatch('get_checked_in_bookings')
      context.dispatch('get_checked_out_bookings')
      router.go(-1)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_bookings (context, publicId) {
    Vue.http.get(bookingUrls.getBookings + publicId).then(function (response) {
      context.commit('GET_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_bookings (context) {
    Vue.http.get(bookingUrls.getDeletedBooking).then(function (response) {
      context.commit('GET_DELETED_BOOKINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_booking (context, publicId) {
    Vue.http.get(bookingUrls.getBooking + publicId).then(function (response) {
      context.commit('GET_BOOKING', response.data)
      context.dispatch('loading_false')
    })
  },
  post_booking_public (context, data) {
    Vue.http.post(bookingUrls.postBooking, data).then(function (response) {
      window.location.replace('/payment/info/' + response.body.data.booking_public_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  request_receipt (context, data) {
    Vue.http.post(bookingUrls.reprintReceipt, data).then(function (response) {
      window.open(response.body.url, '_blank')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_booking (context, data) {
    Vue.http.post(bookingUrls.postBooking, data).then(function (response) {
      context.dispatch('resetForNewBooking')
      router.push({
        name: 'Receipt',
        params: {
          booking_id: response.body.data.booking_public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_group_booking (context, data) {
    Vue.http.post(bookingUrls.postGroupBooking, data).then(function (response) {
      context.dispatch('resetForNewBooking')
      router.push({
        name: 'Receipt',
        params: {
          booking_id: response.body.data.booking_public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_partner_booking (context, data) {
    Vue.http.post(bookingUrls.postPartnerBooking, data).then(function (response) {
      context.dispatch('resetForNewBooking')
      router.push({
        name: 'PartnerReceipt',
        params: {
          booking_id: response.body.data.booking_public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_express_booking (context, data) {
    Vue.http.post(bookingUrls.postExpressBooking, data).then(function (response) {
      context.dispatch('resetForNewBooking')
      router.push({
        name: 'Receipt',
        params: {
          booking_id: response.body.data.booking_public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_booking (context, data) {
    Vue.http.patch(bookingUrls.editBooking, data).then(function () {
      context.dispatch('get_all_bookings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_booking (context, data) {
    Vue.http.patch(bookingUrls.deleteBooking, data).then(function () {
      router.go(-1)
      context.dispatch('loading_false')
    })
  },
  restoreBooking (context, publicId) {
    Vue.http.get(bookingUrls.restoreBooking + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_BOOKINGS', response.data)
      router.push({
        name: 'Module.Bookings'
      })
      context.dispatch('loading_false')
    })
  },
  resetForNewBooking (context) {
    context.dispatch('get_all_vehicle_fees')
    context.dispatch('get_all_conservation_fees')
    localStorage.setItem('vehicle_data', JSON.stringify([]))
    localStorage.setItem('conservation_data', JSON.stringify([]))
    localStorage.setItem('facility_data', JSON.stringify([]))
    localStorage.setItem('activity_data', JSON.stringify([]))
  }
}
