import Vue from 'vue'
import {bookingSearchUrls} from '../urls'
import router from '../../../router/index'

export default {
  search_booking_by_key (context, data) {
    Vue.http.post(bookingSearchUrls.searchByKey, data).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.body)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  search_booking_by_date (context, data) {
    Vue.http.post(bookingSearchUrls.searchBySingleDate, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  search_checked_in_by_date_range (context, data) {
    Vue.http.post(bookingSearchUrls.searchCheckInByDateRange, data).then(function (response) {
      context.commit('GET_ALL_BOOKINGS', response.body)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  search_checked_out_by_date_range (context, data) {
    Vue.http.post(bookingSearchUrls.searchCheckOutByDateRange, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  }
}
