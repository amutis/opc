import Vue from 'vue'
import {paymentMethodUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_payment_methods (context) {
    Vue.http.get(paymentMethodUrls.getAllPaymentMethods).then(function (response) {
      context.commit('GET_ALL_PAYMENT_METHODS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_payment_methods (context, publicId) {
    Vue.http.get(paymentMethodUrls.getPaymentMethods + publicId).then(function (response) {
      context.commit('GET_PAYMENT_METHODS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_payment_methods (context) {
    Vue.http.get(paymentMethodUrls.getDeletedPaymentMethod).then(function (response) {
      context.commit('GET_DELETED_PAYMENT_METHODS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_payment_method (context, publicId) {
    Vue.http.get(paymentMethodUrls.getPaymentMethod + publicId).then(function (response) {
      context.commit('GET_PAYMENT_METHOD', response.data)
      context.dispatch('loading_false')
    })
  },
  post_payment_method (context, data) {
    Vue.http.post(paymentMethodUrls.postPaymentMethod, data).then(function () {
      context.dispatch('get_all_payment_methods')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_payment_method (context, data) {
    Vue.http.post(paymentMethodUrls.postPaymentMethod, data).then(function () {
      context.dispatch('get_all_payment_methods')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_payment_method (context, publicId) {
    Vue.http.get(paymentMethodUrls.deletePaymentMethod + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PAYMENT_METHODS', response.data)
      router.push({
        name: 'Module.DeletedPaymentMethods'
      })
      context.dispatch('loading_false')
    })
  },
  restorePaymentMethod (context, publicId) {
    Vue.http.get(paymentMethodUrls.restorePaymentMethod + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PAYMENT_METHODS', response.data)
      router.push({
        name: 'Module.PaymentMethods'
      })
      context.dispatch('loading_false')
    })
  }
}
