import mutations from './mutations'
import actions from './actions'

const state = {
  all_payment_methods: [],
  payment_methods: [],
  payment_method: [],
  deleted_payment_methods: []
}

export default {
  state, mutations, actions
}
