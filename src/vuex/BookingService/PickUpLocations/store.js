import mutations from './mutations'
import actions from './actions'

const state = {
  all_pick_up_locations: [],
  pick_up_locations: [],
  pickuplocation: [],
  deleted_pick_up_locations: []
}

export default {
  state, mutations, actions
}
