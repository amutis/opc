export default {
  GET_PICK_UP_LOCATIONS (state, data) {
    state.pick_up_locations = data.data
  },
  GET_ALL_PICK_UP_LOCATIONS (state, data) {
    state.all_pick_up_locations = data.data
  },
  GET_PICKUPLOCATION (state, data) {
    state.pickuplocation = data.data
  },
  GET_DELETED_PICK_UP_LOCATIONS (state, data) {
    state.deleted_pick_up_locations = data.data
  }
}
