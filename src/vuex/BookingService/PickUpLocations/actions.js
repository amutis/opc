import Vue from 'vue'
import {PickUpLocationUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_pick_up_locations (context) {
    Vue.http.get(PickUpLocationUrls.getAllPick_up_locations).then(function (response) {
      context.commit('GET_ALL_PICK_UP_LOCATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_PICK_UP_LOCATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_pick_up_locations (context) {
    Vue.http.get(PickUpLocationUrls.getDeletedPick_up_locations).then(function (response) {
      context.commit('GET_DELETED_PICK_UP_LOCATIONS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_PICK_UP_LOCATIONS', data)
      context.dispatch('loading_false')
    })
  },
  get_pickuplocation (context, publicId) {
    Vue.http.get(PickUpLocationUrls.getPickuplocation + publicId).then(function (response) {
      context.commit('GET_PICKUPLOCATION', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_PICKUPLOCATION', data)
      context.dispatch('loading_false')
    })
  },
  post_pickuplocation (context, data) {
    Vue.http.post(PickUpLocationUrls.postPickuplocation, data).then(function (response) {
      context.dispatch('get_all_pick_up_locations')
      context.dispatch('loading_false')
    })
  },
  update_pickuplocation (context, data) {
    Vue.http.patch(PickUpLocationUrls.editPickuplocation, data).then(function (response) {
      context.dispatch('get_all_pick_up_locations')
      context.dispatch('loading_false')
    })
  },
  delete_pickuplocation (context, data) {
    Vue.http.patch(PickUpLocationUrls.deletePickuplocation, data).then(function (response) {
      context.dispatch('get_all_pick_up_locations')
      context.dispatch('loading_false')
    })
  },
  restore_pickuplocation (context, publicId) {
    Vue.http.get(PickUpLocationUrls.restorePickuplocation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PICK_UP_LOCATIONS', response.data)
      router.push({
        name: 'Module.Pick_up_locations'
      })
      context.dispatch('loading_false')
    })
  }
}
