import mutations from './mutations'
import actions from './actions'

const state = {
  all_group_booking_types: [],
  group_booking_types: [],
  group_booking_type: [],
  deleted_group_booking_types: []
}

export default {
  state, mutations, actions
}
