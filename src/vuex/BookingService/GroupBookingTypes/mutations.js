export default {
  GET_GROUP_BOOKING_TYPES (state, data) {
    state.group_booking_types = data.data
  },
  GET_ALL_GROUP_BOOKING_TYPES (state, data) {
    state.all_group_booking_types = data.data
  },
  GET_GROUP_BOOKING_TYPE (state, data) {
    state.group_booking_type = data.data
  },
  GET_DELETED_GROUP_BOOKING_TYPES (state, data) {
    state.deleted_group_booking_types = data.data
  }
}
