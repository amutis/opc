import Vue from 'vue'
import {groupBookingTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_group_booking_types (context) {
    Vue.http.get(groupBookingTypeUrls.getAllGroupBookingTypes).then(function (response) {
      context.commit('GET_ALL_GROUP_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_group_booking_types (context, publicId) {
    Vue.http.get(groupBookingTypeUrls.getGroupBookingTypes + publicId).then(function (response) {
      context.commit('GET_GROUP_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_group_booking_types (context) {
    Vue.http.get(groupBookingTypeUrls.getDeletedGroupBookingType).then(function (response) {
      context.commit('GET_DELETED_GROUP_BOOKING_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_group_booking_type (context, publicId) {
    Vue.http.get(groupBookingTypeUrls.getGroupBookingType + publicId).then(function (response) {
      context.commit('GET_GROUP_BOOKING_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_group_booking_type (context, data) {
    Vue.http.post(groupBookingTypeUrls.postGroupBookingType, data).then(function () {
      context.dispatch('get_all_group_booking_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_group_booking_type (context, data) {
    Vue.http.post(groupBookingTypeUrls.postGroupBookingType, data).then(function () {
      context.dispatch('get_all_group_booking_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_group_booking_type (context, publicId) {
    Vue.http.get(groupBookingTypeUrls.deleteGroupBookingType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_GROUP_BOOKING_TYPES', response.data)
      router.push({
        name: 'Module.DeletedGroupBookingTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreGroupBookingType (context, publicId) {
    Vue.http.get(groupBookingTypeUrls.restoreGroupBookingType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_GROUP_BOOKING_TYPES', response.data)
      router.push({
        name: 'Module.GroupBookingTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
