import mutations from './mutations'
import actions from './actions'

const state = {
  all_conservation_fees: [],
  conservation_fees: [],
  conservation_fee: [],
  deleted_conservation_fees: [],
  all_school_conservation_fees: [],
  all_full_conservation_fees: []
}

export default {
  state, mutations, actions
}
