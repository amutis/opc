export default {
  GET_CONSERVATION_FEES (state, data) {
    state.conservation_fees = data.data
  },
  GET_ALL_CONSERVATION_FEES (state, data) {
    state.all_conservation_fees = data.data
  },
  UPDATE_CONSERVATION_FEES (state, data) {
    state.all_conservation_fees = data
  },
  GET_CONSERVATION_FEE (state, data) {
    state.conservation_fee = data.data
  },
  GET_DELETED_CONSERVATION_FEES (state, data) {
    state.deleted_conservation_fees = data.data
  },
  LOCAL_CONSERVATION (state, data) {
    state.all_conservation_fees = data
  },
  GET_ALL_SCHOOL_CONSERVATION_FEES (state, data) {
    state.all_school_conservation_fees = data.data
  },
  GET_FULL_CONSERVATION_FEES (state, data) {
    state.all_full_conservation_fees = data.data
  }
}
