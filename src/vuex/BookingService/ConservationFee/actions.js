import Vue from 'vue'
import {conservationFeesUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_conservation_fees (context) {
    Vue.http.get(conservationFeesUrls.getAllConservationFees).then(function (response) {
      context.commit('GET_ALL_CONSERVATION_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_full_conservation_fees (context) {
    Vue.http.get(conservationFeesUrls.getFullConservationFees).then(function (response) {
      context.commit('GET_FULL_CONSERVATION_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  all_school_conservation_fees (context) {
    Vue.http.get(conservationFeesUrls.getSchoolConservationFees).then(function (response) {
      context.commit('GET_ALL_SCHOOL_CONSERVATION_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_conservation_fees (context, publicId) {
    Vue.http.get(conservationFeesUrls.getConservationFees + publicId).then(function (response) {
      context.commit('GET_CONSERVATION_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_conservation_fees (context) {
    Vue.http.get(conservationFeesUrls.getDeletedConservationFee).then(function (response) {
      context.commit('GET_DELETED_CONSERVATION_FEES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_conservation_fee (context, publicId) {
    Vue.http.get(conservationFeesUrls.getConservationFee + publicId).then(function (response) {
      context.commit('GET_CONSERVATION_FEE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_conservation_fee (context, data) {
    Vue.http.post(conservationFeesUrls.postConservationFee, data).then(function () {
      context.dispatch('get_full_conservation_fees')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_conservation_fee (context, data) {
    Vue.http.patch(conservationFeesUrls.editConservationFee, data).then(function () {
      context.dispatch('get_full_conservation_fees')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_conservation_fee (context, data) {
    Vue.http.patch(conservationFeesUrls.deleteConservationFee, data).then(function () {
      context.dispatch('get_all_conservation_fees')
      context.dispatch('loading_false')
    })
  },
  restoreConservationFee (context, publicId) {
    Vue.http.get(conservationFeesUrls.restoreConservationFee + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CONSERVATION_FEES', response.data)
      router.push({
        name: 'Module.ConservationFees'
      })
      context.dispatch('loading_false')
    })
  }
}
