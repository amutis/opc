export default {
  GET_FACILITY_PRICING_TYPES (state, data) {
    state.facility_pricing_types = data.data
  },
  GET_ALL_FACILITY_PRICING_TYPES (state, data) {
    state.all_facility_pricing_types = data.data
  },
  GET_FACILITYPRICINGTYPE (state, data) {
    state.facilitypricingtype = data.data
  },
  GET_DELETED_FACILITY_PRICING_TYPES (state, data) {
    state.deleted_facility_pricing_types = data.data
  }
}
