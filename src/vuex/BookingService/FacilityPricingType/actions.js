import Vue from 'vue'
import {FacilityPricingTypeUrls} from './../urls'
import router from '../../../router/index'

export default {
  get_all_facility_pricing_types (context) {
    Vue.http.get(FacilityPricingTypeUrls.getAllFacility_pricing_types).then(function (response) {
      context.commit('GET_ALL_FACILITY_PRICING_TYPES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_FACILITY_PRICING_TYPES', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_pricing_types (context) {
    Vue.http.get(FacilityPricingTypeUrls.getDeletedFacility_pricing_types).then(function (response) {
      context.commit('GET_DELETED_FACILITY_PRICING_TYPES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_FACILITY_PRICING_TYPES', data)
      context.dispatch('loading_false')
    })
  },
  get_facilitypricingtype (context, publicId) {
    Vue.http.get(FacilityPricingTypeUrls.getFacilitypricingtype + publicId).then(function (response) {
      context.commit('GET_FACILITYPRICINGTYPE', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_FACILITYPRICINGTYPE', data)
      context.dispatch('loading_false')
    })
  },
  post_facilitypricingtype (context, data) {
    Vue.http.post(FacilityPricingTypeUrls.postFacilitypricingtype, data).then(function (response) {
      context.dispatch('get_all_facility_pricing_types')
      context.dispatch('loading_false')
    })
  },
  update_facilitypricingtype (context, data) {
    Vue.http.patch(FacilityPricingTypeUrls.editFacilitypricingtype, data).then(function (response) {
      context.dispatch('get_all_facility_pricing_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_facilitypricingtype (context, data) {
    Vue.http.patch(FacilityPricingTypeUrls.deleteFacilitypricingtype, data).then(function (response) {
      context.commit('GET_DELETED_FACILITY_PRICING_TYPES', response.data)
      router.push({
        name: 'Module.DeletedFacility_pricing_types'
      })
      context.dispatch('loading_false')
    })
  },
  restore_facilitypricingtype (context, publicId) {
    Vue.http.get(FacilityPricingTypeUrls.restoreFacilitypricingtype + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_PRICING_TYPES', response.data)
      router.push({
        name: 'Module.Facility_pricing_types'
      })
      context.dispatch('loading_false')
    })
  }
}
