import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_pricing_types: [],
  facility_pricing_types: [],
  facilitypricingtype: [],
  deleted_facility_pricing_types: []
}

export default {
  state, mutations, actions
}
