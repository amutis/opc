export default {
  GET_PROGRAMS (state, data) {
    state.programs = data.data
  },
  GET_ALL_PROGRAMS (state, data) {
    state.all_programs = data.data
  },
  GET_PROGRAM (state, data) {
    state.program = data
  },
  GET_DELETED_PROGRAMS (state, data) {
    state.deleted_programs = data.data
  },
  GET_PROGRAM_VOLUNTEERS (state, data) {
    state.program_voluntees = data.data
  }
}
