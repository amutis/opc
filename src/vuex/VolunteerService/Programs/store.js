import mutations from './mutations'
import actions from './actions'

const state = {
  all_programs: [],
  programs: [],
  program: [],
  deleted_programs: [],
  program_voluntees: []
}

export default {
  state, mutations, actions
}
