import Vue from 'vue'
import {programUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_programs (context) {
    Vue.http.get(programUrls.getAllPrograms).then(function (response) {
      context.commit('GET_ALL_PROGRAMS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_public_programs (context) {
    Vue.http.get(programUrls.getPublicProgram).then(function (response) {
      context.commit('GET_ALL_PROGRAMS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_program_genders (context) {
    Vue.http.get(programUrls.getProgramGenders).then(function (response) {
      context.commit('GET_GENDERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_programs (context, publicId) {
    Vue.http.get(programUrls.getPrograms + publicId).then(function (response) {
      context.commit('GET_PROGRAMS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_programs (context) {
    Vue.http.get(programUrls.getDeletedProgram).then(function (response) {
      context.commit('GET_DELETED_PROGRAMS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_program (context, publicId) {
    Vue.http.get(programUrls.getProgram + publicId).then(function (response) {
      context.commit('GET_PROGRAM', response.data)
      context.dispatch('loading_false')
    })
  },
  get_program_volunteers (context, publicId) {
    Vue.http.get(programUrls.getProgramVolunteers + publicId).then(function (response) {
      context.commit('GET_PROGRAM_VOLUNTEERS', response.data)
      context.dispatch('loading_false')
    })
  },
  post_program (context, data) {
    Vue.http.post(programUrls.postProgram, data).then(function (response) {
      router.push({
        name: 'VolunteerPrograms'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_program (context, data) {
    Vue.http.post(programUrls.editProgram + data.public_id, data).then(function () {
      router.push({
        name: 'VolunteerPrograms'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_program (context, data) {
    Vue.http.patch(programUrls.deleteProgram + data.public_id, data).then(function () {
      router.push({
        name: 'VolunteerPrograms'
      })
      context.dispatch('loading_false')
    })
  },
  restoreProgram (context, publicId) {
    Vue.http.get(programUrls.restoreProgram + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PROGRAMS', response.data)
      router.push({
        name: 'Module.Programs'
      })
      context.dispatch('loading_false')
    })
  }
}
