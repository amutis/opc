import Vue from 'vue'
import {volunteerUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_volunteers (context) {
    Vue.http.get(volunteerUrls.getAllVolunteers).then(function (response) {
      context.commit('GET_ALL_VOLUNTEERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_volunteers (context, publicId) {
    Vue.http.get(volunteerUrls.getVolunteers + publicId).then(function (response) {
      context.commit('GET_VOLUNTEERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_volunteers (context) {
    Vue.http.get(volunteerUrls.getDeletedVolunteer).then(function (response) {
      context.commit('GET_DELETED_VOLUNTEERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_volunteer (context, publicId) {
    Vue.http.get(volunteerUrls.getVolunteer + publicId).then(function (response) {
      context.commit('GET_VOLUNTEER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_volunteer (context, data) {
    Vue.http.post(volunteerUrls.postVolunteer, data).then(function (response) {
      router.push({
        name: 'ProgramPayment',
        params: {
          program_id: response.body.volunteer_public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  remind_volunteer_of_payment (context, data) {
    Vue.http.post(volunteerUrls.remindVolunteerPayment, data).then(function (response) {
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_volunteer (context, data) {
    Vue.http.post(volunteerUrls.editVolunteer, data).then(function () {
      context.dispatch('get_all_volunteers')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_volunteer (context, publicId) {
    Vue.http.get(volunteerUrls.deleteVolunteer + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_VOLUNTEERS', response.data)
      router.push({
        name: 'Module.DeletedVolunteers'
      })
      context.dispatch('loading_false')
    })
  },
  restoreVolunteer (context, publicId) {
    Vue.http.get(volunteerUrls.restoreVolunteer + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VOLUNTEERS', response.data)
      router.push({
        name: 'Module.Volunteers'
      })
      context.dispatch('loading_false')
    })
  }
}
