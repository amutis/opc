import mutations from './mutations'
import actions from './actions'

const state = {
  all_volunteers: [],
  volunteers: [],
  volunteer: [],
  deleted_volunteers: []
}

export default {
  state, mutations, actions
}
