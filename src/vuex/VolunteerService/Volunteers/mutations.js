export default {
  GET_VOLUNTEERS (state, data) {
    state.volunteers = data.data
  },
  GET_ALL_VOLUNTEERS (state, data) {
    state.all_volunteers = data.data
  },
  GET_VOLUNTEER (state, data) {
    state.volunteer = data
  },
  GET_DELETED_VOLUNTEERS (state, data) {
    state.deleted_volunteers = data.data
  }
}
