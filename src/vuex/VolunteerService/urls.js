import {urls} from '../urls'
export const volunteerService = urls.developmentUrl + ':5012/v1/'

export const countryUrls = {
  postCountry: volunteerService + 'starts/post',
  getCountries: volunteerService + 'countries',
  getCountry: volunteerService + 'starts/single/',
  deleteCountry: volunteerService + 'starts/delete/',
  editCountry: volunteerService + 'starts/edit/',
  getAllCountries: volunteerService + 'countries',
  restoreCountry: volunteerService + 'starts/restore/',
  getDeletedCountry: volunteerService + 'starts/deleted'
}

export const programUrls = {
  thumbnailUrl: volunteerService + 'profile/upload/',
  postProgram: volunteerService + 'add/volunteer/program',
  getProgramVolunteers: volunteerService + 'program/volunteers/',
  getPrograms: volunteerService + 'programs',
  getPublicProgram: volunteerService + 'public/programs',
  getProgram: volunteerService + 'programs/',
  deleteProgram: volunteerService + 'remove/program/',
  editProgram: volunteerService + 'update/program/',
  getAllPrograms: volunteerService + 'programs',
  getProgramGenders: volunteerService + 'genders',
  restoreProgram: volunteerService + 'starts/restore/',
  getDeletedProgram: volunteerService + 'starts/deleted'
}

export const volunteerUrls = {
  pushToSalesForce: volunteerService + 'volunteers/salesforce',
  postVolunteer: volunteerService + 'add/volunteer',
  remindVolunteerPayment: volunteerService + 'payment/reminder',
  getVolunteers: volunteerService + 'volunteers',
  getVolunteer: volunteerService + 'volunteers/',
  deleteVolunteer: volunteerService + 'starts/delete/',
  editVolunteer: volunteerService + 'starts/edit/',
  getAllVolunteers: volunteerService + 'volunteers',
  restoreVolunteer: volunteerService + 'starts/restore/',
  getDeletedVolunteer: volunteerService + 'starts/deleted'
}

export const VolunteerContactUrls = {
  postVolunteercontact: volunteerService + 'add/squad',
  getAllVolunteer_contacts: volunteerService + 'notification/squad/',
  getVolunteercontact: volunteerService + 'get_single',
  editVolunteercontact: volunteerService + 'update/squad',
  getDeletedVolunteer_contacts: volunteerService + 'delete',
  deleteVolunteercontact: volunteerService + 'delete/squad/'
}
