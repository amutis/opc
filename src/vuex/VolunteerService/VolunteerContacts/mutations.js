export default {
  GET_VOLUNTEER_CONTACTS (state, data) {
    state.volunteer_contacts = data.data
  },
  GET_ALL_VOLUNTEER_CONTACTS (state, data) {
    state.all_volunteer_contacts = data.data
  },
  GET_VOLUNTEERCONTACT (state, data) {
    state.volunteercontact = data.data
  },
  GET_DELETED_VOLUNTEER_CONTACTS (state, data) {
    state.deleted_volunteer_contacts = data.data
  }
}
