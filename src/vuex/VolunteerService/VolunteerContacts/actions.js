import Vue from 'vue'
import {VolunteerContactUrls} from './../urls'
import router from '../../../router/index'

export default {
  get_all_volunteer_contacts (context, programId) {
    Vue.http.get(VolunteerContactUrls.getAllVolunteer_contacts + programId).then(function (response) {
      context.commit('GET_ALL_VOLUNTEER_CONTACTS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_VOLUNTEER_CONTACTS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_volunteer_contacts (context) {
    Vue.http.get(VolunteerContactUrls.getDeletedVolunteer_contacts).then(function (response) {
      context.commit('GET_DELETED_VOLUNTEER_CONTACTS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_VOLUNTEER_CONTACTS', data)
      context.dispatch('loading_false')
    })
  },
  get_volunteercontact (context, publicId) {
    Vue.http.get(VolunteerContactUrls.getVolunteercontact + publicId).then(function (response) {
      context.commit('GET_VOLUNTEERCONTACT', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_VOLUNTEERCONTACT', data)
      context.dispatch('loading_false')
    })
  },
  post_volunteercontact (context, data) {
    Vue.http.post(VolunteerContactUrls.postVolunteercontact, data).then(function (response) {
      context.dispatch('get_all_volunteer_contacts', data.program_id)
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  update_volunteercontact (context, data) {
    Vue.http.post(VolunteerContactUrls.editVolunteercontact, data).then(function (response) {
      context.dispatch('get_all_volunteer_contacts', data.program_id)
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_volunteercontact (context, data) {
    Vue.http.delete(VolunteerContactUrls.deleteVolunteercontact + data.public_id).then(function (response) {
      context.dispatch('get_all_volunteer_contacts', data.program_id)
      context.dispatch('loading_false')
    })
  },
  restore_volunteercontact (context, publicId) {
    Vue.http.get(VolunteerContactUrls.restoreVolunteercontact + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VOLUNTEER_CONTACTS', response.data)
      router.push({
        name: 'Module.Volunteer_contacts'
      })
      context.dispatch('loading_false')
    })
  }
}
