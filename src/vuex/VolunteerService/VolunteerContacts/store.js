import mutations from './mutations'
import actions from './actions'

const state = {
  all_volunteer_contacts: [],
  volunteer_contacts: [],
  volunteercontact: [],
  deleted_volunteer_contacts: []
}

export default {
  state, mutations, actions
}
