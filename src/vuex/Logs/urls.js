// If there is an external url file uncomment below and replace main url with the import
// import url from ''

const mainUrl = 'http://../'

export const LogUrls = {
  postLog: mainUrl + 'post',
  getAllLogs: mainUrl + 'get_all',
  getLog: mainUrl + 'get_single',
  editLog: mainUrl + 'update',
  getDeletedLogs: mainUrl + 'delete,'
  deleteLog: mainUrl + 'delete'
}