export default {
  GET_LOGS (state, data) {
    state.logs = data.data
  },
  GET_ALL_LOGS (state, data) {
    state.all_logs = data.data
  },
  GET_LOG (state, data) {
    state.log = data.data
  },
  GET_DELETED_LOGS (state, data) {
    state.deleted_logs = data.data
  }
}
