import axios from 'axios'
import {LogUrls} from './urls'
import router from '../../router/index'

export default {
  get_all_logs (context) {
    axios.get(LogUrls.getAllLogs).then(function (response) {
      context.commit('GET_ALL_LOGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_ALL_LOGS', data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_logs (context) {
    axios.get(LogUrls.getDeletedLogs).then(function (response) {
      context.commit('GET_DELETED_LOGS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_DELETED_LOGS', data)
      context.dispatch('loading_false')
    })
  },
  get_log (context, publicId) {
    axios.get(LogUrls.getLog + publicId).then(function (response) {
      context.commit('GET_LOG', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      var data = {data: []}
      context.commit('GET_LOG', data)
      context.dispatch('loading_false')
    })
  },
  post_log (context, data) {
    axios.post(LogUrls.postLog, data).then(function (response) {
      context.dispatch('get_all_logs')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  update_log (context, data) {
    axios.patch(LogUrls.editLog, data).then(function (response) {
      context.dispatch('get_all_logs')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
     })
  },
  delete_log (context, publicId) {
    axios.delete(LogUrls.deleteLog + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_LOGS', response.data)
      router.push({
        name: 'Module.DeletedLogs'
      })
      context.dispatch('loading_false')
    })
  },
  restore_log (context, publicId) {
    axios.get(LogUrls.restoreLog + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_LOGS', response.data)
      router.push({
        name: 'Module.Logs'
      })
      context.dispatch('loading_false')
    })
  }
}
