export default {
  GET_ACTIVITY_CURRENCIES (state, data) {
    state.activity_currencies = data.data
  },
  GET_ALL_ACTIVITY_CURRENCIES (state, data) {
    state.all_activity_currencies = data.data
  },
  GET_ACTIVITY_CURRENCY (state, data) {
    state.activity_currency = data.data
  },
  GET_DELETED_ACTIVITY_CURRENCIES (state, data) {
    state.deleted_activity_currencies = data.data
  }
}
