import Vue from 'vue'
import {activityCurrencyUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_currencies (context) {
    Vue.http.get(activityCurrencyUrls.getAllActivityCurrencies).then(function (response) {
      context.commit('GET_ALL_ACTIVITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_currencies (context, publicId) {
    Vue.http.get(activityCurrencyUrls.getActivityCurrencies + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activity_currencies (context) {
    Vue.http.get(activityCurrencyUrls.getDeletedActivityCurrency).then(function (response) {
      context.commit('GET_DELETED_ACTIVITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_currency (context, publicId) {
    Vue.http.get(activityCurrencyUrls.getActivityCurrency + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CURRENCY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_currency (context, data) {
    Vue.http.post(activityCurrencyUrls.postActivityCurrency, data).then(function () {
      context.dispatch('get_all_activity_currencies')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_currency (context, data) {
    Vue.http.post(activityCurrencyUrls.editActivityCurrency + data.public_id, data).then(function () {
      context.dispatch('get_all_activity_currencies')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_currency (context, data) {
    Vue.http.post(activityCurrencyUrls.deleteActivityCurrency + data.public_id, data).then(function (response) {
      context.dispatch('get_all_activity_currencies')
      context.dispatch('loading_false')
    })
  },
  restoreActivityCurrency (context, publicId) {
    Vue.http.get(activityCurrencyUrls.restoreActivityCurrency + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITY_CURRENCIES', response.data)
      router.push({
        name: 'Module.ActivityCurrencies'
      })
      context.dispatch('loading_false')
    })
  }
}
