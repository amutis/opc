import mutations from './mutations'
import actions from './actions'

const state = {
  all_activity_currencies: [],
  activity_currencies: [],
  activity_currency: [],
  deleted_activity_currencies: []
}

export default {
  state, mutations, actions
}
