import mutations from './mutations'
import actions from './actions'

const state = {
  all_activity_contacts: [],
  activity_contacts: [],
  activity_contact: [],
  deleted_activity_contacts: []
}

export default {
  state, mutations, actions
}
