import Vue from 'vue'
import {activityContactUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_contacts (context, publicId) {
    Vue.http.get(activityContactUrls.getAllActivityContact + publicId).then(function (response) {
      context.commit('GET_ALL_ACTIVITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_contacts (context, publicId) {
    Vue.http.get(activityContactUrls.getAllActivityContact + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_contacts (context, publicId) {
    Vue.http.get(activityContactUrls.getActiveActivityContact + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_in_active_contacts (context, publicId) {
    Vue.http.get(activityContactUrls.getInActiveActivityContact + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activity_contacts (context) {
    Vue.http.get(activityContactUrls.getDeletedActivityContact).then(function (response) {
      context.commit('GET_DELETED_ACTIVITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_contact (context, publicId) {
    Vue.http.get(activityContactUrls.getActivityContact + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_CONTACT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_contact (context, data) {
    Vue.http.post(activityContactUrls.postActivityContact, data).then(function () {
      context.dispatch('get_activity_contacts', data.inventory_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  make_activity_contact_active (context, data) {
    Vue.http.get(activityContactUrls.makeActivityContactActive + data.public_id).then(function () {
      context.commit('GET_ALL_ACTIVITY_CONTACTS', {data: []})
      context.dispatch('get_all_activity_contacts', data.inventory_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  make_activity_contact_in_active (context, data) {
    Vue.http.get(activityContactUrls.makeActivityContactInActive + data.public_id).then(function () {
      context.commit('GET_ALL_ACTIVITY_CONTACTS', {data: []})
      context.dispatch('get_all_activity_contacts', data.inventory_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_contact (context, data) {
    Vue.http.post(activityContactUrls.editActivityContact, data).then(function () {
      context.dispatch('get_all_activity_contacts')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_contact (context, data) {
    Vue.http.post(activityContactUrls.deleteActivityContact + data.public_id, data).then(function () {
      context.dispatch('get_activity_contacts', data.inventory_id)
      context.dispatch('loading_false')
    })
  },
  restoreActivityContact (context, publicId) {
    Vue.http.get(activityContactUrls.restoreActivityContact + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITY_CONTACTS', response.data)
      router.push({
        name: 'Module.ActivityContacts'
      })
      context.dispatch('loading_false')
    })
  }
}
