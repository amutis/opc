export default {
  GET_ACTIVITY_CONTACTS (state, data) {
    state.activity_contacts = data.data
  },
  GET_ALL_ACTIVITY_CONTACTS (state, data) {
    state.all_activity_contacts = data.data
  },
  GET_ACTIVITY_CONTACT (state, data) {
    state.activity_contact = data.data
  },
  GET_DELETED_ACTIVITY_CONTACTS (state, data) {
    state.deleted_activity_contacts = data.data
  }
}
