export default {
  GET_ACTIVITY_PRICINGS (state, data) {
    state.activity_pricings = data.data
  },
  GET_ALL_ACTIVITY_PRICINGS (state, data) {
    state.all_activity_pricings = data.data
  },
  GET_ACTIVITY_PRICING (state, data) {
    state.activity_pricing = data.data
  },
  GET_DELETED_ACTIVITY_PRICINGS (state, data) {
    state.deleted_activity_pricings = data.data
  }
}
