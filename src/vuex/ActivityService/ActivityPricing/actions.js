import Vue from 'vue'
import {activityPricingUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_pricings (context) {
    Vue.http.get(activityPricingUrls.getAllActivityPricings).then(function (response) {
      context.commit('GET_ALL_ACTIVITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_current_activity_pricing (context, publicId) {
    Vue.http.get(activityPricingUrls.getCurrentActivityPricing + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_pricings (context, publicId) {
    Vue.http.get(activityPricingUrls.getActivityPricings + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activity_pricings (context) {
    Vue.http.get(activityPricingUrls.getDeletedActivityPricing).then(function (response) {
      context.commit('GET_DELETED_ACTIVITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_pricing (context, publicId) {
    Vue.http.get(activityPricingUrls.getActivityPricing + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_PRICING', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_pricing (context, data) {
    Vue.http.post(activityPricingUrls.postActivityPricing, data).then(function () {
      context.dispatch('get_activity_pricings', data.public_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_pricing (context, data) {
    Vue.http.post(activityPricingUrls.editActivityPricing, data).then(function () {
      context.dispatch('get_all_activity_pricings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_pricing (context, data) {
    Vue.http.post(activityPricingUrls.deleteActivityPricing + data.public_id, data).then(function (response) {
      context.dispatch('get_activity_pricings', data.inventory)
      context.dispatch('loading_false')
    })
  },
  restoreActivityPricing (context, publicId) {
    Vue.http.get(activityPricingUrls.restoreActivityPricing + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITY_PRICINGS', response.data)
      router.push({
        name: 'Module.ActivityPricings'
      })
      context.dispatch('loading_false')
    })
  }
}
