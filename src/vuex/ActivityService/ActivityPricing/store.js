import mutations from './mutations'
import actions from './actions'

const state = {
  all_activity_pricings: [],
  activity_pricings: [],
  activity_pricing: [],
  deleted_activity_pricings: []
}

export default {
  state, mutations, actions
}
