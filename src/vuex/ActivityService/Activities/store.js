import mutations from './mutations'
import actions from './actions'

const state = {
  all_activities: [],
  activities: [],
  activity: [],
  deleted_activities: [],
  active_activities: [],
  in_active_activities: [],
  activity_images: [],
  chosenInventory: [],
  local_activity: []
}

export default {
  state, mutations, actions
}
