export default {
  GET_ACTIVITIES (state, data) {
    state.activities = data.data
  },
  GET_ALL_ACTIVITIES (state, data) {
    state.all_activities = data.data
  },
  GET_ACTIVITY (state, data) {
    state.activity = data.data[0]
  },
  GET_DELETED_ACTIVITIES (state, data) {
    state.deleted_activities = data.data
  },
  GET_ACTIVE_ACTIVITIES (state, data) {
    state.active_activities = data.data
  },
  GET_IN_ACTIVE_ACTIVITIES (state, data) {
    state.in_active_activities = data.data
  },
  GET_ACTIVITY_IMAGES (state, data) {
    state.activity_images = data.data
  },
  CHOSEN_ACTIVITIES (state, data) {
    state.chosenInventory = data
  },
  ADD_ACTIVITY (state, data) {
    console.log('activities', data)
    state.chosenInventory.push(data)
  },
  REMOVE_INVENTORY (state, data) {
    state.chosenInventory.pop(data)
  }
}
