import Vue from 'vue'
import {activityUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activities (context) {
    Vue.http.get(activityUrls.getAllActivitys).then(function (response) {
      context.commit('GET_ALL_ACTIVITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_activities (context) {
    Vue.http.get(activityUrls.getActiveActivities).then(function (response) {
      context.commit('GET_ACTIVE_ACTIVITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_in_active_activities (context) {
    Vue.http.get(activityUrls.getInActiveActivities).then(function (response) {
      context.commit('GET_IN_ACTIVE_ACTIVITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_images (context, publicId) {
    Vue.http.get(activityUrls.getActivityImages + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_IMAGES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_ACTIVITY_IMAGES', [])
    })
  },
  delete_activity_image (context, data) {
    Vue.http.get(activityUrls.deleteActivityImage + data.public_id).then(function (response) {
      context.dispatch('get_activity_images', data.inventory_id)
      context.dispatch('loading_false')
    })
  },
  make_activity_inactive (context, data) {
    Vue.http.post(activityUrls.makeActivityInactive, data).then(function () {
      context.dispatch('get_all_activities')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  make_activity_active (context, data) {
    Vue.http.post(activityUrls.makeActivityActive, data).then(function () {
      context.dispatch('get_all_activities')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  get_activities (context, publicId) {
    Vue.http.get(activityUrls.getActivitys + publicId).then(function (response) {
      context.commit('GET_ACTIVITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activities (context) {
    Vue.http.get(activityUrls.getDeletedActivity).then(function (response) {
      context.commit('GET_DELETED_ACTIVITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity (context, publicId) {
    context.commit('GET_ACTIVITY', {data: [{}]})
    Vue.http.get(activityUrls.getActivity + publicId).then(function (response) {
      context.commit('GET_ACTIVITY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity (context, data) {
    Vue.http.post(activityUrls.postActivity, data).then(function () {
      context.dispatch('get_all_activities')
      router.push({
        name: 'Inventory'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity (context, data) {
    Vue.http.post(activityUrls.editActivity + data.public_id, data).then(function () {
      context.dispatch('get_all_activities')
      router.push({
        name: 'Inventory',
        params: {
          inventory_id: data.public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity (context, data) {
    Vue.http.post(activityUrls.deleteActivity + data.inventory_id, data).then(function () {
      router.push({
        name: 'Inventory'
      })
      context.dispatch('loading_false')
    })
  },
  restoreActivity (context, publicId) {
    Vue.http.get(activityUrls.restoreActivity + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITIES', response.data)
      router.push({
        name: 'Module.Activitys'
      })
      context.dispatch('loading_false')
    })
  }
}
