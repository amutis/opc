export default {
  GET_ACTIVITY_TYPES (state, data) {
    state.activity_types = data.data
  },
  GET_ALL_ACTIVITY_TYPES (state, data) {
    state.all_activity_types = data.data
  },
  GET_ACTIVITY_TYPE (state, data) {
    state.activity_type = data.data
  },
  GET_DELETED_ACTIVITY_TYPES (state, data) {
    state.deleted_activity_types = data.data
  }
}
