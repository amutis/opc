import Vue from 'vue'
import {activityTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_types (context) {
    Vue.http.get(activityTypeUrls.getAllActivityTypes).then(function (response) {
      context.commit('GET_ALL_ACTIVITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_types (context, publicId) {
    Vue.http.get(activityTypeUrls.getActivityTypes + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activity_types (context) {
    Vue.http.get(activityTypeUrls.getDeletedActivityType).then(function (response) {
      context.commit('GET_DELETED_ACTIVITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_type (context, publicId) {
    Vue.http.get(activityTypeUrls.getActivityType + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_type (context, data) {
    Vue.http.post(activityTypeUrls.postActivityType, data).then(function () {
      context.dispatch('get_all_activity_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_type (context, data) {
    Vue.http.post(activityTypeUrls.editActivityType, data).then(function () {
      context.dispatch('get_all_activity_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_type (context, data) {
    Vue.http.post(activityTypeUrls.deleteActivityType + data.public_id, data).then(function (response) {
      context.dispatch('get_all_activity_types')
      context.dispatch('loading_false')
    })
  },
  restoreActivityType (context, publicId) {
    Vue.http.get(activityTypeUrls.restoreActivityType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITY_TYPES', response.data)
      router.push({
        name: 'Module.ActivityTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
