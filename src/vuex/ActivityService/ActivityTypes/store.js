import mutations from './mutations'
import actions from './actions'

const state = {
  all_activity_types: [],
  activity_types: [],
  activity_type: [],
  deleted_activity_types: []
}

export default {
  state, mutations, actions
}
