import mutations from './mutations'
import actions from './actions'

const state = {
  all_a_discount_types: [],
  facility_a_discount_types: [],
  facility_a_discount_type: [],
  deleted_a_discount_types: []
}

export default {
  state, mutations, actions
}
