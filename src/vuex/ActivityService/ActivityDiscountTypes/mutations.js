export default {
  GET_A_DISCOUNT_TYPES (state, data) {
    state.a_discount_types = data.data
  },
  GET_ALL_A_DISCOUNT_TYPES (state, data) {
    state.all_a_discount_types = data.data
  },
  GET_A_DISCOUNT_TYPE (state, data) {
    state.a_discount_type = data.data
  },
  GET_DELETED_A_DISCOUNT_TYPES (state, data) {
    state.deleted_a_discount_types = data.data
  }
}
