import Vue from 'vue'
import {activityDiscountTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_discount_types (context) {
    Vue.http.get(activityDiscountTypeUrls.getAllActivityDiscountTypes).then(function (response) {
      context.commit('GET_ALL_A_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_discount_types (context, publicId) {
    Vue.http.get(activityDiscountTypeUrls.getActivityDiscountTypes + publicId).then(function (response) {
      context.commit('GET_A_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_activity_discount_types (context) {
    Vue.http.get(activityDiscountTypeUrls.getDeletedActivityDiscountType).then(function (response) {
      context.commit('GET_DELETED_A_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_discount_type (context, publicId) {
    Vue.http.get(activityDiscountTypeUrls.getActivityDiscountType + publicId).then(function (response) {
      context.commit('GET_A_DISCOUNT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_discount_type (context, data) {
    Vue.http.post(activityDiscountTypeUrls.postActivityDiscountType, data).then(function () {
      context.dispatch('get_all_activity_discount_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_discount_type (context, data) {
    Vue.http.post(activityDiscountTypeUrls.editActivityDiscountType, data).then(function () {
      context.dispatch('get_all_activity_discount_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_discount_type (context, publicId) {
    Vue.http.get(activityDiscountTypeUrls.deleteActivityDiscountType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_A_DISCOUNT_TYPES', response.data)
      router.push({
        name: 'Module.DeletedActivityDiscountTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreActivityDiscountType (context, publicId) {
    Vue.http.get(activityDiscountTypeUrls.restoreActivityDiscountType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_A_DISCOUNT_TYPES', response.data)
      router.push({
        name: 'Module.ActivityDiscountTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
