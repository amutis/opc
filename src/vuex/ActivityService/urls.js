import {urls} from '../urls'
export const activityService = urls.developmentUrl + ':5002/'

export const activityTypeUrls = {
  postActivityType: activityService + 'inventory/type/save',
  getActivityTypes: activityService + 'inventory/types',
  getActivityType: activityService + 'starts/single/',
  deleteActivityType: activityService + 'inventory/type/delete/',
  editActivityType: activityService + 'inventory/alter/type/',
  getAllActivityTypes: activityService + 'inventory/types',
  restoreActivityType: activityService + 'starts/restore/',
  getDeletedActivityType: activityService + 'starts/deleted'
}

export const activityCurrencyUrls = {
  postActivityCurrency: activityService + 'inventory/currency/save',
  getActivityCurrencies: activityService + 'inventory/currency',
  getActivityCurrency: activityService + 'inventory/currency/',
  deleteActivityCurrency: activityService + 'inventory/currency/delete/',
  editActivityCurrency: activityService + 'inventory/currency/alter/',
  getAllActivityCurrencies: activityService + 'inventory/currency',
  restoreActivityCurrency: activityService + 'starts/restore/',
  getDeletedActivityCurrency: activityService + 'starts/deleted'
}

export const activityContactUrls = {
  postActivityContact: activityService + 'inventory/contact/save',
  getActivityContacts: activityService + 'starts',
  getAllActivityContact: activityService + 'inventory/contact/',
  getActiveActivityContact: activityService + 'inventory/contact/active/',
  getInActiveActivityContact: activityService + 'inventory/contact/inactive/',
  makeActivityContactActive: activityService + 'inventory/contact/alter/status/active/',
  makeActivityContactInActive: activityService + 'inventory/contact/alter/status/inactive/',
  deleteActivityContact: activityService + 'inventory/contact/delete/',
  editActivityContact: activityService + 'inventory/contact/alter/',
  getAllActivityContacts: activityService + 'starts/all',
  restoreActivityContact: activityService + 'starts/restore/',
  getDeletedActivityContact: activityService + 'starts/deleted'
}

export const activityDiscountTypeUrls = {
  postActivityDiscountType: activityService + 'discount/type/save',
  getActivityDiscountTypes: activityService + 'discount/types',
  getActivityDiscountType: activityService + 'discount/type/',
  deleteActivityDiscountType: activityService + 'discount/type/delete/',
  editActivityDiscountType: activityService + 'discount/type/alter/',
  getAllActivityDiscountTypes: activityService + 'discount/types',
  restoreActivityDiscountType: activityService + 'starts/restore/',
  getDeletedActivityDiscountType: activityService + 'starts/deleted'
}

export const activityDiscountUrls = {
  postActivityDiscount: activityService + 'discount/save',
  getActivityDiscounts: activityService + 'discounts/',
  getActivityDiscount: activityService + 'starts/single/',
  deleteActivityDiscount: activityService + 'discount/delete/',
  editActivityDiscount: activityService + 'discount/alter/',
  getAllActivityDiscounts: activityService + 'starts/all',
  restoreActivityDiscount: activityService + 'starts/restore/',
  getDeletedActivityDiscount: activityService + 'starts/deleted'
}

export const activityPricingUrls = {
  postActivityPricing: activityService + 'inventory/pricing/save',
  getActivityPricings: activityService + 'inventory/pricing/',
  getCurrentActivityPricing: activityService + 'inventory/current/pricing/',
  getActivityPricing: activityService + 'inventory/pricing/',
  deleteActivityPricing: activityService + 'inventory/pricing/delete/',
  editActivityPricing: activityService + 'inventory/pricing/alter/',
  getAllActivityPricings: activityService + 'starts/all',
  restoreActivityPricing: activityService + 'starts/restore/',
  getDeletedActivityPricing: activityService + 'starts/deleted'
}

export const activityUrls = {
  postActivity: activityService + 'inventory/save',
  getActivities: activityService + 'starts',
  getActiveActivities: activityService + 'inventory/active',
  getInActiveActivities: activityService + 'inventory/inactive',
  getActivity: activityService + 'inventory/',
  deleteActivityImage: activityService + 'inventory/image/delete/',
  getActivityImages: activityService + 'inventory/multiple/images/',
  makeActivityInactive: activityService + 'inventory/set/inactive/',
  makeActivityActive: activityService + 'inventory/set/active/',
  deleteActivity: activityService + 'inventory/delete/',
  editActivity: activityService + 'inventory/alter/',
  getAllActivities: activityService + 'starts/all',
  restoreActivity: activityService + 'starts/restore/',
  getDeletedActivity: activityService + 'starts/deleted'
}
