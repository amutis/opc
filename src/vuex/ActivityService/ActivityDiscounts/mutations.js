export default {
  GET_ACTIVITY_DISCOUNTS (state, data) {
    state.activity_discounts = data.data
  },
  GET_ALL_ACTIVITY_DISCOUNTS (state, data) {
    state.all_activity_discounts = data.data
  },
  GET_ACTIVITY_DISCOUNT (state, data) {
    state.activity_discount = data.data
  },
  GET_DELETED_ACTIVITY_DISCOUNTS (state, data) {
    state.deleted_activity_discounts = data.data
  }
}
