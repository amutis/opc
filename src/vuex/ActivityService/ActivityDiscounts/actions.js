import Vue from 'vue'
import {activityDiscountUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_activity_discounts (context) {
    Vue.http.get(activityDiscountUrls.getAllActivityDiscounts).then(function (response) {
      context.commit('GET_ALL_ACTIVITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_discounts (context, publicId) {
    Vue.http.get(activityDiscountUrls.getActivityDiscounts + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_ACTIVITY_DISCOUNTS', [])
    })
  },
  get_deleted_activity_discounts (context) {
    Vue.http.get(activityDiscountUrls.getDeletedActivityDiscount).then(function (response) {
      context.commit('GET_DELETED_ACTIVITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_activity_discount (context, publicId) {
    Vue.http.get(activityDiscountUrls.getActivityDiscount + publicId).then(function (response) {
      context.commit('GET_ACTIVITY_DISCOUNT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_activity_discount (context, data) {
    Vue.http.post(activityDiscountUrls.postActivityDiscount, data).then(function () {
      context.dispatch('get_activity_discounts', data.inventory_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_activity_discount (context, data) {
    Vue.http.post(activityDiscountUrls.editActivityDiscount, data).then(function () {
      context.dispatch('get_all_activity_discounts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_activity_discount (context, data) {
    Vue.http.post(activityDiscountUrls.deleteActivityDiscount + data.public_id, data).then(function (response) {
      context.dispatch('get_activity_discounts', data.inventory_id)
      context.dispatch('loading_false')
    })
  },
  restoreActivityDiscount (context, publicId) {
    Vue.http.get(activityDiscountUrls.restoreActivityDiscount + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACTIVITY_DISCOUNTS', response.data)
      router.push({
        name: 'Module.ActivityDiscounts'
      })
      context.dispatch('loading_false')
    })
  }
}
