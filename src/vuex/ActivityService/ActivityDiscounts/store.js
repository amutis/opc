import mutations from './mutations'
import actions from './actions'

const state = {
  all_activity_discounts: [],
  activity_discounts: [],
  activity_discount: [],
  deleted_activity_discounts: []
}

export default {
  state, mutations, actions
}
