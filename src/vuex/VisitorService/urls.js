import {urls} from '../urls'
export const visitorService = urls.developmentUrl + ':5009/'

export const identificationTypeUrls = {
  postIdentificationType: visitorService + 'identification/types/new',
  getIdentificationTypes: visitorService + 'identification/types/view',
  getIdentificationType: visitorService + 'identification/types/view/',
  deleteIdentificationType: visitorService + 'identification/types/delete',
  editIdentificationType: visitorService + 'identification/types/edit',
  getAllIdentificationTypes: visitorService + 'identification/types/view',
  restoreIdentificationType: visitorService + 'starts/restore/',
  getDeletedIdentificationType: visitorService + 'starts/deleted'
}

export const visitorUrls = {
  postVisitor: visitorService + 'visitors/new',
  getVisitors: visitorService + 'visitors/view',
  getOpenVisitations: visitorService + 'visitors/view/open',
  getClosedVisitations: visitorService + 'visitors/view/closed',
  closeVisitation: visitorService + 'visitors/close',
  getVisitor: visitorService + 'visitors/view/',
  deleteVisitor: visitorService + 'visitors/delete/',
  editVisitor: visitorService + 'visitors/edit',
  getAllVisitors: visitorService + 'visitors/view',
  restoreVisitor: visitorService + 'starts/restore/',
  getDeletedVisitor: visitorService + 'starts/deleted'
}

export const visitorTypeUrls = {
  postVisitorType: visitorService + 'visitor/types/new',
  getVisitorTypes: visitorService + 'visitor/types/view',
  getVisitorType: visitorService + 'visitor/types/view/',
  deleteVisitorType: visitorService + 'visitor/types/delete',
  editVisitorType: visitorService + 'visitor/types/edit',
  getAllVisitorTypes: visitorService + 'visitor/types/view',
  restoreVisitorType: visitorService + 'starts/restore/',
  getDeletedVisitorType: visitorService + 'starts/deleted'
}
