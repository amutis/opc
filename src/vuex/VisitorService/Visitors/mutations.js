export default {
  GET_VISITORS (state, data) {
    state.visitors = data.data
  },
  GET_ALL_VISITORS (state, data) {
    state.all_visitors = data.data
  },
  GET_VISITOR (state, data) {
    state.visitor = data.data[0]
  },
  GET_DELETED_VISITORS (state, data) {
    state.deleted_visitors = data.data
  },
  GET_OPEN_VISITORS (state, data) {
    state.open_visitors = data.data
  }
}
