import mutations from './mutations'
import actions from './actions'

const state = {
  all_visitors: [],
  visitors: [],
  visitor: [],
  deleted_visitors: [],
  open_visitors: []
}

export default {
  state, mutations, actions
}
