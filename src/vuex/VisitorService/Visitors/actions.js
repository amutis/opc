import Vue from 'vue'
import {visitorUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_visitors (context) {
    Vue.http.get(visitorUrls.getAllVisitors).then(function (response) {
      context.commit('GET_ALL_VISITORS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_open_visitors (context) {
    Vue.http.get(visitorUrls.getOpenVisitations).then(function (response) {
      context.commit('GET_OPEN_VISITORS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_visitors (context, publicId) {
    Vue.http.get(visitorUrls.getVisitors + publicId).then(function (response) {
      context.commit('GET_VISITORS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_visitors (context) {
    Vue.http.get(visitorUrls.getDeletedVisitor).then(function (response) {
      context.commit('GET_DELETED_VISITORS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_visitor (context, publicId) {
    Vue.http.get(visitorUrls.getVisitor + publicId).then(function (response) {
      context.commit('GET_VISITOR', response.data)
      context.dispatch('loading_false')
    })
  },
  close_visitation (context, data) {
    Vue.http.patch(visitorUrls.closeVisitation, data).then(function () {
      context.dispatch('get_all_visitors')
      router.push({
        name: 'Visitors'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_visitor (context, data) {
    Vue.http.post(visitorUrls.postVisitor, data).then(function (response) {
      window.open(response.data.url, '_blank').focus()
      context.dispatch('get_all_visitors')
      router.push({
        name: 'Visitors'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_visitor (context, data) {
    Vue.http.patch(visitorUrls.editVisitor, data).then(function () {
      context.dispatch('get_all_visitors')
      router.push({
        name: 'Visitors'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_visitor (context, publicId) {
    Vue.http.get(visitorUrls.deleteVisitor + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_VISITORS', response.data)
      router.push({
        name: 'Module.DeletedVisitors'
      })
      context.dispatch('loading_false')
    })
  },
  restoreVisitor (context, publicId) {
    Vue.http.get(visitorUrls.restoreVisitor + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VISITORS', response.data)
      router.push({
        name: 'Module.Visitors'
      })
      context.dispatch('loading_false')
    })
  }
}
