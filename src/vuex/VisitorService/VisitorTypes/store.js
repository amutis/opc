import mutations from './mutations'
import actions from './actions'

const state = {
  all_visitor_types: [],
  visitor_types: [],
  visitor_type: [],
  deleted_visitor_types: []
}

export default {
  state, mutations, actions
}
