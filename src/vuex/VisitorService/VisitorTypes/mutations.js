export default {
  GET_VISITOR_TYPES (state, data) {
    state.visitor_types = data.data
  },
  GET_ALL_VISITOR_TYPES (state, data) {
    state.all_visitor_types = data.data
  },
  GET_VISITOR_TYPE (state, data) {
    state.visitor_type = data.data
  },
  GET_DELETED_VISITOR_TYPES (state, data) {
    state.deleted_visitor_types = data.data
  }
}
