import Vue from 'vue'
import {visitorTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_visitor_types (context) {
    Vue.http.get(visitorTypeUrls.getAllVisitorTypes).then(function (response) {
      context.commit('GET_ALL_VISITOR_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_visitor_types (context, publicId) {
    Vue.http.get(visitorTypeUrls.getVisitorTypes + publicId).then(function (response) {
      context.commit('GET_VISITOR_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_visitor_types (context) {
    Vue.http.get(visitorTypeUrls.getDeletedVisitorType).then(function (response) {
      context.commit('GET_DELETED_VISITOR_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_visitor_type (context, publicId) {
    Vue.http.get(visitorTypeUrls.getVisitorType + publicId).then(function (response) {
      context.commit('GET_VISITOR_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_visitor_type (context, data) {
    Vue.http.post(visitorTypeUrls.postVisitorType, data).then(function () {
      context.dispatch('get_all_visitor_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_visitor_type (context, data) {
    Vue.http.patch(visitorTypeUrls.editVisitorType, data).then(function () {
      context.dispatch('get_all_visitor_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_visitor_type (context, data) {
    Vue.http.patch(visitorTypeUrls.deleteVisitorType, data).then(function (response) {
      context.dispatch('get_all_visitor_types')
      context.dispatch('loading_false')
    })
  },
  restoreVisitorType (context, publicId) {
    Vue.http.get(visitorTypeUrls.restoreVisitorType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_VISITOR_TYPES', response.data)
      router.push({
        name: 'Module.VisitorTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
