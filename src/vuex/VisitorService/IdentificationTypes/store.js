import mutations from './mutations'
import actions from './actions'

const state = {
  all_identification_types: [],
  identification_types: [],
  identification_type: [],
  deleted_identification_types: []
}

export default {
  state, mutations, actions
}
