export default {
  GET_IDENTIFICATION_TYPES (state, data) {
    state.identification_types = data.data
  },
  GET_ALL_IDENTIFICATION_TYPES (state, data) {
    state.all_identification_types = data.data
  },
  GET_IDENTIFICATION_TYPE (state, data) {
    state.identification_type = data.data
  },
  GET_DELETED_IDENTIFICATION_TYPES (state, data) {
    state.deleted_identification_types = data.data
  }
}
