import Vue from 'vue'
import {identificationTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_identification_types (context) {
    Vue.http.get(identificationTypeUrls.getAllIdentificationTypes).then(function (response) {
      context.commit('GET_ALL_IDENTIFICATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_identification_types (context, publicId) {
    Vue.http.get(identificationTypeUrls.getIdentificationTypes + publicId).then(function (response) {
      context.commit('GET_IDENTIFICATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_identification_types (context) {
    Vue.http.get(identificationTypeUrls.getDeletedIdentificationType).then(function (response) {
      context.commit('GET_DELETED_IDENTIFICATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_identification_type (context, publicId) {
    Vue.http.get(identificationTypeUrls.getIdentificationType + publicId).then(function (response) {
      context.commit('GET_IDENTIFICATION_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_identification_type (context, data) {
    Vue.http.post(identificationTypeUrls.postIdentificationType, data).then(function () {
      context.dispatch('get_all_identification_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_identification_type (context, data) {
    Vue.http.patch(identificationTypeUrls.editIdentificationType, data).then(function () {
      context.dispatch('get_all_identification_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_identification_type (context, data) {
    Vue.http.patch(identificationTypeUrls.deleteIdentificationType, data).then(function () {
      context.dispatch('get_all_identification_types')
      context.dispatch('loading_false')
    })
  },
  restoreIdentificationType (context, publicId) {
    Vue.http.get(identificationTypeUrls.restoreIdentificationType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_IDENTIFICATION_TYPES', response.data)
      router.push({
        name: 'Module.IdentificationTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
