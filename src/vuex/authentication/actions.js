import Vue from 'vue'
import router from '../../router/index'
import {authUrls} from '../defaults/.url'

export default {
  do_login (context, loginData) {
    const postData = {
      email: loginData.email,
      password: loginData.password
    }
    Vue.http.post(authUrls.login, postData).then(function (response) {
      localStorage.setItem('access_token', response.data.auth_token)
      localStorage.setItem('unem', JSON.stringify(response.data))
      context.dispatch('loading_false')
      context.dispatch('get_credentials')
    }).catch(function (error) {
      console.log(error)
      // context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // var notification = {
      //   title: 'Login Success',
      //   text: 'Welcome'
      // }
      // context.commit('UpdateNotifications', notification)
      // console.log(error.data)
    })
  },
  request_password_reset (context, data) {
    Vue.http.post(authUrls.request_reset, data).then(function (response) {
      localStorage.setItem('access_token', response.data.auth_token)
      context.dispatch('loading_false')
      router.push({
        name: 'Login'
      })
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
    })
  },
  reset_password (context, data) {
    Vue.http.post(authUrls.reset_password + data.reset_id, data).then(function (response) {
      localStorage.setItem('access_token', response.data.auth_token)
      context.dispatch('loading_false')
      window.location.replace('/login')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
    })
  },
  get_credentials (context) {
    Vue.http.get(authUrls.user_details).then(function (response) {
      if (response.status !== 200) {
        alert('Wrong Credentials')
      } else {
        context.commit('GET_STATE', response.data)
        context.dispatch('find_view')
      }
    })
  },
  find_view (context) {
    window.location.replace('/dashboard')
  },
  reset_storage (context) {
    localStorage.clear()
    router.push({
      name: 'Login'
    })
  }
}
