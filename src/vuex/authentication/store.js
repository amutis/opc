import mutations from './mutations'
import actions from './actions'

const state = {
  session: {},
  login_url: 'http://127.0.0.1:8000/oauth/token',
  menu: JSON.parse(localStorage.getItem('unem'))
}

export default {
  state, mutations, actions
}
