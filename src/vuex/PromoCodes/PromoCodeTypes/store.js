import mutations from './mutations'
import actions from './actions'

const state = {
  all_promo_code_types: [],
  promo_code_types: [],
  promo_code_type: [],
  deleted_promo_code_types: []
}

export default {
  state, mutations, actions
}
