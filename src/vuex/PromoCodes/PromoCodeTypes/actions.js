import Vue from 'vue'
import {promoCodeTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_promo_code_types (context) {
    Vue.http.get(promoCodeTypeUrls.getAllPromoCodeTypes).then(function (response) {
      context.commit('GET_ALL_PROMO_CODE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_promo_code_types (context, publicId) {
    Vue.http.get(promoCodeTypeUrls.getPromoCodeTypes + publicId).then(function (response) {
      context.commit('GET_PROMO_CODE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_promo_code_types (context) {
    Vue.http.get(promoCodeTypeUrls.getDeletedPromoCodeType).then(function (response) {
      context.commit('GET_DELETED_PROMO_CODE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_promo_code_type (context, publicId) {
    Vue.http.get(promoCodeTypeUrls.getPromoCodeType + publicId).then(function (response) {
      context.commit('GET_PROMO_CODE_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_promo_code_type (context, data) {
    Vue.http.post(promoCodeTypeUrls.postPromoCodeType, data).then(function () {
      context.dispatch('get_all_promo_code_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_promo_code_type (context, data) {
    Vue.http.post(promoCodeTypeUrls.editPromoCodeType, data).then(function () {
      context.dispatch('get_all_promo_code_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_promo_code_type (context, publicId) {
    Vue.http.get(promoCodeTypeUrls.deletePromoCodeType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PROMO_CODE_TYPES', response.data)
      router.push({
        name: 'Module.DeletedPromoCodeTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restorePromoCodeType (context, publicId) {
    Vue.http.get(promoCodeTypeUrls.restorePromoCodeType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PROMO_CODE_TYPES', response.data)
      router.push({
        name: 'Module.PromoCodeTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
