export default {
  GET_PROMO_CODE_TYPES (state, data) {
    state.promo_code_types = data.data
  },
  GET_ALL_PROMO_CODE_TYPES (state, data) {
    state.all_promo_code_types = data.data
  },
  GET_PROMO_CODE_TYPE (state, data) {
    state.promo_code_type = data.data
  },
  GET_DELETED_PROMO_CODE_TYPES (state, data) {
    state.deleted_promo_code_types = data.data
  }
}
