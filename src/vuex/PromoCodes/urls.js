import {urls} from '../urls'
export const promoCodeService = urls.developmentUrl + ':5017/'

export const promoCodeUrls = {
  postPromoCode: promoCodeService + 'promo/code/type/save',
  getPromoCodes: promoCodeService + 'promo/code/active/valid',
  getActivePromoCodes: promoCodeService + 'promo/code/active/valid',
  makePromoCodeActive: promoCodeService + 'promo/code/alter/status/active/',
  makePromoCodeInActive: promoCodeService + 'promo/code/alter/status/inactive/',
  getInActivePromoCodes: promoCodeService + 'promo/code/inactive',
  getPromoCode: promoCodeService + 'promo/code/',
  deletePromoCode: promoCodeService + 'promo/code/delete/',
  editPromoCode: promoCodeService + 'promo/code/alter/',
  getAllPromoCodes: promoCodeService + 'promo/code/active/valid',
  restorePromoCode: promoCodeService + 'starts/restore/',
  getDeletedPromoCode: promoCodeService + 'starts/deleted'
}

export const promoCodeTypeUrls = {
  postPromoCodeType: promoCodeService + 'promo/code/type/save',
  getPromoCodeTypes: promoCodeService + 'promo/code/types',
  getPromoCodeType: promoCodeService + 'promo/code/type/',
  deletePromoCodeType: promoCodeService + 'promo/code/type/delete/',
  editPromoCodeType: promoCodeService + 'promo/code/type/alter/',
  getAllPromoCodeTypes: promoCodeService + 'promo/code/types',
  restorePromoCodeType: promoCodeService + 'starts/restore/',
  getDeletedPromoCodeType: promoCodeService + 'starts/deleted'
}
