import Vue from 'vue'
import {promoCodeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_promo_codes (context) {
    Vue.http.get(promoCodeUrls.getAllPromoCodes).then(function (response) {
      context.commit('GET_ALL_PROMO_CODES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_promo_codes (context, publicId) {
    Vue.http.get(promoCodeUrls.getPromoCodes + publicId).then(function (response) {
      context.commit('GET_PROMO_CODES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_inactive_promo_codes (context) {
    Vue.http.get(promoCodeUrls.getInActivePromoCodes).then(function (response) {
      context.commit('GET_INACTIVE_PROMO_CODES', response.data)
      context.dispatch('loading_false')
    })
  },
  make_promo_code_active (context, publicId) {
    Vue.http.get(promoCodeUrls.makePromoCodeActive + publicId).then(function () {
      context.dispatch('get_inactive_promo_codes')
      context.dispatch('get_all_promo_codes')
      context.dispatch('loading_false')
    })
  },
  make_promo_code_in_active (context, publicId) {
    Vue.http.get(promoCodeUrls.makePromoCodeInActive + publicId).then(function () {
      context.dispatch('get_inactive_promo_codes')
      context.dispatch('get_all_promo_codes')
      context.dispatch('loading_false')
    })
  },
  get_deleted_promo_codes (context) {
    Vue.http.get(promoCodeUrls.getDeletedPromoCode).then(function (response) {
      context.commit('GET_DELETED_PROMO_CODES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_promo_code (context, publicId) {
    Vue.http.get(promoCodeUrls.getPromoCode + publicId).then(function (response) {
      context.commit('GET_PROMO_CODE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_promo_code (context, data) {
    Vue.http.post(promoCodeUrls.postPromoCode, data).then(function () {
      context.dispatch('get_all_promo_codes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_promo_code (context, data) {
    Vue.http.post(promoCodeUrls.editPromoCode, data).then(function () {
      context.dispatch('get_all_promo_codes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_promo_code (context, publicId) {
    Vue.http.get(promoCodeUrls.deletePromoCode + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PROMO_CODES', response.data)
      router.push({
        name: 'Module.DeletedPromoCodes'
      })
      context.dispatch('loading_false')
    })
  },
  restorePromoCode (context, publicId) {
    Vue.http.get(promoCodeUrls.restorePromoCode + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PROMO_CODES', response.data)
      router.push({
        name: 'Module.PromoCodes'
      })
      context.dispatch('loading_false')
    })
  }
}
