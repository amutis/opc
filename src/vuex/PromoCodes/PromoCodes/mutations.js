export default {
  GET_PROMO_CODES (state, data) {
    state.promo_codes = data.data
  },
  GET_ALL_PROMO_CODES (state, data) {
    state.all_promo_codes = data.data
  },
  GET_PROMO_CODE (state, data) {
    state.promo_code = data.data
  },
  GET_DELETED_PROMO_CODES (state, data) {
    state.deleted_promo_codes = data.data
  },
  GET_INACTIVE_PROMO_CODES (state, data) {
    state.inactive_promo_codes = data.data
  }
}
