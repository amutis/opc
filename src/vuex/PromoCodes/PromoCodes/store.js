import mutations from './mutations'
import actions from './actions'

const state = {
  all_promo_codes: [],
  promo_codes: [],
  promo_code: [],
  deleted_promo_codes: [],
  inactive_promo_codes: []
}

export default {
  state, mutations, actions
}
