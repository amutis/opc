import Vue from 'vue'
import Vuex from 'vuex'
import AuthenticationStore from './authentication/store'
import LoadingStore from './loading/store'

// Activity Service
import ActivityContactStore from './ActivityService/ActivityContacts/store'
import ActivityCurrencyStore from './ActivityService/ActivityCurrencies/store'
import ActivityDiscountStore from './ActivityService/ActivityDiscounts/store'
import ActivityDiscountTypeStore from './ActivityService/ActivityDiscountTypes/store'
import ActivityPricingStore from './ActivityService/ActivityPricing/store'
import ActivityTypeStore from './ActivityService/ActivityTypes/store'
import ActivityStore from './ActivityService/Activities/store'

// Booking Service
import BookingStore from './BookingService/Bookings/store'
import BookingSearchStore from './BookingService/BookingSearch/store'
import BookingTypeStore from './BookingService/BookingTypes/store'
import CalendarStore from './BookingService/Calendar/store'
import ConservationFeeStore from './BookingService/ConservationFee/store'
import DestinationStore from './BookingService/Destinations/store'
import GatePassStore from './BookingService/GatePasses/store'
import GroupBookingTypeStore from './BookingService/GroupBookingTypes/store'
import PaymentMethodStore from './BookingService/PaymentMethods/store'
import VehicleFeeStore from './BookingService/VehicleFees/store'
import GateStore from './BookingService/Gates/store'
import PickUpLocationStore from './BookingService/PickUpLocations/store'
import FacilityPricingTypeStore from './BookingService/FacilityPricingType/store'

// Currency Service
import CateringLevyStore from './CurrencyService/CateringLevy/store'
import CurrencyStore from './CurrencyService/Currency/store'
import VatStore from './CurrencyService/Vat/store'

// Facility Service
import AccommodationTypeStore from './FacilityService/AccommodationTypes/store'
import FacilityStore from './FacilityService/Facilities/store'
import FacilityContactStore from './FacilityService/FacilityContacts/store'
import FacilityCurrencyStore from './FacilityService/FacilityCurrencies/store'
import FacilityDiscountStore from './FacilityService/FacilityDiscounts/store'
import FacilityDiscountTypeStore from './FacilityService/FacilityDiscountTypes/store'
import FacilityMaintananceStore from './FacilityService/FacilityMaintanance/store'
import FacilityPricingStore from './FacilityService/FacilityPricing/store'
import FacilityTypeStore from './FacilityService/FacilityTypes/store'

// Volunteer Service
import ProgramStore from './VolunteerService/Programs/store'
import VolunteerStore from './VolunteerService/Volunteers/store'
import VolunteerContactStore from './VolunteerService/VolunteerContacts/store'

// Partner Service
import PartnerStore from './PartnerService/Partners/store'
import PartnerTypeStore from './PartnerService/PartnerTypes/store'

// Member Service
import HouseHoldStore from './MemberService/HouseHolds/store'
import MemberStore from './MemberService/Members/store'
import MemberTypeStore from './MemberService/MemberTypes/store'
import ResidenceTypeStore from './MemberService/ResidenceTypes/store'
import MembershipSettingStore from './MemberService/MembershipSettings/store'

// Amura Service
import UserTypeStore from './AmuraService/UserTypes/store'
import UserStore from './AmuraService/Users/store'
import MenuStore from './AmuraService/Menus/store'
import FeatureStore from './AmuraService/Features/store'
import CountryStore from './AmuraService/Countries/store'
import GenderStore from './AmuraService/Genders/store'

// Settings
import CallToActionStore from './SettingsService/CallToAction/store'
import HolidayStore from './SettingsService/Holidays/store'
import PageStore from './SettingsService/Pages/store'
import PageTypeStore from './SettingsService/PageTypes/store'

// Staff Service
import DepartmentStore from './StaffService/Departments/store'
import StaffStore from './StaffService/Staffs/store'

// Visitor Service
import IdentificationTypeStore from './VisitorService/IdentificationTypes/store'
import VisitorStore from './VisitorService/Visitors/store'
import VisitorTypeStore from './VisitorService/VisitorTypes/store'

// Donation Service
import AnonymityStore from './DonationService/Anonymity/store'
import CauseStore from './DonationService/Causes/store'
import DonationStore from './DonationService/Donations/store'
import PaymentFrequencyStore from './DonationService/PaymentFrequency/store'
import DonationPaymentMethodStore from './DonationService/PaymentMethods/store'

// Mpesa
import MpesaStore from './MpesaService/store'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

// const debug = process.env.NODE_ENV !== 'production'
const debug = false

export default new Vuex.Store({
  modules: {
    AuthenticationStore,
    LoadingStore,
    // Donation Service
    AnonymityStore,
    CauseStore,
    DonationStore,
    PaymentFrequencyStore,
    DonationPaymentMethodStore,
    // Visitor Service
    IdentificationTypeStore,
    VisitorStore,
    VisitorTypeStore,
    // Staff Service
    DepartmentStore,
    StaffStore,
    // Settings Sevice
    CallToActionStore,
    HolidayStore,
    PageStore,
    PageTypeStore,
    // Amura Service
    UserTypeStore,
    UserStore,
    MenuStore,
    FeatureStore,
    GenderStore,
    // Activity Service
    ActivityContactStore,
    ActivityCurrencyStore,
    ActivityDiscountStore,
    ActivityDiscountTypeStore,
    ActivityPricingStore,
    ActivityTypeStore,
    // Booking Service
    BookingStore,
    BookingSearchStore,
    BookingTypeStore,
    CalendarStore,
    ConservationFeeStore,
    DestinationStore,
    GatePassStore,
    GroupBookingTypeStore,
    PaymentMethodStore,
    VehicleFeeStore,
    GateStore,
    PickUpLocationStore,
    FacilityPricingTypeStore,
    // Currency Service
    CateringLevyStore,
    CurrencyStore,
    VatStore,
    // Facility Service
    AccommodationTypeStore,
    FacilityStore,
    FacilityContactStore,
    FacilityCurrencyStore,
    FacilityDiscountStore,
    FacilityDiscountTypeStore,
    FacilityMaintananceStore,
    FacilityPricingStore,
    FacilityTypeStore,
    ActivityStore,
    // Volunteer Service
    CountryStore,
    ProgramStore,
    VolunteerStore,
    VolunteerContactStore,
    // Partner Service
    PartnerStore,
    PartnerTypeStore,
    // Member Service
    MembershipSettingStore,
    HouseHoldStore,
    MemberStore,
    MemberTypeStore,
    ResidenceTypeStore,
    // Mpesa
    MpesaStore
  },
  strict: debug
})
