import mutations from './mutations'
import actions from './actions'

const state = {
  all_call_to_actions: [],
  call_to_actions: [],
  call_to_action: [],
  deleted_call_to_actions: []
}

export default {
  state, mutations, actions
}
