import Vue from 'vue'
import {callToActionUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_call_to_actions (context) {
    Vue.http.get(callToActionUrls.getAllCallToActions).then(function (response) {
      context.commit('GET_ALL_CALL_TO_ACTIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_call_to_actions (context, publicId) {
    Vue.http.get(callToActionUrls.getCallToActions + publicId).then(function (response) {
      context.commit('GET_CALL_TO_ACTIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_call_to_actions (context) {
    Vue.http.get(callToActionUrls.getDeletedCallToAction).then(function (response) {
      context.commit('GET_DELETED_CALL_TO_ACTIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_call_to_action (context, publicId) {
    Vue.http.get(callToActionUrls.getCallToAction + publicId).then(function (response) {
      context.commit('GET_CALL_TO_ACTION', response.data)
      context.dispatch('loading_false')
    })
  },
  post_call_to_action (context, data) {
    Vue.http.post(callToActionUrls.postCallToAction, data).then(function () {
      context.dispatch('get_all_call_to_actions')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_call_to_action (context, data) {
    Vue.http.post(callToActionUrls.editCallToAction, data).then(function () {
      context.dispatch('get_all_call_to_actions')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_call_to_action (context, publicId) {
    var postData = {
      deleted_by: localStorage.getItem('lkjhgfdsa')
    }
    Vue.http.post(callToActionUrls.deleteCallToAction + publicId, postData).then(function (response) {
      context.dispatch('get_all_call_to_actions')
      context.dispatch('loading_false')
    })
  },
  restoreCallToAction (context, publicId) {
    Vue.http.get(callToActionUrls.restoreCallToAction + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CALL_TO_ACTIONS', response.data)
      router.push({
        name: 'Module.CallToActions'
      })
      context.dispatch('loading_false')
    })
  }
}
