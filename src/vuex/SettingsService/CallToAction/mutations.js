export default {
  GET_CALL_TO_ACTIONS (state, data) {
    state.call_to_actions = data.data
  },
  GET_ALL_CALL_TO_ACTIONS (state, data) {
    state.all_call_to_actions = data.data
  },
  GET_CALL_TO_ACTION (state, data) {
    state.call_to_action = data.data
  },
  GET_DELETED_CALL_TO_ACTIONS (state, data) {
    state.deleted_call_to_actions = data.data
  }
}
