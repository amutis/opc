import {urls} from '../urls'
export const settingService = urls.developmentUrl + ':5018/'

export const pageUrls = {
  postPage: settingService + 'page/save',
  getPages: settingService + 'pages',
  getPage: settingService + 'pages/categorised/',
  deletePage: settingService + 'page/delete/',
  editPage: settingService + 'page/alter/',
  getAllPages: settingService + 'pages',
  restorePage: settingService + 'starts/restore/',
  getDeletedPage: settingService + 'starts/deleted'
}

export const holidayUrls = {
  postHoliday: settingService + 'holiday/save',
  getHolidays: settingService + 'holiday',
  getHoliday: settingService + 'holiday/',
  deleteHoliday: settingService + 'holiday/delete/',
  editHoliday: settingService + 'holiday/alter/',
  getAllHolidays: settingService + 'holiday',
  restoreHoliday: settingService + 'starts/restore/',
  getDeletedHoliday: settingService + 'starts/deleted'
}

export const callToActionUrls = {
  postCallToAction: settingService + 'call/to/action/save',
  getCallToActions: settingService + 'call/to/action',
  getCallToAction: settingService + 'call/to/action/',
  deleteCallToAction: settingService + 'call/to/action/delete/',
  editCallToAction: settingService + 'call/to/action/alter/',
  getAllCallToActions: settingService + 'call/to/action',
  restoreCallToAction: settingService + 'starts/restore/',
  getDeletedCallToAction: settingService + 'starts/deleted'
}

export const pageTypeUrls = {
  postPageType: settingService + 'page/type/save',
  getPageTypes: settingService + 'page/types',
  getPageType: settingService + 'page/type/',
  deletePageType: settingService + 'page/type/delete/',
  editPageType: settingService + 'page/type/alter/',
  getAllPageTypes: settingService + 'page/types',
  restorePageType: settingService + 'starts/restore/',
  getDeletedPageType: settingService + 'starts/deleted'
}
