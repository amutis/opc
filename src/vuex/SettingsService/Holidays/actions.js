import Vue from 'vue'
import {holidayUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_holidays (context) {
    Vue.http.get(holidayUrls.getAllHolidays).then(function (response) {
      context.commit('GET_ALL_HOLIDAYS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_holidays (context, publicId) {
    Vue.http.get(holidayUrls.getHolidays + publicId).then(function (response) {
      context.commit('GET_HOLIDAYS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_holidays (context) {
    Vue.http.get(holidayUrls.getDeletedHoliday).then(function (response) {
      context.commit('GET_DELETED_HOLIDAYS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_holiday (context, publicId) {
    Vue.http.get(holidayUrls.getHoliday + publicId).then(function (response) {
      context.commit('GET_HOLIDAY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_holiday (context, data) {
    Vue.http.post(holidayUrls.postHoliday, data).then(function () {
      context.dispatch('get_all_holidays')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_holiday (context, data) {
    Vue.http.post(holidayUrls.editHoliday, data).then(function () {
      context.dispatch('get_all_holidays')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_holiday (context, publicId) {
    Vue.http.get(holidayUrls.deleteHoliday + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_HOLIDAYS', response.data)
      router.push({
        name: 'Module.DeletedHolidays'
      })
      context.dispatch('loading_false')
    })
  },
  restoreHoliday (context, publicId) {
    Vue.http.get(holidayUrls.restoreHoliday + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_HOLIDAYS', response.data)
      router.push({
        name: 'Module.Holidays'
      })
      context.dispatch('loading_false')
    })
  }
}
