export default {
  GET_HOLIDAYS (state, data) {
    state.holidays = data.data
  },
  GET_ALL_HOLIDAYS (state, data) {
    state.all_holidays = data.data
  },
  GET_HOLIDAY (state, data) {
    state.holiday = data.data
  },
  GET_DELETED_HOLIDAYS (state, data) {
    state.deleted_holidays = data.data
  }
}
