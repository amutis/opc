import mutations from './mutations'
import actions from './actions'

const state = {
  all_holidays: [],
  holidays: [],
  holiday: [],
  deleted_holidays: []
}

export default {
  state, mutations, actions
}
