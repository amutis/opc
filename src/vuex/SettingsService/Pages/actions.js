import Vue from 'vue'
import {pageUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_pages (context) {
    Vue.http.get(pageUrls.getAllPages).then(function (response) {
      context.commit('GET_ALL_PAGES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_pages (context, publicId) {
    Vue.http.get(pageUrls.getPages + publicId).then(function (response) {
      context.commit('GET_PAGES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_pages (context) {
    Vue.http.get(pageUrls.getDeletedPage).then(function (response) {
      context.commit('GET_DELETED_PAGES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_page (context, publicId) {
    Vue.http.get(pageUrls.getPage + publicId).then(function (response) {
      context.commit('GET_PAGE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_page (context, data) {
    Vue.http.post(pageUrls.postPage, data).then(function () {
      context.dispatch('get_all_pages')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_page (context, data) {
    Vue.http.post(pageUrls.editPage, data).then(function () {
      context.dispatch('get_all_pages')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_page (context, publicId) {
    Vue.http.get(pageUrls.deletePage + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PAGES', response.data)
      router.push({
        name: 'Module.DeletedPages'
      })
      context.dispatch('loading_false')
    })
  },
  restorePage (context, publicId) {
    Vue.http.get(pageUrls.restorePage + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PAGES', response.data)
      router.push({
        name: 'Module.Pages'
      })
      context.dispatch('loading_false')
    })
  }
}
