import mutations from './mutations'
import actions from './actions'

const state = {
  all_pages: [],
  pages: [],
  page: [],
  deleted_pages: []
}

export default {
  state, mutations, actions
}
