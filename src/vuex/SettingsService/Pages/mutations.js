export default {
  GET_PAGES (state, data) {
    state.pages = data.data
  },
  GET_ALL_PAGES (state, data) {
    state.all_pages = data.data
  },
  GET_PAGE (state, data) {
    state.page = data.data[0]
  },
  GET_DELETED_PAGES (state, data) {
    state.deleted_pages = data.data
  }
}
