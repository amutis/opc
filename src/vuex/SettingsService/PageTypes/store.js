import mutations from './mutations'
import actions from './actions'

const state = {
  all_page_types: [],
  page_types: [],
  page_type: [],
  deleted_page_types: []
}

export default {
  state, mutations, actions
}
