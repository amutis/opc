export default {
  GET_PAGE_TYPES (state, data) {
    state.page_types = data.data
  },
  GET_ALL_PAGE_TYPES (state, data) {
    state.all_page_types = data.data
  },
  GET_PAGE_TYPE (state, data) {
    state.page_type = data.data
  },
  GET_DELETED_PAGE_TYPES (state, data) {
    state.deleted_page_types = data.data
  }
}
