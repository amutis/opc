import Vue from 'vue'
import {pageTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_page_types (context) {
    Vue.http.get(pageTypeUrls.getAllPageTypes).then(function (response) {
      context.commit('GET_ALL_PAGE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_page_types (context, publicId) {
    Vue.http.get(pageTypeUrls.getPageTypes + publicId).then(function (response) {
      context.commit('GET_PAGE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_page_types (context) {
    Vue.http.get(pageTypeUrls.getDeletedPageType).then(function (response) {
      context.commit('GET_DELETED_PAGE_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_page_type (context, publicId) {
    Vue.http.get(pageTypeUrls.getPageType + publicId).then(function (response) {
      context.commit('GET_PAGE_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_page_type (context, data) {
    Vue.http.post(pageTypeUrls.postPageType, data).then(function () {
      context.dispatch('get_all_page_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_page_type (context, data) {
    Vue.http.post(pageTypeUrls.editPageType, data).then(function () {
      context.dispatch('get_all_page_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_page_type (context, publicId) {
    Vue.http.get(pageTypeUrls.deletePageType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PAGE_TYPES', response.data)
      router.push({
        name: 'Module.DeletedPageTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restorePageType (context, publicId) {
    Vue.http.get(pageTypeUrls.restorePageType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PAGE_TYPES', response.data)
      router.push({
        name: 'Module.PageTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
