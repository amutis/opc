import mutations from './mutations'
import actions from './actions'

const state = {
  all_countries: [],
  countries: [],
  country: [],
  all_support_countries: [],
  deleted_countries: []
}

export default {
  state, mutations, actions
}
