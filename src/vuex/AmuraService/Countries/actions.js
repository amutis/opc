import Vue from 'vue'
import {countryUrls} from '../../AmuraService/urls'
import router from '../../../router/index'

export default {
  get_all_countries (context) {
    Vue.http.get(countryUrls.getAllCountries).then(function (response) {
      context.commit('GET_ALL_COUNTRIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_countries (context, publicId) {
    Vue.http.get(countryUrls.getCountries + publicId).then(function (response) {
      context.commit('GET_COUNTRIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_countries (context) {
    Vue.http.get(countryUrls.getDeletedCountry).then(function (response) {
      context.commit('GET_DELETED_COUNTRIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_country (context, publicId) {
    Vue.http.get(countryUrls.getCountry + publicId).then(function (response) {
      context.commit('GET_COUNTRY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_country (context, data) {
    Vue.http.post(countryUrls.postCountry, data).then(function () {
      context.dispatch('get_all_countries')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_country (context, data) {
    Vue.http.post(countryUrls.editCountry, data).then(function () {
      context.dispatch('get_all_countries')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_country (context, publicId) {
    Vue.http.get(countryUrls.deleteCountry + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_COUNTRIES', response.data)
      router.push({
        name: 'Module.DeletedCountries'
      })
      context.dispatch('loading_false')
    })
  },
  restoreCountry (context, publicId) {
    Vue.http.get(countryUrls.restoreCountry + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_COUNTRIES', response.data)
      router.push({
        name: 'Module.Countries'
      })
      context.dispatch('loading_false')
    })
  }
}
