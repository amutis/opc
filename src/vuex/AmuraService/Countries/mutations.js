export default {
  GET_COUNTRIES (state, data) {
    state.countries = data.data
  },
  GET_ALL_COUNTRIES (state, data) {
    state.all_countries = data.data
  },
  GET_COUNTRY (state, data) {
    state.country = data.data
  },
  GET_DELETED_COUNTRIES (state, data) {
    state.deleted_countries = data.data
  }
}
