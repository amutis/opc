import Vue from 'vue'
import {userTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_user_types (context) {
    Vue.http.get(userTypeUrls.getAllUserTypes).then(function (response) {
      context.commit('GET_ALL_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_types (context, publicId) {
    Vue.http.get(userTypeUrls.getUserTypes + publicId).then(function (response) {
      context.commit('GET_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_type_features (context, publicId) {
    Vue.http.get(userTypeUrls.getUserTypeFeatures + publicId).then(function (response) {
      context.commit('GET_USER_TYPE_FEATURES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_not_assigned_features (context, publicId) {
    Vue.http.get(userTypeUrls.getNotAssignedFeatures + publicId).then(function (response) {
      context.commit('GET_NOT_ASSIGNED_FEATURES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_user_types (context) {
    Vue.http.get(userTypeUrls.getDeletedUserType).then(function (response) {
      context.commit('GET_DELETED_USER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_user_type (context, publicId) {
    Vue.http.get(userTypeUrls.getUserType + publicId).then(function (response) {
      context.commit('GET_USER_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_user_type (context, data) {
    Vue.http.post(userTypeUrls.postUserType, data).then(function () {
      context.dispatch('get_all_user_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_user_type (context, data) {
    Vue.http.post(userTypeUrls.editUserType + data.public_id, data).then(function () {
      context.dispatch('get_all_user_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_user_type (context, publicId) {
    Vue.http.delete(userTypeUrls.deleteUserType + publicId).then(function (response) {
      context.dispatch('get_all_user_types')
      context.dispatch('loading_false')
    })
  },
  restoreUserType (context, publicId) {
    Vue.http.get(userTypeUrls.restoreUserType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USER_TYPES', response.data)
      router.push({
        name: 'Module.UserTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
