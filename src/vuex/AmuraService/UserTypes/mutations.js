export default {
  GET_USER_TYPES (state, data) {
    state.user_types = data.data
  },
  GET_ALL_USER_TYPES (state, data) {
    state.all_user_types = data.data
  },
  GET_USER_TYPE (state, data) {
    state.user_type = data
  },
  GET_DELETED_USER_TYPES (state, data) {
    state.deleted_user_types = data.data
  },
  GET_USER_TYPE_FEATURES (state, data) {
    state.user_type_features = data.data
  },
  GET_NOT_ASSIGNED_FEATURES (state, data) {
    state.not_assigned_features = data.data
  }
}
