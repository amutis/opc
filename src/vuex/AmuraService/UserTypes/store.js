import mutations from './mutations'
import actions from './actions'

const state = {
  all_user_types: [],
  user_types: [],
  user_type: [],
  deleted_user_types: [],
  user_type_features: [],
  not_assigned_features: []
}

export default {
  state, mutations, actions
}
