export default {
  GET_FEATURES (state, data) {
    state.features = data.data
  },
  GET_ALL_FEATURES (state, data) {
    state.all_features = data.data
  },
  GET_FEATURE (state, data) {
    state.feature = data.data
  },
  GET_DELETED_FEATURES (state, data) {
    state.deleted_features = data.data
  }
}
