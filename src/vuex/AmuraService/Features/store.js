import mutations from './mutations'
import actions from './actions'

const state = {
  all_features: [],
  features: [],
  feature: [],
  deleted_features: []
}

export default {
  state, mutations, actions
}
