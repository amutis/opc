import Vue from 'vue'
import {featureUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_features (context) {
    Vue.http.get(featureUrls.getAllFeatures).then(function (response) {
      context.commit('GET_ALL_FEATURES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_features (context, publicId) {
    Vue.http.get(featureUrls.getFeatures + publicId).then(function (response) {
      context.commit('GET_FEATURES', response.data)
      context.dispatch('loading_false')
    })
  },
  assign_feature (context, data) {
    Vue.http.post(featureUrls.assignFeature, data).then(function () {
      context.dispatch('get_user_type_features', data.role_public_id)
      context.dispatch('get_not_assigned_features', data.role_public_id)
      context.dispatch('loading_false')
    })
  },
  detach_feature (context, data) {
    Vue.http.post(featureUrls.removeFeature, data).then(function () {
      context.dispatch('get_user_type_features', data.role_public_id)
      context.dispatch('get_not_assigned_features', data.role_public_id)
      context.dispatch('loading_false')
    })
  },
  get_deleted_features (context) {
    Vue.http.get(featureUrls.getDeletedFeature).then(function (response) {
      context.commit('GET_DELETED_FEATURES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_feature (context, publicId) {
    Vue.http.get(featureUrls.getFeature + publicId).then(function (response) {
      context.commit('GET_FEATURE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_feature (context, data) {
    Vue.http.post(featureUrls.postFeature, data).then(function () {
      context.dispatch('get_all_features')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_feature (context, data) {
    Vue.http.post(featureUrls.editFeature, data).then(function () {
      context.dispatch('get_all_features')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_feature (context, publicId) {
    Vue.http.get(featureUrls.deleteFeature + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_FEATURES', response.data)
      router.push({
        name: 'Module.DeletedFeatures'
      })
      context.dispatch('loading_false')
    })
  },
  restoreFeature (context, publicId) {
    Vue.http.get(featureUrls.restoreFeature + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FEATURES', response.data)
      router.push({
        name: 'Module.Features'
      })
      context.dispatch('loading_false')
    })
  }
}
