import Vue from 'vue'
import {genderUrl} from '../urls'

export default {
  get_all_genders (context) {
    Vue.http.get(genderUrl.getAllGenders).then(function (response) {
      context.commit('GET_ALL_GENDERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_all_genders2 (context) {
    Vue.http.get(genderUrl.getAllGenders2).then(function (response) {
      context.commit('GET_ALL_GENDERS', response.data)
      context.dispatch('loading_false')
    })
  }
}
