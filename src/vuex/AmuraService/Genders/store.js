import mutations from './mutations'
import actions from './actions'

const state = {
  all_genders: []
}

export default {
  state, mutations, actions
}
