import Vue from 'vue'
import {userUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_users (context) {
    Vue.http.get(userUrls.getAllUsers).then(function (response) {
      context.commit('GET_ALL_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_institution_users (context, publicId) {
    Vue.http.get(userUrls.getUsers + publicId).then(function (response) {
      context.commit('GET_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_institution_users2 (context, publicId) {
    Vue.http.get(userUrls.getUsers + publicId).then(function (response) {
      context.commit('GET_ALL_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_users (context) {
    Vue.http.get(userUrls.getDeletedUser).then(function (response) {
      context.commit('GET_DELETED_USERS', response.data)
      context.dispatch('loading_false')
    })
  },
  suspend_user (context, publicId) {
    Vue.http.get(userUrls.suspendUser + publicId).then(function (response) {
      router.push({
        name: 'Users'
      })
      context.dispatch('loading_false')
    })
  },
  unsuspend_user (context, publicId) {
    Vue.http.get(userUrls.unsuspendUser + publicId).then(function (response) {
      router.push({
        name: 'Users'
      })
      context.dispatch('loading_false')
    })
  },
  get_user (context, publicId) {
    Vue.http.get(userUrls.getUser + publicId).then(function (response) {
      context.commit('GET_USER', response.data)
      context.dispatch('loading_false')
    })
  },
  post_partner_user (context, data) {
    Vue.http.post(userUrls.postPartnerUser, data).then(function () {
      context.dispatch('get_all_users')
      router.push({
        name: 'Users'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_user (context, data) {
    Vue.http.post(userUrls.postUser, data).then(function () {
      context.dispatch('get_all_users')
      router.push({
        name: 'Users'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_user (context, data) {
    Vue.http.post(userUrls.editUser + data.public_id, data).then(function () {
      context.dispatch('get_user', data.public_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_user (context, publicId) {
    Vue.http.get(userUrls.deleteUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_USERS', response.data)
      router.push({
        name: 'Module.DeletedUsers'
      })
      context.dispatch('loading_false')
    })
  },
  restoreUser (context, publicId) {
    Vue.http.get(userUrls.restoreUser + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_USERS', response.data)
      router.push({
        name: 'Module.Users'
      })
      context.dispatch('loading_false')
    })
  }
}
