import {aumra} from '../defaults/.url'
import {urls} from '../urls'
export const amuraService = urls.developmentUrl + ':9000/v1/'

export const userUrls = {
  postUser: amuraService + 'signup',
  postPartnerUser: amuraService + 'partner/user',
  getUsers: aumra + 'users/institution/',
  getPartnerUsers: amuraService + 'users/institution/',
  getUser: amuraService + 'user/',
  deleteUser: amuraService + 'user/delete/',
  editUser: amuraService + 'user/update/',
  getAllUsers: amuraService + 'users',
  restoreUser: amuraService + 'starts/restore/',
  getDeletedUser: amuraService + 'starts/deleted',
  suspendUser: amuraService + 'user/suspend/',
  unsuspendUser: amuraService + 'user/unsuspend/'
}

export const userTypeUrls = {
  postUserType: amuraService + 'add/usertype',
  getUserTypes: amuraService + 'usertypes',
  getUserType: amuraService + 'single/usertype/',
  getUserTypeFeatures: amuraService + 'features/role/',
  getNotAssignedFeatures: amuraService + 'feature/notassigned/',
  deleteUserType: amuraService + 'delete/usertype/',
  editUserType: amuraService + 'update/usertype/',
  getAllUserTypes: amuraService + 'usertypes',
  restoreUserType: amuraService + 'starts/restore/',
  getDeletedUserType: amuraService + 'starts/deleted'
}

export const menuUrls = {
  postMenu: amuraService + 'menu/add',
  getMenus: amuraService + 'menus',
  getMenu: amuraService + 'menu/',
  deleteMenu: amuraService + 'starts/delete/',
  editMenu: amuraService + 'menu/update/',
  getAllMenus: amuraService + 'menus',
  restoreMenu: amuraService + 'starts/restore/',
  getDeletedMenu: amuraService + 'starts/deleted'
}

export const featureUrls = {
  assignFeature: amuraService + 'role/feature/add',
  removeFeature: amuraService + 'role/feature/detach',
  postFeature: amuraService + 'feature/add',
  getFeatures: amuraService + 'features',
  getFeature: amuraService + 'starts/single/',
  deleteFeature: amuraService + 'starts/delete/',
  editFeature: amuraService + 'starts/edit/',
  getAllFeatures: amuraService + 'features',
  restoreFeature: amuraService + 'starts/restore/',
  getDeletedFeature: amuraService + 'starts/deleted'
}

export const countryUrls = {
  postCountry: amuraService + 'post',
  getAllCountries: amuraService + 'countries',
  getCountry: amuraService + 'get_single',
  editCountry: amuraService + 'update',
  deleteCountry: amuraService + 'delete'
}

export const genderUrl = {
  getAllGenders: amuraService + 'genders',
  getAllGenders2: amuraService + 'genders/schools'
}
