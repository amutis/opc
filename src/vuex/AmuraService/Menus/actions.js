import Vue from 'vue'
import {menuUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_menus (context) {
    Vue.http.get(menuUrls.getAllMenus).then(function (response) {
      context.commit('GET_ALL_MENUS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_menus (context, publicId) {
    Vue.http.get(menuUrls.getMenus + publicId).then(function (response) {
      context.commit('GET_MENUS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_menus (context) {
    Vue.http.get(menuUrls.getDeletedMenu).then(function (response) {
      context.commit('GET_DELETED_MENUS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_menu (context, publicId) {
    Vue.http.get(menuUrls.getMenu + publicId).then(function (response) {
      context.commit('GET_MENU', response.data)
      context.dispatch('loading_false')
    })
  },
  post_menu (context, data) {
    Vue.http.post(menuUrls.postMenu, data).then(function () {
      context.dispatch('get_all_menus')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_menu (context, data) {
    Vue.http.post(menuUrls.editMenu, data).then(function () {
      context.dispatch('get_all_menus')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_menu (context, publicId) {
    Vue.http.get(menuUrls.deleteMenu + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_MENUS', response.data)
      router.push({
        name: 'Module.DeletedMenus'
      })
      context.dispatch('loading_false')
    })
  },
  restoreMenu (context, publicId) {
    Vue.http.get(menuUrls.restoreMenu + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_MENUS', response.data)
      router.push({
        name: 'Module.Menus'
      })
      context.dispatch('loading_false')
    })
  }
}
