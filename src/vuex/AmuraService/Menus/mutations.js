export default {
  GET_MENUS (state, data) {
    state.menus = data.data
  },
  GET_ALL_MENUS (state, data) {
    state.all_menus = data.data
  },
  GET_MENU (state, data) {
    state.menu = data.data
  },
  GET_DELETED_MENUS (state, data) {
    state.deleted_menus = data.data
  }
}
