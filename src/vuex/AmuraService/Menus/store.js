import mutations from './mutations'
import actions from './actions'

const state = {
  all_menus: [],
  menus: [],
  menu: [],
  deleted_menus: []
}

export default {
  state, mutations, actions
}
