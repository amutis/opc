export default {
  GET_STAFFS (state, data) {
    state.staffs = data.data
  },
  GET_ALL_STAFFS (state, data) {
    state.all_staffs = data.data
  },
  GET_STAFF (state, data) {
    state.staff = data.data[0]
  },
  GET_DELETED_STAFFS (state, data) {
    state.deleted_staffs = data.data
  }
}
