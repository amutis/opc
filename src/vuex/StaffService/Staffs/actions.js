import Vue from 'vue'
import {staffUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_staffs (context) {
    Vue.http.get(staffUrls.getAllStaffs).then(function (response) {
      context.commit('GET_ALL_STAFFS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_staffs (context, publicId) {
    Vue.http.get(staffUrls.getStaffs + publicId).then(function (response) {
      context.commit('GET_STAFFS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_staffs (context) {
    Vue.http.get(staffUrls.getDeletedStaff).then(function (response) {
      context.commit('GET_DELETED_STAFFS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_staff (context, publicId) {
    Vue.http.get(staffUrls.getStaff + publicId).then(function (response) {
      context.commit('GET_STAFF', response.data)
      context.dispatch('loading_false')
    })
  },
  post_staff (context, data) {
    Vue.http.post(staffUrls.postStaff, data).then(function () {
      context.dispatch('get_all_staffs')
      router.push({
        name: 'AllStaff'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_staff (context, data) {
    Vue.http.post(staffUrls.editStaff + data.public_id, data).then(function () {
      context.dispatch('get_all_staffs')
      router.push({
        name: 'AllStaff'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_staff (context, data) {
    Vue.http.post(staffUrls.deleteStaff + data.public_id, data).then(function (response) {
      context.dispatch('get_all_staffs')
    })
  },
  restoreStaff (context, publicId) {
    Vue.http.get(staffUrls.restoreStaff + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_STAFFS', response.data)
      router.push({
        name: 'Module.Staffs'
      })
      context.dispatch('loading_false')
    })
  }
}
