import mutations from './mutations'
import actions from './actions'

const state = {
  all_staffs: [],
  staffs: [],
  staff: [],
  deleted_staffs: []
}

export default {
  state, mutations, actions
}
