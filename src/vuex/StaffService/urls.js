import {urls} from '../urls'
export const staffService = urls.developmentUrl + ':5013/'

export const departmentUrls = {
  postDepartment: staffService + 'save/department',
  getDepartments: staffService + 'departments',
  getDepartment: staffService + 'department/',
  deleteDepartment: staffService + 'department/delete/',
  editDepartment: staffService + 'department/alter/',
  getAllDepartments: staffService + 'departments/active',
  restoreDepartment: staffService + 'departments/restore/',
  getDeletedDepartment: staffService + 'departments/deleted'
}

export const staffUrls = {
  postStaff: staffService + 'staff/save',
  getStaffs: staffService + 'staff',
  getActiveStaffs: staffService + 'staff/active/',
  getInActiveStaffs: staffService + 'staff/inactive/',
  makeStaffActive: staffService + 'staff/alter/status/active/',
  makeStaffInActive: staffService + 'staff/alter/status/inactive/',
  getStaff: staffService + 'staff/',
  deleteStaff: staffService + 'staff/delete/',
  editStaff: staffService + 'staff/alter/',
  getAllStaffs: staffService + 'staff',
  restoreStaff: staffService + 'staffs/restore/',
  getDeletedStaff: staffService + 'staffs/deleted'
}
