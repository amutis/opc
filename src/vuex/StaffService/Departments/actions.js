import Vue from 'vue'
import {departmentUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_departments (context) {
    Vue.http.get(departmentUrls.getAllDepartments).then(function (response) {
      context.commit('GET_ALL_DEPARTMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_departments (context, publicId) {
    Vue.http.get(departmentUrls.getDepartments + publicId).then(function (response) {
      context.commit('GET_DEPARTMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_departments (context) {
    Vue.http.get(departmentUrls.getDeletedDepartment).then(function (response) {
      context.commit('GET_DELETED_DEPARTMENTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_department (context, publicId) {
    Vue.http.get(departmentUrls.getDepartment + publicId).then(function (response) {
      context.commit('GET_DEPARTMENT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_department (context, data) {
    Vue.http.post(departmentUrls.postDepartment, data).then(function () {
      context.dispatch('get_all_departments')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_department (context, data) {
    Vue.http.post(departmentUrls.editDepartment + data.public_id, data).then(function () {
      context.dispatch('get_all_departments')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_department (context, data) {
    Vue.http.post(departmentUrls.deleteDepartment + data.public_id, data).then(function () {
      context.dispatch('get_all_departments')
      context.dispatch('loading_false')
    })
  },
  restoreDepartment (context, publicId) {
    Vue.http.get(departmentUrls.restoreDepartment + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_DEPARTMENTS', response.data)
      router.push({
        name: 'Module.Departments'
      })
      context.dispatch('loading_false')
    })
  }
}
