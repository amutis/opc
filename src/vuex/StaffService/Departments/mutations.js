export default {
  GET_DEPARTMENTS (state, data) {
    state.departments = data.data
  },
  GET_ALL_DEPARTMENTS (state, data) {
    state.all_departments = data.data
  },
  GET_DEPARTMENT (state, data) {
    state.department = data.data
  },
  GET_DELETED_DEPARTMENTS (state, data) {
    state.deleted_departments = data.data
  }
}
