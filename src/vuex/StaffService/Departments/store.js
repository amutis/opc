import mutations from './mutations'
import actions from './actions'

const state = {
  all_departments: [],
  departments: [],
  department: [],
  deleted_departments: []
}

export default {
  state, mutations, actions
}
