export default {
  GET_DONATIONS (state, data) {
    state.donations = data.data
  },
  GET_ALL_DONATIONS (state, data) {
    state.all_donations = data.data
  },
  GET_DONATION (state, data) {
    state.donation = data
  },
  GET_DELETED_DONATIONS (state, data) {
    state.deleted_donations = data.data
  }
}
