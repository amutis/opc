import Vue from 'vue'
import {donationUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_donations (context) {
    Vue.http.get(donationUrls.getAllDonations).then(function (response) {
      context.commit('GET_ALL_DONATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_donations (context, publicId) {
    Vue.http.get(donationUrls.getDonations + publicId).then(function (response) {
      context.commit('GET_DONATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_donations (context) {
    Vue.http.get(donationUrls.getDeletedDonation).then(function (response) {
      context.commit('GET_DELETED_DONATIONS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_donation (context, publicId) {
    Vue.http.get(donationUrls.getDonation + publicId).then(function (response) {
      context.commit('GET_DONATION', response.data)
      context.dispatch('loading_false')
    })
  },
  donation_request_payment (context, data) {
    Vue.http.post(donationUrls.requestMpesa, data).then(function (response) {
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_public_donation (context, data) {
    Vue.http.post(donationUrls.postDonation, data).then(function (response) {
      window.location.replace('/support-us-payment/' + response.body.donation_ref_code)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_donation (context, data) {
    Vue.http.post(donationUrls.editDonation, data).then(function () {
      context.dispatch('get_all_donations')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_donation (context, publicId) {
    var postData = {
      cancelled_by: localStorage.getItem('lkjhgfdsa')
    }
    Vue.http.post(donationUrls.deleteDonation + publicId, postData).then(function () {
      router.push({
        name: 'Donations'
      })
      context.dispatch('loading_false')
    })
  },
  restoreDonation (context, publicId) {
    Vue.http.get(donationUrls.restoreDonation + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_DONATIONS', response.data)
      router.push({
        name: 'Module.Donations'
      })
      context.dispatch('loading_false')
    })
  }
}
