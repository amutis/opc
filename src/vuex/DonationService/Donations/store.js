import mutations from './mutations'
import actions from './actions'

const state = {
  all_donations: [],
  donations: [],
  donation: [],
  deleted_donations: []
}

export default {
  state, mutations, actions
}
