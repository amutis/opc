import {urls} from '../urls'
export const donationService = urls.developmentUrl + ':5008/v1/'

export const causeUrls = {
  postCause: donationService + 'starts/post',
  getCauses: donationService + 'payment/cause',
  getCause: donationService + 'starts/single/',
  deleteCause: donationService + 'starts/delete/',
  editCause: donationService + 'starts/edit/',
  getAllCauses: donationService + 'payment/cause',
  restoreCause: donationService + 'starts/restore/',
  getDeletedCause: donationService + 'starts/deleted'
}

export const paymentMethodUrls = {
  postPaymentMethod: donationService + 'starts/post',
  getPaymentMethods: donationService + 'payment/methods',
  getPaymentMethod: donationService + 'starts/single/',
  deletePaymentMethod: donationService + 'starts/delete/',
  editPaymentMethod: donationService + 'starts/edit/',
  getAllPaymentMethods: donationService + 'payment/methods',
  restorePaymentMethod: donationService + 'starts/restore/',
  getDeletedPaymentMethod: donationService + 'starts/deleted'
}

export const donationUrls = {
  requestMpesa: donationService + 'donations/payment',
  postDonation: donationService + 'make/donation',
  getDonations: donationService + 'donations',
  getDonation: donationService + 'single/donations/',
  deleteDonation: donationService + 'cancel/donation/',
  editDonation: donationService + 'starts/edit/',
  getAllDonations: donationService + 'donations',
  restoreDonation: donationService + 'starts/restore/',
  getDeletedDonation: donationService + 'starts/deleted'
}

export const anonymityUrls = {
  getAllAnonymity: donationService + 'anonymity'
}

export const frequencyUrls = {
  getAllFrequency: donationService + 'payment/frequency'
}
