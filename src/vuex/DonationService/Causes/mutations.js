export default {
  GET_CAUSES (state, data) {
    state.causes = data.data
  },
  GET_ALL_CAUSES (state, data) {
    state.all_causes = data.data
  },
  GET_CAUSE (state, data) {
    state.cause = data.data
  },
  GET_DELETED_CAUSES (state, data) {
    state.deleted_causes = data.data
  }
}
