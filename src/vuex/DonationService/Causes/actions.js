import Vue from 'vue'
import {causeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_causes (context) {
    Vue.http.get(causeUrls.getAllCauses).then(function (response) {
      context.commit('GET_ALL_CAUSES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_causes (context, publicId) {
    Vue.http.get(causeUrls.getCauses + publicId).then(function (response) {
      context.commit('GET_CAUSES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_causes (context) {
    Vue.http.get(causeUrls.getDeletedCause).then(function (response) {
      context.commit('GET_DELETED_CAUSES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_cause (context, publicId) {
    Vue.http.get(causeUrls.getCause + publicId).then(function (response) {
      context.commit('GET_CAUSE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_cause (context, data) {
    Vue.http.post(causeUrls.postCause, data).then(function () {
      context.dispatch('get_all_causes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_cause (context, data) {
    Vue.http.post(causeUrls.editCause, data).then(function () {
      context.dispatch('get_all_causes')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_cause (context, publicId) {
    Vue.http.get(causeUrls.deleteCause + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_CAUSES', response.data)
      router.push({
        name: 'Module.DeletedCauses'
      })
      context.dispatch('loading_false')
    })
  },
  restoreCause (context, publicId) {
    Vue.http.get(causeUrls.restoreCause + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_CAUSES', response.data)
      router.push({
        name: 'Module.Causes'
      })
      context.dispatch('loading_false')
    })
  }
}
