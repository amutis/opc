import mutations from './mutations'
import actions from './actions'

const state = {
  all_causes: [],
  causes: [],
  cause: [],
  deleted_causes: []
}

export default {
  state, mutations, actions
}
