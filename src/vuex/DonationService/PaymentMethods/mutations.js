export default {
  GET_PAYMENT_METHODS (state, data) {
    state.payment_methods = data.data
  },
  GET_ALL_PAYMENT_METHODS (state, data) {
    state.all_payment_methods = data.data
  },
  GET_PAYMENT_METHOD (state, data) {
    state.payment_method = data.data
  },
  GET_DELETED_PAYMENT_METHODS (state, data) {
    state.deleted_payment_methods = data.data
  }
}
