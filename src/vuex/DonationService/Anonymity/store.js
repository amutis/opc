import mutations from './mutations'
import actions from './actions'

const state = {
  all_anonymity: []
}

export default {
  state, mutations, actions
}
