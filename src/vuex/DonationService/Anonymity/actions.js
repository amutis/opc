import Vue from 'vue'
import {anonymityUrls} from '../urls'

export default {
  get_all_anonymity (context) {
    Vue.http.get(anonymityUrls.getAllAnonymity).then(function (response) {
      context.commit('GET_ALL_ANONYMITY', response.data)
      context.dispatch('loading_false')
    })
  }
}
