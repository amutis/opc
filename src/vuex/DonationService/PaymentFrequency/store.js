import mutations from './mutations'
import actions from './actions'

const state = {
  all_frequency: []
}

export default {
  state, mutations, actions
}
