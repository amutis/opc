import Vue from 'vue'
import {frequencyUrls} from '../urls'

export default {
  get_all_frequency (context) {
    Vue.http.get(frequencyUrls.getAllFrequency).then(function (response) {
      context.commit('GET_ALL_FREQUENCY', response.data)
      context.dispatch('loading_false')
    })
  }
}
