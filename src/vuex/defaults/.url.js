import {urls} from '../urls'
export const aumra = urls.developmentUrl + ':9000/v1/'
// export const aumra = 'http://localhost:8000/'
export const authUrls = {
  login: aumra + 'login',
  request_reset: aumra + 'password/reset',
  reset_password: aumra + 'password/reset/',
  user_details: aumra + 'user/single',
  menu: aumra + 'menu'
}
