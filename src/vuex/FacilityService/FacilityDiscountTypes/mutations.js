export default {
  GET_DISCOUNT_TYPES (state, data) {
    state.discount_types = data.data
  },
  GET_ALL_DISCOUNT_TYPES (state, data) {
    state.all_facility_discount_types = data.data
  },
  GET_DISCOUNT_TYPE (state, data) {
    state.discount_type = data.data
  },
  GET_DELETED_DISCOUNT_TYPES (state, data) {
    state.deleted_facility_discount_types = data.data
  }
}
