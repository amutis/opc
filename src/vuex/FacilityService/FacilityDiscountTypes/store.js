import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_discount_types: [],
  facility_discount_types: [],
  facility_discount_type: [],
  deleted_facility_discount_types: []
}

export default {
  state, mutations, actions
}
