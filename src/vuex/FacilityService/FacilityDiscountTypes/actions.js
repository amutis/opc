import Vue from 'vue'
import {facilityDiscountTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_discount_types (context) {
    Vue.http.get(facilityDiscountTypeUrls.getAllFacilityDiscountTypes).then(function (response) {
      context.commit('GET_ALL_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_discount_types (context, publicId) {
    Vue.http.get(facilityDiscountTypeUrls.getFacilityDiscountTypes + publicId).then(function (response) {
      context.commit('GET_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_discount_types (context) {
    Vue.http.get(facilityDiscountTypeUrls.getDeletedFacilityDiscountType).then(function (response) {
      context.commit('GET_DELETED_DISCOUNT_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_discount_type (context, publicId) {
    Vue.http.get(facilityDiscountTypeUrls.getFacilityDiscountType + publicId).then(function (response) {
      context.commit('GET_DISCOUNT_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility_discount_type (context, data) {
    Vue.http.post(facilityDiscountTypeUrls.postFacilityDiscountType, data).then(function () {
      context.dispatch('get_all_facility_discount_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_discount_type (context, data) {
    Vue.http.post(facilityDiscountTypeUrls.editFacilityDiscountType, data).then(function () {
      context.dispatch('get_all_facility_discount_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_discount_type (context, publicId) {
    Vue.http.get(facilityDiscountTypeUrls.deleteFacilityDiscountType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_DISCOUNT_TYPES', response.data)
      router.push({
        name: 'Module.DeletedFacilityDiscountTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreFacilityDiscountType (context, publicId) {
    Vue.http.get(facilityDiscountTypeUrls.restoreFacilityDiscountType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_DISCOUNT_TYPES', response.data)
      router.push({
        name: 'Module.FacilityDiscountTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
