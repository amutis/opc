import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_contacts: [],
  facility_contacts: [],
  facility_contact: [],
  deleted_facility_contacts: []
}

export default {
  state, mutations, actions
}
