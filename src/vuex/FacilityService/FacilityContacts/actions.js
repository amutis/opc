import Vue from 'vue'
import {facilityContactUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_contacts (context, publicId) {
    Vue.http.get(facilityContactUrls.getAllFacilityContact + publicId).then(function (response) {
      context.commit('GET_ALL_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_contacts (context, publicId) {
    Vue.http.get(facilityContactUrls.getAllFacilityContact + publicId).then(function (response) {
      context.commit('GET_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_contacts (context, publicId) {
    Vue.http.get(facilityContactUrls.getActiveFacilityContact + publicId).then(function (response) {
      context.commit('GET_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_in_active_contacts (context, publicId) {
    Vue.http.get(facilityContactUrls.getInActiveFacilityContact + publicId).then(function (response) {
      context.commit('GET_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_contacts (context) {
    Vue.http.get(facilityContactUrls.getDeletedFacilityContact).then(function (response) {
      context.commit('GET_DELETED_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_contact (context, publicId) {
    Vue.http.get(facilityContactUrls.getFacilityContact + publicId).then(function (response) {
      context.commit('GET_FACILITY_CONTACT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility_contact (context, data) {
    Vue.http.post(facilityContactUrls.postFacilityContact, data).then(function () {
      context.dispatch('get_all_facility_contacts', data.facility_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  make_facility_contact_active (context, data) {
    Vue.http.get(facilityContactUrls.makeFacilityContactActive + data.contact_id).then(function () {
      context.commit('GET_ALL_FACILITY_CONTACTS', {data: []})
      context.dispatch('get_all_facility_contacts', data.facility_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  make_facility_contact_in_active (context, data) {
    Vue.http.get(facilityContactUrls.makeFacilityContactInActive + data.contact_id).then(function () {
      context.commit('GET_ALL_FACILITY_CONTACTS', {data: []})
      context.dispatch('get_all_facility_contacts', data.facility_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_contact (context, data) {
    Vue.http.post(facilityContactUrls.editFacilityContact, data).then(function () {
      context.dispatch('get_all_facility_contacts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_contact (context, publicId) {
    Vue.http.get(facilityContactUrls.deleteFacilityContact + publicId).then(function (response) {
      context.commit('GET_DELETED_FACILITY_CONTACTS', response.data)
      context.dispatch('loading_false')
    })
  },
  restoreFacilityContact (context, publicId) {
    Vue.http.get(facilityContactUrls.restoreFacilityContact + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_CONTACTS', response.data)
      router.push({
        name: 'Module.FacilityContacts'
      })
      context.dispatch('loading_false')
    })
  }
}
