export default {
  GET_FACILITY_CONTACTS (state, data) {
    state.facility_contacts = data.data
  },
  GET_ALL_FACILITY_CONTACTS (state, data) {
    state.all_facility_contacts = data.data
  },
  GET_FACILITY_CONTACT (state, data) {
    state.facility_contact = data.data
  },
  GET_DELETED_FACILITY_CONTACTS (state, data) {
    state.deleted_facility_contacts = data.data
  }
}
