export default {
  GET_FACILITIES (state, data) {
    state.facilities = data.data
  },
  GET_ALL_FACILITIES (state, data) {
    state.all_facilities = data.data
  },
  GET_FACILITY (state, data) {
    state.facility = data.data[0]
  },
  GET_DELETED_FACILITIES (state, data) {
    state.deleted_facilities = data.data
  },
  GET_ACTIVE_CAMPING (state, data) {
    state.active_camping = data.data
  },
  GET_ACTIVE_FACILITIES (state, data) {
    state.active_facilities = data.data
  },
  GET_IN_ACTIVE_FACILITIES (state, data) {
    state.inactive_facilities = data.data
  },
  GET_ACTIVE_ACCOMMODATION (state, data) {
    state.active_accommodations = data.data
  },
  RECOMMENDED_CAMPING (state, data) {
    state.recommended_camping = data.data
  },
  RECOMMENDED_ACCOMMODATION (state, data) {
    state.recommended_accommodation = data.data
  },
  GET_FACILITY_IMAGES (state, data) {
    state.facility_images = data.data
  },
  GET_FACILITY_AVAILABILITY (state, data) {
    state.active_facilities = data.facility_data.concat(data.camping_data)
  },
  BOOKING_FACILITIES (state, data) {
    state.chosenFacilities = data
  },
  BROWSER_FACILITIES (state, data) {
    state.browser_facilities = data
  },
  ADD_FACILITY (state, data) {
    state.chosenFacilities.push(data)
  },
  REMOVE_FACILITY (state, data) {
    state.chosenFacilities.splice(state.chosenFacilities.indexOf(data), 1)
  }
}
