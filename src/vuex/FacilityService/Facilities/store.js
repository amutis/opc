import mutations from './mutations'
import actions from './actions'

const state = {
  all_facilities: [],
  facilities: [],
  facility: [],
  deleted_facilities: [],
  active_camping: [],
  active_facilities: [],
  inactive_facilities: [],
  active_accommodations: [],
  recommended_camping: [],
  recommended_accommodation: [],
  facility_images: [],
  browser_facilities: [],
  chosenFacilities: []
}

export default {
  state, mutations, actions
}
