import Vue from 'vue'
import {facilityUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facilities (context) {
    Vue.http.get(facilityUrls.getAllFacilities).then(function (response) {
      context.commit('GET_ALL_FACILITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_camping (context) {
    Vue.http.get(facilityUrls.getActiveCamping).then(function (response) {
      context.commit('GET_ACTIVE_CAMPING', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_facilities (context) {
    Vue.http.get(facilityUrls.getActiveFacilities).then(function (response) {
      context.commit('GET_ACTIVE_FACILITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_inactive_facilities (context) {
    Vue.http.get(facilityUrls.getInActiveFacilities).then(function (response) {
      context.commit('GET_IN_ACTIVE_FACILITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_active_accommodation (context) {
    Vue.http.get(facilityUrls.getActiveAccommodation).then(function (response) {
      context.commit('GET_ACTIVE_ACCOMMODATION', response.data)
      context.dispatch('loading_false')
    })
  },
  recommend_camping (context, publicId) {
    Vue.http.get(facilityUrls.recommendCamping + publicId).then(function (response) {
      context.commit('RECOMMENDED_CAMPING', response.data)
      context.dispatch('loading_false')
    })
  },
  recommend_accommodation (context, publicId) {
    Vue.http.get(facilityUrls.recommendAccommodation + publicId).then(function (response) {
      context.commit('RECOMMENDED_ACCOMMODATION', response.data)
      context.dispatch('loading_false')
    })
  },
  make_facility_inactive (context, data) {
    Vue.http.post(facilityUrls.makeFacilityInactive + data.public_id, data).then(function (response) {
      context.dispatch('get_active_facilities')
      context.dispatch('loading_false')
    })
  },
  make_facility_active (context, data) {
    Vue.http.post(facilityUrls.makeFacilityActive + data.public_id, data).then(function (response) {
      context.dispatch('get_inactive_facilities')
      router.push({
        name: 'Facilities'
      })
      context.dispatch('loading_false')
    })
  },
  get_facilities (context, publicId) {
    Vue.http.get(facilityUrls.getFacilities + publicId).then(function (response) {
      context.commit('GET_FACILITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_images (context, publicId) {
    Vue.http.get(facilityUrls.getFacilityImages + publicId).then(function (response) {
      context.commit('GET_FACILITY_IMAGES', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_FACILITY_IMAGES', [])
    })
  },
  delete_facility_image (context, data) {
    Vue.http.get(facilityUrls.deleteFacilityImage + data.public_id).then(function (response) {
      context.dispatch('get_facility_images', data.facility_id)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facilities (context) {
    Vue.http.get(facilityUrls.getDeletedFacility).then(function (response) {
      context.commit('GET_DELETED_FACILITIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility (context, publicId) {
    Vue.http.get(facilityUrls.getFacility + publicId).then(function (response) {
      context.commit('GET_FACILITY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility (context, data) {
    Vue.http.post(facilityUrls.postFacility, data).then(function () {
      router.push({
        name: 'Facilities'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility (context, data) {
    Vue.http.post(facilityUrls.editFacility + data.public_id, data).then(function () {
      router.push({
        name: 'Facility',
        params: {
          facility_id: data.public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility (context, data) {
    Vue.http.post(facilityUrls.deleteFacility + data.facility_id, data).then(function () {
      router.push({
        name: 'Facilities'
      })
      context.dispatch('loading_false')
    })
  },
  restoreFacility (context, publicId) {
    Vue.http.get(facilityUrls.restoreFacility + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITIES', response.data)
      router.push({
        name: 'Module.Facilities'
      })
      context.dispatch('loading_false')
    })
  }
}
