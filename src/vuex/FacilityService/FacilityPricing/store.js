import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_pricings: [],
  facility_pricings: [],
  facility_pricing: [],
  deleted_facility_pricings: []
}

export default {
  state, mutations, actions
}
