export default {
  GET_FACILITY_PRICINGS (state, data) {
    state.facility_pricings = data.data
  },
  GET_ALL_FACILITY_PRICINGS (state, data) {
    state.all_facility_pricings = data.data
  },
  GET_FACILITY_PRICING (state, data) {
    state.facility_pricing = data.data
  },
  GET_DELETED_FACILITY_PRICINGS (state, data) {
    state.deleted_facility_pricings = data.data
  }
}
