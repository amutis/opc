import Vue from 'vue'
import {facilityPricingUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_pricings (context) {
    Vue.http.get(facilityPricingUrls.getAllFacilityPricings).then(function (response) {
      context.commit('GET_ALL_FACILITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_current_facility_pricing (context, publicId) {
    Vue.http.get(facilityPricingUrls.getCurrentFacilityPricing + publicId).then(function (response) {
      context.commit('GET_FACILITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_pricings (context, publicId) {
    Vue.http.get(facilityPricingUrls.getFacilityPricings + publicId).then(function (response) {
      context.commit('GET_FACILITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_pricings (context) {
    Vue.http.get(facilityPricingUrls.getDeletedFacilityPricing).then(function (response) {
      context.commit('GET_DELETED_FACILITY_PRICINGS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_pricing (context, publicId) {
    Vue.http.get(facilityPricingUrls.getFacilityPricing + publicId).then(function (response) {
      context.commit('GET_FACILITY_PRICING', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_FACILITY_PRICING', {data: []})
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_facility_pricing (context, data) {
    Vue.http.post(facilityPricingUrls.postFacilityPricing, data).then(function () {
      context.dispatch('get_facility_pricing', data.facility_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_pricing (context, data) {
    Vue.http.post(facilityPricingUrls.editFacilityPricing, data).then(function () {
      context.dispatch('get_all_facility_pricings')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_pricing (context, data) {
    Vue.http.post(facilityPricingUrls.deleteFacilityPricing + data.public_id, data).then(function (response) {
      context.dispatch('get_facility_pricing', data.facility_id)
      context.dispatch('loading_false')
    })
  },
  restoreFacilityPricing (context, publicId) {
    Vue.http.get(facilityPricingUrls.restoreFacilityPricing + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_PRICINGS', response.data)
      router.push({
        name: 'Module.FacilityPricings'
      })
      context.dispatch('loading_false')
    })
  }
}
