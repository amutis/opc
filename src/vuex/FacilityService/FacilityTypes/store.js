import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_types: [],
  facility_types: [],
  facility_type: [],
  deleted_facility_types: []
}

export default {
  state, mutations, actions
}
