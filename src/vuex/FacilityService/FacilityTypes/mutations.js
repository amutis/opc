export default {
  GET_FACILITY_TYPES (state, data) {
    state.facility_types = data.data
  },
  GET_ALL_FACILITY_TYPES (state, data) {
    state.all_facility_types = data.data
  },
  GET_FACILITY_TYPE (state, data) {
    state.facility_type = data.data
  },
  GET_DELETED_FACILITY_TYPES (state, data) {
    state.deleted_facility_types = data.data
  }
}
