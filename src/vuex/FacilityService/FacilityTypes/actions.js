import Vue from 'vue'
import {facilityTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_types (context) {
    Vue.http.get(facilityTypeUrls.getAllFacilityTypes).then(function (response) {
      context.commit('GET_ALL_FACILITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_types (context, publicId) {
    Vue.http.get(facilityTypeUrls.getFacilityTypes + publicId).then(function (response) {
      context.commit('GET_FACILITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_types (context) {
    Vue.http.get(facilityTypeUrls.getDeletedFacilityType).then(function (response) {
      context.commit('GET_DELETED_FACILITY_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_type (context, publicId) {
    Vue.http.get(facilityTypeUrls.getFacilityType + publicId).then(function (response) {
      context.commit('GET_FACILITY_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility_type (context, data) {
    Vue.http.post(facilityTypeUrls.postFacilityType, data).then(function () {
      context.dispatch('get_all_facility_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_type (context, data) {
    Vue.http.post(facilityTypeUrls.editFacilityType + data.public_id, data).then(function () {
      context.dispatch('get_all_facility_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_type (context, data) {
    Vue.http.post(facilityTypeUrls.deleteFacilityType + data.public_id, data).then(function (response) {
      context.dispatch('get_all_facility_types')
      context.dispatch('loading_false')
    })
  },
  restoreFacilityType (context, publicId) {
    Vue.http.get(facilityTypeUrls.restoreFacilityType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_TYPES', response.data)
      router.push({
        name: 'Module.FacilityTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
