import Vue from 'vue'
import {facilityMaintainanceUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_starts (context) {
    Vue.http.get(facilityMaintainanceUrls.getAllFacilityMaintainances).then(function (response) {
      context.commit('GET_ALL_FACILITY_MAINTAINANCES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_starts (context, publicId) {
    Vue.http.get(facilityMaintainanceUrls.getFacilityMaintainances + publicId).then(function (response) {
      context.commit('GET_FACILITY_MAINTAINANCES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_starts (context) {
    Vue.http.get(facilityMaintainanceUrls.getDeletedFacilityMaintainance).then(function (response) {
      context.commit('GET_DELETED_FACILITY_MAINTAINANCES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_start (context, publicId) {
    Vue.http.get(facilityMaintainanceUrls.getFacilityMaintainance + publicId).then(function (response) {
      context.commit('GET_FACILITY_MAINTAINANCE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_start (context, data) {
    Vue.http.post(facilityMaintainanceUrls.postFacilityMaintainance, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_start (context, data) {
    Vue.http.post(facilityMaintainanceUrls.editFacilityMaintainance, data).then(function () {
      context.dispatch('get_all_starts')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_start (context, publicId) {
    Vue.http.get(facilityMaintainanceUrls.deleteFacilityMaintainance + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_FACILITY_MAINTAINANCES', response.data)
      router.push({
        name: 'Module.DeletedFacilityMaintainances'
      })
      context.dispatch('loading_false')
    })
  },
  restoreFacilityMaintainance (context, publicId) {
    Vue.http.get(facilityMaintainanceUrls.restoreFacilityMaintainance + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_MAINTAINANCES', response.data)
      router.push({
        name: 'Module.FacilityMaintainances'
      })
      context.dispatch('loading_false')
    })
  }
}
