import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_maintainances: [],
  facility_maintainances: [],
  facility_maintainance: [],
  deleted_facility_maintainances: []
}

export default {
  state, mutations, actions
}
