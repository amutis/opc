export default {
  GET_FACILITY_MAINTAINANCES (state, data) {
    state.facility_maintainances = data.data
  },
  GET_ALL_FACILITY_MAINTAINANCES (state, data) {
    state.all_facility_maintainances = data.data
  },
  GET_FACILITY_MAINTAINANCE (state, data) {
    state.facility_maintainance = data.data
  },
  GET_DELETED_FACILITY_MAINTAINANCES (state, data) {
    state.deleted_facility_maintainances = data.data
  }
}
