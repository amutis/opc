import Vue from 'vue'
import {facilityDiscountUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_discounts (context) {
    Vue.http.get(facilityDiscountUrls.getAllFacilityDiscounts).then(function (response) {
      context.commit('GET_ALL_FACILITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_discounts (context, publicId) {
    Vue.http.get(facilityDiscountUrls.getFacilityDiscounts + publicId).then(function (response) {
      context.commit('GET_FACILITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    }).catch(function () {
      context.commit('GET_FACILITY_DISCOUNTS', [])
    })
  },
  get_deleted_facility_discounts (context) {
    Vue.http.get(facilityDiscountUrls.getDeletedFacilityDiscount).then(function (response) {
      context.commit('GET_DELETED_FACILITY_DISCOUNTS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_discount (context, publicId) {
    Vue.http.get(facilityDiscountUrls.getFacilityDiscount + publicId).then(function (response) {
      context.commit('GET_FACILITY_DISCOUNT', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility_discount (context, data) {
    Vue.http.post(facilityDiscountUrls.postFacilityDiscount, data).then(function () {
      context.dispatch('get_facility_discounts', data.facility_id)
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_discount (context, data) {
    Vue.http.post(facilityDiscountUrls.editFacilityDiscount, data).then(function () {
      context.dispatch('get_all_facility_discounts')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_discount (context, data) {
    Vue.http.post(facilityDiscountUrls.deleteFacilityDiscount + data.public_id, data).then(function (response) {
      context.dispatch('get_facility_discounts', data.facility_id)
      context.dispatch('loading_false')
    })
  },
  restoreFacilityDiscount (context, publicId) {
    Vue.http.get(facilityDiscountUrls.restoreFacilityDiscount + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_DISCOUNTS', response.data)
      router.push({
        name: 'Module.FacilityDiscounts'
      })
      context.dispatch('loading_false')
    })
  }
}
