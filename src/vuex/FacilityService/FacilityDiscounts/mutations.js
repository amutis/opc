export default {
  GET_FACILITY_DISCOUNTS (state, data) {
    state.facility_discounts = data.data
  },
  GET_ALL_FACILITY_DISCOUNTS (state, data) {
    state.all_facility_discounts = data.data
  },
  GET_FACILITY_DISCOUNT (state, data) {
    state.facility_discount = data.data
  },
  GET_DELETED_FACILITY_DISCOUNTS (state, data) {
    state.deleted_facility_discounts = data.data
  }
}
