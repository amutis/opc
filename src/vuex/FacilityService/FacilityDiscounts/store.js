import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_discounts: [],
  facility_discounts: [],
  facility_discount: [],
  deleted_facility_discounts: []
}

export default {
  state, mutations, actions
}
