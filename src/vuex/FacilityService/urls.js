import {urls} from '../urls'
export const facilityService = urls.developmentUrl + ':5001/'

export const facilityTypeUrls = {
  postFacilityType: facilityService + 'facility/type/save',
  getFacilityTypes: facilityService + 'facility/types',
  getFacilityType: facilityService + 'starts/single/',
  deleteFacilityType: facilityService + 'facility/type/delete/',
  editFacilityType: facilityService + 'facility/alter/type/',
  getAllFacilityTypes: facilityService + 'facility/types',
  restoreFacilityType: facilityService + 'starts/restore/',
  getDeletedFacilityType: facilityService + 'starts/deleted'
}

export const facilityUrls = {
  postFacility: facilityService + 'facility/save',
  getFacilityImages: facilityService + 'facility/images/',
  deleteFacilityImage: facilityService + 'facility/image/delete/',
  getActiveCamping: facilityService + 'facility/camping/active',
  getActiveFacilities: facilityService + 'facilities/active',
  getInActiveFacilities: facilityService + 'facilities/inactive',
  getActiveAccommodation: facilityService + 'facility/accomodation/active',
  recommendCamping: facilityService + 'facilities/recomended/camping/active/',
  recommendAccommodation: facilityService + 'facilities/recomended/accomodation/active/',
  getFacility: facilityService + 'facility/',
  makeFacilityInactive: facilityService + 'facility/alter/status/inactive/',
  makeFacilityActive: facilityService + 'facility/alter/status/active/',
  deleteFacility: facilityService + 'starts/delete/',
  editFacility: facilityService + 'facility/alter/',
  getAllFacilities: facilityService + 'starts/all',
  restoreFacility: facilityService + 'starts/restore/',
  getDeletedFacility: facilityService + 'starts/deleted'
}

export const facilityCurrencyUrls = {
  postFacilityCurrency: facilityService + 'facility/currency/save',
  getFacilityCurrencies: facilityService + 'facility/currency',
  getFacilityCurrency: facilityService + 'facility/currency/',
  deleteFacilityCurrency: facilityService + 'facility/currency/delete/',
  editFacilityCurrency: facilityService + 'facility/currency/alter/',
  getAllFacilityCurrencies: facilityService + 'facility/currency',
  restoreFacilityCurrency: facilityService + 'starts/restore/',
  getDeletedFacilityCurrency: facilityService + 'starts/deleted'
}

export const facilityContactUrls = {
  postFacilityContact: facilityService + 'facility/contact/save',
  getFacilityContacts: facilityService + 'starts',
  getAllFacilityContact: facilityService + 'facility/contact/',
  getActiveFacilityContact: facilityService + 'facility/contact/active/',
  getInActiveFacilityContact: facilityService + 'facility/contact/inactive/',
  makeFacilityContactActive: facilityService + 'facility/contact/inactive/status/',
  makeFacilityContactInActive: facilityService + 'facility/contact/active/status/',
  deleteFacilityContact: facilityService + 'facility/contact/delete/',
  editFacilityContact: facilityService + 'facility/contact/alter/',
  getAllFacilityContacts: facilityService + 'starts/all',
  restoreFacilityContact: facilityService + 'starts/restore/',
  getDeletedFacilityContact: facilityService + 'starts/deleted'
}

export const facilityDiscountTypeUrls = {
  postFacilityDiscountType: facilityService + 'discount/type/save',
  getFacilityDiscountTypes: facilityService + 'discount/types',
  getFacilityDiscountType: facilityService + 'discount/type/',
  deleteFacilityDiscountType: facilityService + 'discount/type/delete/',
  editFacilityDiscountType: facilityService + 'discount/type/alter/',
  getAllFacilityDiscountTypes: facilityService + 'discount/types',
  restoreFacilityDiscountType: facilityService + 'starts/restore/',
  getDeletedFacilityDiscountType: facilityService + 'starts/deleted'
}

export const facilityDiscountUrls = {
  postFacilityDiscount: facilityService + 'discount/save',
  getFacilityDiscounts: facilityService + 'discounts/',
  getFacilityDiscount: facilityService + 'starts/single/',
  deleteFacilityDiscount: facilityService + 'discount/delete/',
  editFacilityDiscount: facilityService + 'discount/alter/',
  getAllFacilityDiscounts: facilityService + 'starts/all',
  restoreFacilityDiscount: facilityService + 'starts/restore/',
  getDeletedFacilityDiscount: facilityService + 'starts/deleted'
}

export const facilityPricingUrls = {
  postFacilityPricing: facilityService + 'facility/pricing/save',
  getFacilityPricings: facilityService + 'facility/pricing/',
  getCurrentFacilityPricing: facilityService + 'facility/current/pricing/',
  getFacilityPricing: facilityService + 'facility/pricing/',
  deleteFacilityPricing: facilityService + 'facility/pricing/delete/',
  editFacilityPricing: facilityService + 'facility/pricing/alter/',
  getAllFacilityPricings: facilityService + 'starts/all',
  restoreFacilityPricing: facilityService + 'starts/restore/',
  getDeletedFacilityPricing: facilityService + 'starts/deleted'
}

export const facilityMaintainanceUrls = {
  postFacilityMaintainance: facilityService + 'down/type/save',
  getFacilityMaintainances: facilityService + 'down/type/',
  getFacilityMaintainance: facilityService + 'starts/single/',
  deleteFacilityMaintainance: facilityService + 'down/type/delete/',
  editFacilityMaintainance: facilityService + 'down/alter/type/',
  getAllFacilityMaintainances: facilityService + 'starts/all',
  restoreFacilityMaintainance: facilityService + 'starts/restore/',
  getDeletedFacilityMaintainance: facilityService + 'starts/deleted'
}

export const accommodationTypeUrls = {
  postAccommodationType: facilityService + 'accomodation/type/save',
  getAccommodationTypes: facilityService + 'accomodation/types',
  getAccommodationType: facilityService + 'accomodation/type/',
  deleteAccommodationType: facilityService + 'accomodation/type/delete/',
  editAccommodationType: facilityService + 'accomodation/alter/type/',
  getAllAccommodationTypes: facilityService + 'accomodation/types',
  restoreAccommodationType: facilityService + 'starts/restore/',
  getDeletedAccommodationType: facilityService + 'starts/deleted'
}
