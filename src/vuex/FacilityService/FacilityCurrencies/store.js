import mutations from './mutations'
import actions from './actions'

const state = {
  all_facility_currencies: [],
  facility_currencies: [],
  facility_currency: [],
  deleted_facility_currencies: []
}

export default {
  state, mutations, actions
}
