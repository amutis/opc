import Vue from 'vue'
import {facilityCurrencyUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_facility_currencies (context) {
    Vue.http.get(facilityCurrencyUrls.getAllFacilityCurrencies).then(function (response) {
      context.commit('GET_ALL_FACILITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_currencies (context, publicId) {
    Vue.http.get(facilityCurrencyUrls.getFacilityCurrencies + publicId).then(function (response) {
      context.commit('GET_FACILITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_facility_currencies (context) {
    Vue.http.get(facilityCurrencyUrls.getDeletedFacilityCurrency).then(function (response) {
      context.commit('GET_DELETED_FACILITY_CURRENCIES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_facility_currency (context, publicId) {
    Vue.http.get(facilityCurrencyUrls.getFacilityCurrency + publicId).then(function (response) {
      context.commit('GET_FACILITY_CURRENCY', response.data)
      context.dispatch('loading_false')
    })
  },
  post_facility_currency (context, data) {
    Vue.http.post(facilityCurrencyUrls.postFacilityCurrency, data).then(function () {
      context.dispatch('get_all_facility_currencies')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_facility_currency (context, data) {
    Vue.http.post(facilityCurrencyUrls.postFacilityCurrency, data).then(function () {
      context.dispatch('get_all_facility_currencies')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_facility_currency (context, publicId) {
    Vue.http.get(facilityCurrencyUrls.deleteFacilityCurrency + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_FACILITY_CURRENCIES', response.data)
      router.push({
        name: 'Module.DeletedFacilityCurrencies'
      })
      context.dispatch('loading_false')
    })
  },
  restoreFacilityCurrency (context, publicId) {
    Vue.http.get(facilityCurrencyUrls.restoreFacilityCurrency + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_FACILITY_CURRENCIES', response.data)
      router.push({
        name: 'Module.FacilityCurrencies'
      })
      context.dispatch('loading_false')
    })
  }
}
