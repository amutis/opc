export default {
  GET_FACILITY_CURRENCIES (state, data) {
    state.facility_currencies = data.data
  },
  GET_ALL_FACILITY_CURRENCIES (state, data) {
    state.all_facility_currencies = data.data
  },
  GET_FACILITY_CURRENCY (state, data) {
    state.facility_currency = data.data
  },
  GET_DELETED_FACILITY_CURRENCIES (state, data) {
    state.deleted_facility_currencies = data.data
  }
}
