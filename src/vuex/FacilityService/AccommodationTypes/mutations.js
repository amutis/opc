export default {
  GET_ACCOMMODATION_TYPES (state, data) {
    state.accommodation_types = data.data
  },
  GET_ALL_ACCOMMODATION_TYPES (state, data) {
    state.all_accommodation_types = data.data
  },
  GET_ACCOMMODATION_TYPE (state, data) {
    state.accommodation_type = data.data
  },
  GET_DELETED_ACCOMMODATION_TYPES (state, data) {
    state.deleted_accommodation_types = data.data
  }
}
