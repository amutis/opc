import Vue from 'vue'
import {accommodationTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_accommodation_types (context) {
    Vue.http.get(accommodationTypeUrls.getAllAccommodationTypes).then(function (response) {
      context.commit('GET_ALL_ACCOMMODATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_accommodation_types (context, publicId) {
    Vue.http.get(accommodationTypeUrls.getAccommodationTypes + publicId).then(function (response) {
      context.commit('GET_ACCOMMODATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_accommodation_types (context) {
    Vue.http.get(accommodationTypeUrls.getDeletedAccommodationType).then(function (response) {
      context.commit('GET_DELETED_ACCOMMODATION_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_accommodation_type (context, publicId) {
    Vue.http.get(accommodationTypeUrls.getAccommodationType + publicId).then(function (response) {
      context.commit('GET_ACCOMMODATION_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_accommodation_type (context, data) {
    Vue.http.post(accommodationTypeUrls.postAccommodationType, data).then(function () {
      context.dispatch('get_all_accommodation_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_accommodation_type (context, data) {
    Vue.http.post(accommodationTypeUrls.editAccommodationType, data).then(function () {
      context.dispatch('get_all_accommodation_types')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_accommodation_type (context, publicId) {
    Vue.http.get(accommodationTypeUrls.deleteAccommodationType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_ACCOMMODATION_TYPES', response.data)
      router.push({
        name: 'Module.DeletedAccommodationTypes'
      })
      context.dispatch('loading_false')
    })
  },
  restoreAccommodationType (context, publicId) {
    Vue.http.get(accommodationTypeUrls.restoreAccommodationType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_ACCOMMODATION_TYPES', response.data)
      router.push({
        name: 'Module.AccommodationTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
