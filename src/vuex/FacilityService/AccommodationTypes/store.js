import mutations from './mutations'
import actions from './actions'

const state = {
  all_accommodation_types: [],
  accommodation_types: [],
  accommodation_type: [],
  deleted_accommodation_types: []
}

export default {
  state, mutations, actions
}
