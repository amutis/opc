export default {
  GET_PARTNER_TYPES (state, data) {
    state.partner_types = data.data
  },
  GET_ALL_PARTNER_TYPES (state, data) {
    state.all_partner_types = data.data
  },
  GET_PARTNER_TYPE (state, data) {
    state.partner_type = data.data
  },
  GET_DELETED_PARTNER_TYPES (state, data) {
    state.deleted_partner_types = data.data
  }
}
