import mutations from './mutations'
import actions from './actions'

const state = {
  all_partner_types: [],
  partner_types: [],
  partner_type: [],
  deleted_partner_types: []
}

export default {
  state, mutations, actions
}
