import Vue from 'vue'
import {partnerTypeUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_partner_types (context) {
    Vue.http.get(partnerTypeUrls.getAllPartnerTypes).then(function (response) {
      context.commit('GET_ALL_PARTNER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner_types (context, publicId) {
    Vue.http.get(partnerTypeUrls.getPartnerTypes + publicId).then(function (response) {
      context.commit('GET_PARTNER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_partner_types (context) {
    Vue.http.get(partnerTypeUrls.getDeletedPartnerType).then(function (response) {
      context.commit('GET_DELETED_PARTNER_TYPES', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner_type (context, publicId) {
    Vue.http.get(partnerTypeUrls.getPartnerType + publicId).then(function (response) {
      context.commit('GET_PARTNER_TYPE', response.data)
      context.dispatch('loading_false')
    })
  },
  post_partner_type (context, data) {
    Vue.http.post(partnerTypeUrls.postPartnerType, data).then(function () {
      context.dispatch('get_all_partner_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_partner_type (context, data) {
    Vue.http.post(partnerTypeUrls.editPartnerType + data.public_id, data).then(function () {
      context.dispatch('get_all_partner_types')
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_partner_type (context, publicId) {
    var postData = {
      session_id: localStorage.getItem('lkjhgfdsa')
    }
    Vue.http.patch(partnerTypeUrls.deletePartnerType + publicId, postData).then(function () {
      context.dispatch('get_all_partner_types')
      context.dispatch('loading_false')
    })
  },
  restorePartnerType (context, publicId) {
    Vue.http.get(partnerTypeUrls.restorePartnerType + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PARTNER_TYPES', response.data)
      router.push({
        name: 'Module.PartnerTypes'
      })
      context.dispatch('loading_false')
    })
  }
}
