import mutations from './mutations'
import actions from './actions'

const state = {
  all_partners: [],
  partners: [],
  partner: [],
  deleted_partners: []
}

export default {
  state, mutations, actions
}
