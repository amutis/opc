export default {
  GET_PARTNERS (state, data) {
    state.partners = data.data
  },
  GET_ALL_PARTNERS (state, data) {
    state.all_partners = data.data
  },
  GET_PARTNER (state, data) {
    state.partner = data
  },
  GET_DELETED_PARTNERS (state, data) {
    state.deleted_partners = data.data
  }
}
