import Vue from 'vue'
import {partnerUrls} from '../urls'
import router from '../../../router/index'

export default {
  get_all_partners (context) {
    Vue.http.get(partnerUrls.getAllPartners).then(function (response) {
      context.commit('GET_ALL_PARTNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partners (context, publicId) {
    Vue.http.get(partnerUrls.getPartners + publicId).then(function (response) {
      context.commit('GET_PARTNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_partners (context) {
    Vue.http.get(partnerUrls.getDeletedPartner).then(function (response) {
      context.commit('GET_DELETED_PARTNERS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_partner (context, publicId) {
    Vue.http.get(partnerUrls.getPartner + publicId).then(function (response) {
      context.commit('GET_PARTNER', response.data)
      context.dispatch('loading_false')
    })
  },
  suspend_partner (context, data) {
    Vue.http.patch(partnerUrls.suspendPartner + data.public_id, data).then(function () {
      router.push({
        name: 'Partners'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  activate_partner (context, data) {
    Vue.http.patch(partnerUrls.activatePartner + data.public_id, data).then(function () {
      router.push({
        name: 'Partners'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  post_partner (context, data) {
    Vue.http.post(partnerUrls.postPartner, data).then(function () {
      context.dispatch('get_all_partners')
      router.push({
        name: 'Partners'
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  update_partner (context, data) {
    Vue.http.post(partnerUrls.editPartner + data.public_id, data).then(function () {
      context.dispatch('get_all_partners')
      router.push({
        name: 'Partner',
        params: {
          partner_id: data.public_id
        }
      })
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      // console.log(error.data)
    })
  },
  delete_partner (context, publicId) {
    Vue.http.get(partnerUrls.deletePartner + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_PARTNERS', response.data)
      router.push({
        name: 'Module.DeletedPartners'
      })
      context.dispatch('loading_false')
    })
  },
  restorePartner (context, publicId) {
    Vue.http.get(partnerUrls.restorePartner + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_PARTNERS', response.data)
      router.push({
        name: 'Module.Partners'
      })
      context.dispatch('loading_false')
    })
  }
}
