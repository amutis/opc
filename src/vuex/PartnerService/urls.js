import {urls} from '../urls'
export const partnerService = urls.developmentUrl + ':5003/v1/'

export const partnerTypeUrls = {
  postPartnerType: partnerService + 'add/type',
  getPartnerTypes: partnerService + 'partner/types',
  getPartnerType: partnerService + 'starts/single/',
  deletePartnerType: partnerService + 'delete/type/',
  editPartnerType: partnerService + 'update/type/',
  getAllPartnerTypes: partnerService + 'partner/types',
  restorePartnerType: partnerService + 'starts/restore/',
  getDeletedPartnerType: partnerService + 'starts/deleted'
}

export const partnerUrls = {
  postPartner: partnerService + 'add/partner',
  getPartners: partnerService + 'partners',
  getPartner: partnerService + 'partner/',
  suspendPartner: partnerService + 'suspend/partner/',
  activatePartner: partnerService + 'unsuspend/partner/',
  deletePartner: partnerService + 'starts/delete/',
  editPartner: partnerService + 'update/partner/',
  getAllPartners: partnerService + 'partners',
  restorePartner: partnerService + 'starts/restore/',
  getDeletedPartner: partnerService + 'starts/deleted'
}
